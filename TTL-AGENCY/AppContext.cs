﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Common;

namespace TTL_AGENCY
{
    public class AppContext
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private ISession _session => httpContextAccessor.HttpContext.Session;
        private readonly AppSettings appSettings;
        public AppContext(AppSettings appSettings, IHttpContextAccessor httpContextAccessor)
        {
            this.appSettings = appSettings;
            this.httpContextAccessor = httpContextAccessor;
        }
    }
}
