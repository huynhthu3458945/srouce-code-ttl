﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TTL_AGENCY.Helpers;
using TTL_ERP.Models.SystemMode;

namespace TTL_AGENCY
{
    public class AuthorizeClient : IAsyncAuthorizationFilter
    {
        readonly AppContext appSetting;
        readonly HttpClientHelper htmlHelper;

        public AuthorizeClient(AppContext appSetting, HttpClientHelper htmlHelper)
        {
            this.appSetting = appSetting;
            this.htmlHelper = htmlHelper;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext actionContext)
        {
            UserLoginVMs user = new UserLoginVMs();
            if (!actionContext.HttpContext.Request.Cookies.ContainsKey("AgencyUser_tll"))
            {
                var userValue = WebUtility.UrlDecode(actionContext.HttpContext.Request.Cookies["AgencyUser_tll"]);
                //UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                if (userValue == null)
                {
                    actionContext.Result = new RedirectToActionResult("Index", "Login" , null);
                }
            }
        }
    }
}
