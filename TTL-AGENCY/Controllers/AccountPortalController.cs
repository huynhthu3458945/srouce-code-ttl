﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_AGENCY.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;

namespace TTL_AGENCY.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class AccountPortalController : BaseController
    {
        private readonly IMapper mapper;
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public AccountPortalController(IMapper mapper, TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        [Route("Thong-tin-ca-nhan")]
        public IActionResult Index()
        {
            var user = GetUser();
            AccountPortalRep model = new AccountPortalRep();
            var res = htmlHelper.GetAsync<AccountPortalRep>($"{UrlAPI.Url_AccountPortalGetById}?id={user.Id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.ProvinceList = GetComBoxProvince(model.ProvinceId);
                model.DistrictList = GetComBoxDistrict(model.DistrictId, model.ProvinceId);
                model.GenderList = GetComBoxGender(model.GenderId);
            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(AccountPortalRep model)
        {
            try
            {
                var user = GetUser();
                var entity = new AccountPortalReq();
                PropertyCopier<AccountPortalRep, AccountPortalReq>.Copy(model, entity);
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_AccountPortalUpdate, entity).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
    }
}
