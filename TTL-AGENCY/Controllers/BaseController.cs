﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using TTL_AGENCY.Helpers;
using TTL_ERP.Areas.CRM.Models;
using TTL_ERP.Areas.Helper;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;
using TTL_ERP.Models.SystemMode;

namespace TTL_AGENCY.Controllers
{
    public class BaseController : Controller
    {
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        public BaseController(TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper)
        {
            this.htmlHelper = htmlHelper;
            this.uploadHelper = uploadHelper;
        }
        public UserLoginVMs GetUser()
        {
            UserLoginVMs user = new UserLoginVMs();
            if (HttpContext.Request.Cookies.ContainsKey("AgencyUser_tll"))
            {
                var userValue = WebUtility.UrlDecode(HttpContext.Request.Cookies["AgencyUser_tll"]);
                UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                userInfo.Role = userInfo.Role.Trim();
                userInfo.OrganizationId = 68;
                return userInfo;
            }
            return null;
        }
        public async Task<IActionResult> GetJsonUserInfo()
        {
            var user = GetUser();
            return Json(user);
        }
        public string GetScriptShowToast(string status, string msg)
        {
            return $"<script type='text/javascript'>"
                   + $" ShowToast('{status}', '{msg}')"
                   + $" </script>";
        }
        public IEnumerable<SelectListItem> GetComBoxOrderStatus(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxOrderStatus}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxProductCategory(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProductCategory}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxProductType(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProductType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxTransport(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxTransport}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxPayType(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxPayType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxPriceList(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllPriceListboxPriceList}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxPriceAgencyList(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllPriceListAgencyPriceList}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxCustomer(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCustomer}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxUnit(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxUnit}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);

        }
        public IEnumerable<SelectListItem> GetComBoxProduct(int selectIds, int productTypeId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProduct}?organizationId={user.OrganizationId}&productTypeId={productTypeId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxCombo(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCombo}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);

        }
        public async Task<IActionResult> GetCustomerByPhoneJson(string phone)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ViewCustomer>>>($"{UrlAPI.Url_GetAllByPhoneStore}?organizationId={user.OrganizationId}&phone={phone}").GetAwaiter().GetResult();
            return Json(new { Data = res.Data.ListData.FirstOrDefault(), retCode = res.RetCode, retText = res.RetText });
        }
        public async Task<IActionResult> GetUnitComboxJson(int unitId)
        {
            var res = GetComBoxUnit(unitId);
            return Json(new { Data = res });
        }
        public async Task<IActionResult> GetComboComboxJson(int comboId)
        {
            var res = GetComBoxCombo(comboId);
            return Json(new { Data = res });
        }
        public async Task<IActionResult> GetProductComboxJson(int productId, string productTypeCode)
        {
            var user = GetUser();
            if (productTypeCode != null)
            {
                var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code={productTypeCode}").GetAwaiter().GetResult();
                var res = GetComBoxProduct(productId, productType.Data.Id);
                return Json(new { Data = res });
            }
            else
            {
                var res = GetComBoxProduct(productId, 0);
                return Json(new { Data = res });
            }
        }
        public IEnumerable<SelectListItem> GetComBoxCustomerType(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCustomerType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxCustomerStatus(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCustomerStatus}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxGender(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxGender}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxProvince(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProvince}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }

        public IEnumerable<SelectListItem> GetComBoxDistrict(int selectIds, int provinceId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxDistrict}?provinceId={provinceId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public async Task<IActionResult> GetDistrictByProvinceId(int provinceId)
        {
            var res = GetComBoxDistrict(0, provinceId);
            return Json(new { Data = res });
        }
        public async Task<IActionResult> GetPriceListJson(int id)
        {
            var res = htmlHelper.GetAsync<PriceListModel>($"{UrlAPI.Url_GetByIdPriceList}?id={id}").GetAwaiter().GetResult();
            return Json(new { Data = res.Data, retCode = res.RetCode, retText = res.RetText });
        }
        public async Task<IActionResult> GetDiscountKindJson(int comboId)
        {
            var res = GetComBoxDiscountKind(comboId);
            return Json(new { Data = res });
        }
        public IEnumerable<SelectListItem> GetComBoxDiscountKind(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxDiscountKind}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
    }
}
