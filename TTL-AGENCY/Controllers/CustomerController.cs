﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Areas.CRM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_AGENCY.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class CustomerController : BaseController
    {
        private readonly IMapper mapper;
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public CustomerController(IMapper mapper, TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        [Route("Khach-hang")]
        public IActionResult Index()
        {
            List<ViewCustomer> listModel = new List<ViewCustomer>();
            var model = new IndexViewModel<ViewCustomer>();
            TempData["CurrentMenu"] = "Customer";
            return View(model);
        }
        public PartialViewResult LoadData(ViewCustomerFitter viewCustomerFitter)
        {
            var user = GetUser();
            viewCustomerFitter.UserName = user.Username;
            List<ViewCustomer> listModel = new List<ViewCustomer>();
            var model = new IndexViewModel<ViewCustomer>();
            viewCustomerFitter.OrganizationId = 68;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ViewCustomer>>>($"{UrlAPI.Url_GetListCustomerByPhone}" +
                $"?limit={pageItems}&page={viewCustomerFitter.Page}", viewCustomerFitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, viewCustomerFitter.Page, "href=''");
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult Create()
        {

            TempData["CurrentMenu"] = "Customer";
            CustomerModel model = new CustomerModel();
            model.CustomerTypeList = GetComBoxCustomerType(0);
            model.CustomerStatusList = GetComBoxCustomerStatus(0);
            model.GenderId = 1;
            model.GenderList = GetComBoxGender(0);
            model.ProvinceList = GetComBoxProvince(model.GenderId);
            model.DistrictList = GetComBoxDistrict(0, 0);
            model.ContactList = new List<ContactModel>();
            var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeCustomer}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.Code = res.Data;
            }
            ViewBag.IsCreate = true;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(CustomerModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.Ref_Id = user.Id;

                var resParent = htmlHelper.GetAsync<AccountPortalRep>($"{UrlAPI.Url_AccountPortalGetById}?id={user.Id}").GetAwaiter().GetResult();
                if (resParent.RetCode == RetCodeEnum.Ok)
                {
                    model.Ref_Code = resParent.Data.CustomerCode;
                    model.Ref_Id = resParent.Data.CustomerId.Value;
                }

                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<CustomerModel>($"{UrlAPI.Url_GetByCodeCustomer}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã khách hàng đã tồn tại" });
                model.ContactList = model.Details;
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddCustomerAgency, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {   
                    model = res.Data;
                    //TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/Khach-hang", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public IActionResult Edit(int id)
        {
            try
            {
                TempData["CurrentMenu"] = "Customer";
                var model = new CustomerModel();
                var res = htmlHelper.GetAsync<CustomerModel>($"{UrlAPI.Url_GetByIdCustomer}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.Details = res.Data.ContactList;
                    model.CustomerTypeList = GetComBoxCustomerType(model.CustomerTypeId);
                    model.CustomerStatusList = GetComBoxCustomerStatus(model.CustomerStatusId);
                    model.ProvinceList = GetComBoxProvince(model.ProvinceId);
                    model.DistrictList = GetComBoxDistrict(model.DistrictId, model.ProvinceId);
                    model.GenderList = GetComBoxGender(model.GenderId);
                }
                ViewBag.IsCreate = false;
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Edit(CustomerModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.Ref_Id = user.Id;
                model.ContactList = model.Details;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateCustomer, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Customer/Edit?id={model.Id}", urlIndex = "/Khach-hang", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteCustomer}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetCodeMax()
        {
            try
            {
                var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeCustomer}").GetAwaiter().GetResult();
                return Json(new { retCode = res.RetCode, retText = res.RetText, code = res.Data });
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
