﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Text.Json;
using System.Diagnostics;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Microsoft.AspNetCore.Mvc.Rendering;
using TTL_AGENCY.Controllers;
using TTL_AGENCY.Helpers;
using TTL_ERP.Models.SystemMode;
using TTL_ERP.Helpers;
using TTL_ERP.Models.API;
using TTL_ERP.Helpers.API;
using TTL_ERP.Common;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_AGENCY.Models;

namespace TTL_AGENCY.Controllers
{
    public class LoginController : BaseController
    {
        private HttpClientHandler clientHandler = new HttpClientHandler();
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private readonly AppSettings appSettings;
        private UserLoginVMs userVMs;
        public LoginController(AppSettings _appsettings, TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            clientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            appSettings = _appsettings;
            this.htmlHelper = htmlHelper;
        }

        public ActionResult Index()
        {
            var user = GetUser();
            if (user != null)
                return RedirectToAction("Index", "Dashboard");

            return View();
        }

        public class UserLogin
        {
            public string userName { get; set; }
            public string passWord { get; set; }
            public int OrganizationId { get; set; }
        }
        [HttpPost]
        public async Task<IActionResult> Index(string u, string p, int o, string url)
        {
            var login = new UserLogin()
            {
                userName = u,
                passWord = p,
                OrganizationId = o
            };
            byte[] btoken;

            if (HttpContext.Session.TryGetValue("access_token_agency", out btoken))
            {
                return RedirectToAction("Index", "Dashboard");
            }
            try
            {
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();

                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    HttpResponseMessage response = await httpClient.PostAsJsonAsync($"{appSettings.DomainAPI}{appSettings.PortAPI}{UrlAPI.Url_LoginAccountPortal}", login);
                    if (response.IsSuccessStatusCode)
                    {
                        var user = response.Content.ReadAsAsync<GetResponse<UserLoginVMs>>(new[] { new JsonMediaTypeFormatter() }).Result;
                        //if (!string.IsNullOrEmpty(token.Data.DivisionID))
                        //{
                        //    HttpContext.Session.Set("curr_DivisionID", Encoding.ASCII.GetBytes(token.Data.DivisionID));
                        //}
                        //if (token.Data.Permissions != null)
                        //{
                        //    var dataPermission = JsonConvert.SerializeObject(token.Data.Permissions.FirstOrDefault());
                        //    var dataArray = Encoding.UTF8.GetBytes(dataPermission);
                        //    HttpContext.Session.Set("user_permission", dataArray);
                        //}
                        if (user.RetCode == RetCodeEnum.Ok)
                        {
                            if (!string.IsNullOrEmpty(user.Data.Token))
                            {
                                HttpContext.Session.Set("access_token_agency", Encoding.ASCII.GetBytes(user.Data.Token));
                            }
                            //-- add to Cookie User
                            HttpContext.Response.Cookies.Append("AgencyUser_tll", WebUtility.UrlEncode(JsonSerializer.Serialize(user.Data)));
                            ////-- add to Cookie User + 
                            //if (user.Data.Role.Equals("AGENCY")) // Đại lý
                            //    return Json(new { retCode = user.RetCode, url = "/Administrator/Dashboard/IndexSystem", retText = "Đăng nhập thành công!" });
                            //else if (user.Data.Role.Equals("KH")) // Khách hàng
                            //    return Json(new { retCode = user.RetCode, url = "/Administrator/Dashboard/IndexSupperAdmin", retText = "Đăng nhập thành công!" });
                            //else if (user.Data.Role.Equals("CTV")) // Nhân viên
                                return Json(new { retCode = user.RetCode, url = "/Dashboard/Index", retText = "Đăng nhập thành công!" });
                        }
                        else
                            return Json(new { retCode = user.RetCode, url = "", retText = user.RetText });
                    }
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi hệ thống" });
            }
        }
        public ActionResult Logout()
        {
            if (HttpContext.Request.Cookies["AgencyUser_tll"] != null)
            {
                Response.Cookies.Delete("AgencyUser_tll");
            }
            byte[] btoken;
            if (HttpContext.Session.TryGetValue("access_token_agency", out btoken))
            {
                HttpContext.Session.Clear();
            }
            return RedirectToAction("Index", "Login");
        }

        [HttpPost]
        public IActionResult SendOTP(ForgetPasswordRequest model)
        {
            var user = GetUser();
            var res = htmlHelper.PostAsync<ForgetPasswordRequest>($"{UrlAPI.Url_AccountPortalSendOTP}", model).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(new { retCode = 0, url = $"", retText = $"Đã gửi OTP về số điện thoại {user.Username}!" });
            }
            else
            {
                return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
            }

        }

        public ActionResult ChangePassword()
        {
            var user = GetUser();
            ChangePasswordRequest model = new ChangePasswordRequest();
            model.Phone = user.Username;
            return PartialView("_PopupChangePassword", model);
        }
        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordRequest model)
        {
            var user = GetUser();
            var res = htmlHelper.PostAsyncStr<ChangePasswordRequest>($"{UrlAPI.Url_AccountPortalChangePassword}", model).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(new { retCode = 0, url = $"", retText = "Đổi mật khẩu thành công!" });
            }
            else
            {
                return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
            }

        }
    }
}
