﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_AGENCY.Helpers;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Areas.CRM.Models;
using TTL_ERP.Areas.PO.Models;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_AGENCY.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class OrderController : BaseController
    {
        private readonly IMapper mapper;
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public OrderController(IMapper mapper, TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        [Route("Don-hang")]
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var user = GetUser();
                ViewBag.User = user;
                var model = new IndexViewModel<OrderModel>();
                TempData["CurrentMenu"] = "Order";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public PartialViewResult LoadData(OrderFitter fitter)
        {
            List<OrderModel> listModel = new List<OrderModel>();
            var model = new IndexViewModel<OrderModel>();
            var user = GetUser();
            ViewBag.User = user;
            fitter.CustomerPhoneParent = user.Username;
            fitter.OrganizationId = 68;

            if (!string.IsNullOrEmpty(fitter.FromDateStr))
                fitter.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fitter.FromDateStr));
            else
                fitter.FromDate = null;

            if (!string.IsNullOrEmpty(fitter.ToDateStr))
                fitter.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(fitter.ToDateStr));
            else
                fitter.ToDate = null;

            var res = htmlHelper.PostAsyncFitter<ResponseList<List<OrderModel>>>($"{UrlAPI.Url_GetListOrderByPhoneParent}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult CreatePopUp()
        {
            var model = new IndexViewModel<ProductModel>();
            var productCategoryList = GetComBoxProductCategory(0).ToList();
            productCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductCategoryList = productCategoryList;

            var productTypeList = GetComBoxProductType(0).ToList();
            productTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductTypeList = productTypeList;

            return PartialView("_CreatePopUp", model);
        }
        public PartialViewResult LoadDataProduct(ProductFitter fitter)
        {
            var model = new IndexViewModel<Product_StoreProcedure>();
            var user = GetUser();
            fitter.ProductCategoryId = fitter.PopupProductCategoryId;
            fitter.ProductTypeId = fitter.PopupProductTypeId;
            fitter.OrganizationId = user.OrganizationId;

            var res = htmlHelper.PostAsyncFitter<ResponseList<List<Product_StoreProcedure>>>($"{UrlAPI.Url_GetAllProductQuantityEnd}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopup", model);
        }
        public IActionResult CreatePopUpCombo()
        {
            var model = new IndexViewModel<ComboModel>();
            return PartialView("_CreatePopUpCombo", model);
        }

        public PartialViewResult LoadDataCombo(ComboFitter fitter)
        {
            var model = new IndexViewModel<ComboModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ComboModel>>>($"{UrlAPI.Url_GetAllCombo}" +
                 $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/PriceList/Index?page={0}'");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopupCombo", model);
        }
        public IActionResult Create()
        {
            TempData["CurrentMenu"] = "Order";
            OrderModel model = new OrderModel();
            model.OrderType = OrderTypeEnum.Retail;
            //model.CustomerList = GetComBoxCustomer(0);
            model.CustomerList = new List<SelectListItem>();
            model.OrderStatusId = GetOrderStatus("100").Id;
            model.OrderStatusList = GetComBoxOrderStatus(0);

            model.TransportList = GetComBoxTransport(0);
            model.PayTypeList = GetComBoxPayType(0);
            model.PriceList = GetComBoxPriceAgencyList(0);
            model.SalePhone = GetUser().Username;
            model.DiscountKindList = GetComBoxDiscountKind(0);
            model.OrderDetail = new List<OrderDetailModel>();
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date);
            var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeOrder}?type=DH").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.Code = res.Data;
            }
            ViewBag.IsCreate = true;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(OrderModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.OrderDetail = model.Details;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<OrderModel>($"{UrlAPI.Url_GetByCodeOrder}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Đơn hàng đã tồn tại" });

                if (!string.IsNullOrEmpty(model.CreateOnStr))
                    model.CreateOn = DateTime.Now;
                else
                    model.CreateOn = null;

                var res = htmlHelper.PostAsync(UrlAPI.Url_AddOrder, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    //TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/Don-hang", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        public IActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "Order";
            OrderModel model = new OrderModel();
            var res = htmlHelper.GetAsync<OrderModel>($"{UrlAPI.Url_GetByIdOrder}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;

                model.OrderType = OrderTypeEnum.Retail;

                model.TransportList = GetComBoxTransport(model.TransportId);
                model.PayTypeList = GetComBoxPayType(model.PayTypeId);
                model.CustomerList = GetComBoxCustomer(model.CustomerId);
                model.OrderStatusList = GetComBoxOrderStatus(model.OrderStatusId);
                model.PriceList = GetComBoxPriceList(model.PriceId);
                model.DiscountKindList = GetComBoxDiscountKind(model.MT_DiscountKindId);
                model.OrderDetail.ToList().ForEach(z =>
                {
                    z.UnitList = GetComBoxUnit(z.UnitId);
                    z.ProductList = GetComBoxProduct(z.ProductId, 0);
                    z.ComboList = GetComBoxCombo(z.ComboId);
                    z.DiscountKindList = GetComBoxDiscountKind(z.DT_DiscountKindId);
                });
            }
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", model.CreateOn);
            ViewBag.IsCreate = false;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(OrderModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.OrderDetail = model.Details;

                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateOrder, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/Don-hang", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public IActionResult Detail(int id)
        {
            TempData["CurrentMenu"] = "Order";
            OrderModel model = new OrderModel();
            var res = htmlHelper.GetAsync<OrderModel>($"{UrlAPI.Url_GetByIdOrder}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;

                model.OrderType = OrderTypeEnum.Retail;

                model.TransportList = GetComBoxTransport(model.TransportId);
                model.PayTypeList = GetComBoxPayType(model.PayTypeId);
                model.CustomerList = GetComBoxCustomer(model.CustomerId);
                model.OrderStatusList = GetComBoxOrderStatus(model.OrderStatusId);
                model.PriceList = GetComBoxPriceList(model.PriceId);
                model.DiscountKindList = GetComBoxDiscountKind(model.MT_DiscountKindId);
                model.OrderDetail.ToList().ForEach(z =>
                {
                    z.UnitList = GetComBoxUnit(z.UnitId);
                    z.ProductList = GetComBoxProduct(z.ProductId, 0);
                    z.ComboList = GetComBoxCombo(z.ComboId);
                    z.DiscountKindList = GetComBoxDiscountKind(z.DT_DiscountKindId);
                });
            }
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", model.CreateOn);
            ViewBag.IsCreate = false;
            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetCodeMax(string type)
        {
            try
            {
                var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeOrder}?type={type}").GetAwaiter().GetResult();
                return Json(new { retCode = res.RetCode, retText = res.RetText, code = res.Data });
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
        [HttpPost]
        public async Task<IActionResult> UpdateStatus(int id, int orderStatusCode)
        {
            try
            {
                var user = GetUser();

                //// Detail
                //BillModel model = new BillModel();
                //var res = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
                //if (res.RetCode == RetCodeEnum.Ok)
                //{
                //    model = res.Data;
                //}
                //// check tồn kho
                //if (model.BillType == BillTypeEnum.ExportWareHouse || model.BillType == BillTypeEnum.TranferWareHouse) // Xuất kho -- Chuyển
                //{
                //    ProductFitter fitter = new ProductFitter();
                //    fitter.StoreId = model.BillType == BillTypeEnum.TranferWareHouse ? model.StoreExportId : model.StoreId;
                //    fitter.OrganizationId = user.OrganizationId;

                //    string error = CheckQuantityEnd(fitter, model.BillDetail);

                //    if (!string.IsNullOrEmpty(error))
                //        return Json(new { retCode = RetCodeEnum.ApiError, retText = error });
                //}

                var resCheckQuantity = htmlHelper.PostAsyncStr($"{UrlAPI.Url_CheckEndQuantityProductNumber}?id={id}&orderStatusCode={orderStatusCode}", id).GetAwaiter().GetResult();
                if (resCheckQuantity.RetCode == RetCodeEnum.Ok)
                {
                    if (!string.IsNullOrEmpty(resCheckQuantity.Data))
                    {
                        return Json(new { retCode = 2, urlIndex = "", retText = resCheckQuantity.Data });
                    }
                    else
                    {
                        var resCheck = htmlHelper.PostAsyncStr($"{UrlAPI.Url_UpdateOrderStatus}?id={id}&orderStatusCode={orderStatusCode}", id).GetAwaiter().GetResult();
                        if (resCheck.RetCode == RetCodeEnum.Ok)
                        {
                            TempData["Message"] = GetScriptShowToast("success", "Thanh toán thành công!");
                            return Json(new { retCode = resCheck.RetCode, url = $"", urlIndex = "/WM/Warehouse", retText = "Thanh toán thành công!" });
                        }
                        else
                        {
                            return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                        }
                    }

                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }


            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public IActionResult AddCustomer(string phone)
        {
            CustomerModel model = new CustomerModel();
            ViewBag.IsCreate = true;
            var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeCustomer}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.Code = res.Data;
            }
            model.Phone = phone;
            return PartialView("_PopupCustomer", model);
        }

        [HttpPost]
        public IActionResult AddCustomer(CustomerModel model)
        {
            var user = GetUser();
            model.CreateBy = user.Id;
            model.CreateOn = DateTime.Now.Date;
            model.OrganizationId = user.OrganizationId;
            var resParent = htmlHelper.GetAsync<AccountPortalRep>($"{UrlAPI.Url_AccountPortalGetById}?id={user.Id}").GetAwaiter().GetResult();
            if (resParent.RetCode == RetCodeEnum.Ok)
            {
                model.Ref_Code = resParent.Data.CustomerCode;
                model.Ref_Id = resParent.Data.CustomerId.Value;
            }
            // check tồn tại mã
            var resCheck = htmlHelper.GetAsync<CustomerModel>($"{UrlAPI.Url_GetByCodeCustomer}?code={model.Code}").GetAwaiter().GetResult();
            if (resCheck.Data != null)
                return Json(new { retCode = 1, retText = "Mã khách hàng đã tồn tại" });
            model.ContactList = model.Details;
            var res = htmlHelper.PostAsync(UrlAPI.Url_AddCustomerAgency, model).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(new { retCode = 0, url = $"", retText = "Thêm mới thành công!" });
            }
            else
            {
                return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
            }

        }
        private OrderStatusModel GetOrderStatus(string code)
        {
            OrderStatusModel billStatusModel = new OrderStatusModel();
            var res = htmlHelper.GetAsync<OrderStatusModel>($"{UrlAPI.Url_GetBillOrderByCode}?code={code}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                billStatusModel = res.Data;
            }

            return billStatusModel;
        }
    }
}
