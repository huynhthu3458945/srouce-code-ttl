﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Helpers;
using TTL_AGENCY.Models;

namespace TTL_AGENCY.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class QuaTangController : BaseController
    {
        private readonly IMapper mapper;
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public QuaTangController(IMapper mapper, TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        [Route("Qua-tang")]
        public IActionResult Index()
        {
            TempData["CurrentMenu"] = "Quatang";
            var user = GetUser();
            QuatangModel model = new QuatangModel();
            model.Url = $"https://hitacamp.vn/quatang/infophone1.php?serial={user.Username}";
            return View(model);
        }
    }
}
