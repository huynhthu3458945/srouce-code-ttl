﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using TTL_AGENCY.Helpers;
using TTL_ERP.Helpers;
using TTL_ERP.Models.SystemMode;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using TTL_ERP.Common;
using TTL_AGENCY.Models;

namespace TTL_AGENCY.Controllers
{
    public class RegisterController : BaseController
    {
        private HttpClientHandler clientHandler = new HttpClientHandler();
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private readonly AppSettings appSettings;
        private UserLoginVMs userVMs;
        public RegisterController(AppSettings _appsettings, TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            clientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            appSettings = _appsettings;
            this.htmlHelper = htmlHelper;
        }

        public ActionResult Index()
        {
            return View();
        }

        public class AccountPortalReq
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string Roles { get; set; }
            public bool Status { get; set; }
            public int OrganizationId { get; set; }
            public string FullName { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string OTP { get; set; }
        }
        [HttpPost]
        public async Task<IActionResult> Index(AccountPortalReq accountPortalReq)
        {
            try
            {
                accountPortalReq.Phone = accountPortalReq.UserName;
                accountPortalReq.Status = true;
                accountPortalReq.Roles = "KH";
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_AccountPortalAdd, accountPortalReq).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Đăng ký thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Login", urlIndex = "/Login", retText = "Đăng ký thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        public IActionResult SendOTP(ForgetPasswordRequest model)
        {
            var res = htmlHelper.PostAsync<ForgetPasswordRequest>($"{UrlAPI.Url_AccountPortalSendOTPCreate}", model).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(new { retCode = 0, url = $"", retText = $"Đã gửi OTP về số điện thoại {model.Phone}!" });
            }
            else
            {
                return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
            }

        }
    }
}
