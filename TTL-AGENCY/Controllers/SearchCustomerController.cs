﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Areas.CRM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;
namespace TTL_AGENCY.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class SearchCustomerController : BaseController
    {
        private readonly IMapper mapper;
        private readonly TTL_AGENCY.Helpers.HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public SearchCustomerController(IMapper mapper, TTL_AGENCY.Helpers.HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        [Route("Tra-cuu-khach-hang")]
        public IActionResult Index()
        {
            List<ViewCustomer> listModel = new List<ViewCustomer>();
            var model = new IndexViewModel<ViewCustomer>();
            TempData["CurrentMenu"] = "SearchCustomer";
            return View(model);
        }
        public PartialViewResult LoadData(string phone)
        {
            var user = GetUser();
            List<CustomerModel> listModel = new List<CustomerModel>();
            var model = new IndexViewModel<CustomerModel>();
            var res = htmlHelper.GetAsync<ResponseList<List<CustomerModel>>>($"{UrlAPI.Url_GetAllByPhone}" +
                $"?organizationId=68&phone={phone}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
    }
}
