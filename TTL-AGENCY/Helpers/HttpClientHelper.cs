﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TTL_ERP.Models.API;
using TTL_ERP.Models.SystemMode;
using TTL_ERP.Common;
using TTL_ERP.Helpers;

namespace TTL_AGENCY.Helpers
{
    public class HttpClientHelper
    {
        private readonly AppSettings appSetting;
        private readonly ClientHelper _clientHelper;
        private HttpClientHandler clientHandler = new HttpClientHandler();

        public HttpClientHelper(AppSettings _app, ClientHelper clientHelper)
        {
            clientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            appSetting = _app;
            _clientHelper = clientHelper;
        }

        public async Task<GetResponse<T>> PostAsync<T>(string url, T model, string contentType = "application/json")
        {
            try
            {
                //string token = string.Empty;
                UserLoginVMs userLogin = _clientHelper.CookieValue("AgencyUser_tll");
                //token = _clientHelper.SessionValue("access_token_agency");
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    if (userLogin != null)
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    HttpResponseMessage response = await httpClient.PostAsJsonAsync($"{appSetting.DomainAPI}{appSetting.PortAPI}{url}", model);
                    return (await response.Content.ReadAsAsync<GetResponse<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GetResponse<T>> PostAsyncFitter<T>(string url, object model, string contentType = "application/json")
        {
            try
            {
                //string token = string.Empty;
                UserLoginVMs userLogin = _clientHelper.CookieValue("AgencyUser_tll");
                //token = _clientHelper.SessionValue("access_token");
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    if (userLogin != null)
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    HttpResponseMessage response = await httpClient.PostAsJsonAsync($"{appSetting.DomainAPI}{appSetting.PortAPI}{url}", model);
                    return (await response.Content.ReadAsAsync<GetResponse<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GetResponse<string>> PostAsyncStr<T>(string url, T model, string contentType = "application/json")
        {
            try
            {
                //string token = string.Empty;
                UserLoginVMs userLogin = _clientHelper.CookieValue("AgencyUser_tll");
                //token = _clientHelper.SessionValue("access_token");
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    if (userLogin != null)
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    HttpResponseMessage response = await httpClient.PostAsJsonAsync($"{appSetting.DomainAPI}{appSetting.PortAPI}{url}", model);
                    return (await response.Content.ReadAsAsync<GetResponse<string>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GetResponse<T>> GetAsync<T>(string url)
        {
            try
            {
                //string token = string.Empty;
                UserLoginVMs userLogin = _clientHelper.CookieValue("AgencyUser_tll");
                //token = _clientHelper.SessionValue("access_token");
                using (HttpClient httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (userLogin!=null)
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                    var response = await httpClient.GetAsync($"{appSetting.DomainAPI}{appSetting.PortAPI}{url}");
                    return (await response.Content.ReadAsAsync<GetResponse<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
