﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_AGENCY.Models
{
    public class ChangePasswordRequest
    {
        public string OTP { get; set; }
        public string Phone { get; set; }
        public string PassOld { get; set; }
        public string PassNew { get; set; }
        public string PassConfirm { get; set; }
    }
    public class ForgetPasswordRequest
    {
        public string Phone { get; set; }
    }
}
