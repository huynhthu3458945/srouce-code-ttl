﻿var frm = 'frm';

$(document).ready(function () {
    $('#Birthday').FormatDateTime();

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                var data = GetFormData('frm');
                console.log(data);
                url = "/AccountPortal/Edit";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    dataType: "json",
                    encode: true,
                    success: function (res) {
                        if (res.retCode == 0) {
                            ShowToast('success', res.retText);
                        }
                        else if (res.retCode = 1) {
                            $('#Code').focus();
                            ShowToast('error', res.retText);
                        }
                    },
                    error: function () {
                        console.log('Error - Failed');
                    }
                });
            }
        });
    });
});

let fv = null;
function Validate() {
    const formValidationExamples = document.getElementById(frm);
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã'
                    }
                }
            },
            FullName: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập họ tên'
                    }
                }
            },
            GenderId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn giới tính'
                    }
                }
            },

            Phone: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số điện thoại'
                    },
                    regexp: {
                        regexp: /(84|0[3|5|7|8|9])+([0-9]{8})/,
                        message: 'Số điện thoại không hợp lệ'
                    }
                }
            }
            ,
            Email: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập email'
                    },
                    emailAddress: {
                        message: 'Đại chỉ mail chưa hợp lệ'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}
