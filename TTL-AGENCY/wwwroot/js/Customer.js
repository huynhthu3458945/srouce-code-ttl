﻿var frm = 'frm';
var tabletbodyStr = '#tbodyContact tbody';
var tbodyStr = 'tbodyContact';
var urlGetCodeMax = '/Customer/GetCodeMax';

$(document).ready(function () {
    RefreshDynamicCommon();
    InitValue();

    $('#tbodyContact tbody').Uk_RebuilIdAndName();
    $('#Birthday').FormatDateTime();
    
    $('.btnAddRow').click(function () {
        //alert('aaa');
        var table = $(tabletbodyStr);
        var stt = table.find("tr").length + 1;
        var getTemplate = $("#template_subform").html();

        // Compile the template
        var compileTemplate = Handlebars.compile(getTemplate);
        var context = {
            "stt": stt
        };

        // Pass our data to the template
        var passDataToTemplate = compileTemplate(context);
        // Add the compiled html to the page
        $('#tbodyContact tbody').append(passDataToTemplate);

        InitValue();
        table.Uk_RebuildIndex();
        table.Uk_RebuildSortOrder();

    });

    if ($('#IsCompany').prop("checked"))
        $('#CompanyName').prop("disabled", false);
    else
        $('#CompanyName').prop("disabled", true);

    $('#IsCompany').change(function () {
        if (this.checked)
            $('#CompanyName').prop("disabled", false);
        else
            $('#CompanyName').prop("disabled", true);
    });

    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(true, frm, tbodyStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(true, frm, tbodyStr);
            }
        });
    });

    // Sau khi submit
    //fnc_AfterSubmitForm = (res) => {
    //    ShowToast('success', res.retText)
    //    location.href = res.urlIndex;
    //}

    // Sau khi submit và tiếp tục 
    fnc_AfterSubmitFormAndContinue = () => {
        // get Code 
        GetCodeMax(urlGetCodeMax, '#Code');
    }
});

let fv = null;
function Validate() {
    const formValidationExamples = document.getElementById(frm);
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            //CustomerTypeId: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng chọn kiểu'
            //        }
            //    }
            //},
            //CustomerStatusId: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng chọn loại'
            //        }
            //    }
            //},
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã'
                    }
                }
            },
            FullName: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập họ tên'
                    }
                }
            },
            GenderId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn giới tính'
                    }
                }
            },

            Phone: {
                selector: '.DetailPhone',
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số điện thoại'
                    },
                    regexp: {
                        regexp: /(84|0[3|5|7|8|9])+([0-9]{8})/,
                        message: 'Số điện thoại không hợp lệ'
                    }
                }
            }
            ,
            Email: {
                selector: '.DetailEmail',
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập email'
                    },
                    emailAddress: {
                        message: 'Đại chỉ mail chưa hợp lệ'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

function InitValue() {
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $(tabletbodyStr).find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}
