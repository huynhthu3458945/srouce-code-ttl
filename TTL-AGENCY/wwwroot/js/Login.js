﻿/**
 *  Pages Authentication
 */

'use strict';
const formAuthentication = document.querySelector('#formAuthentication');
document.addEventListener('DOMContentLoaded', function (e) {
    (function () {
        // Form validation for Add new record
        if (formAuthentication) {
            const fv = FormValidation.formValidation(formAuthentication, {
                fields: {
                    username: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập tài khoản'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập email'
                            },
                            emailAddress: {
                                message: 'Email không đúng định dạng'
                            }
                        }
                    },
                    OrganizationId: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng chọn tổ chức'
                            }
                        }
                    },
                    'email-username': {
                        validators: {
                            notEmpty: {
                                message: 'Please enter email / username'
                            },
                            stringLength: {
                                min: 6,
                                message: 'Username must be more than 6 characters'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập mật khẩu'
                            }
                        }
                    },
                    'confirm-password': {
                        validators: {
                            notEmpty: {
                                message: 'Please confirm password'
                            },
                            identical: {
                                compare: function () {
                                    return formAuthentication.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            },
                            stringLength: {
                                min: 6,
                                message: 'Password must be more than 6 characters'
                            }
                        }
                    },
                    terms: {
                        validators: {
                            notEmpty: {
                                message: 'Please agree terms & conditions'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap5: new FormValidation.plugins.Bootstrap5({
                        eleValidClass: '',
                        rowSelector: '.mb-3'
                    }),
                    submitButton: new FormValidation.plugins.SubmitButton(),

                    //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    autoFocus: new FormValidation.plugins.AutoFocus()
                },
                init: instance => {
                    instance.on('plugins.message.placed', function (e) {
                        if (e.element.parentElement.classList.contains('input-group')) {
                            e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
                        }
                    });
                }
            })
            const submitButton = document.getElementById('btnLogin');
            submitButton.addEventListener('click', function (e) {
                // Prevent default button action
                e.preventDefault();

                // Validate form before submit
                if (formAuthentication) {
                    fv.validate().then(function (status) {
                        console.log(status);
                        if (status == 'Valid') {
                            BlockUI();
                            var formData = {
                                u: $("#username").val(),
                                p: $("#password").val()
                            };
                         
                            $.ajax({
                                type: "POST",
                                url: "/Login/Index",
                                data: formData,
                                dataType: "json",
                                encode: true,
                                success: function (res) {
                                    if (res.retCode == 0) {
                                        ShowToast('success', res.retText)
                                        window.location = res.url;
                                    }
                                    else if (res.retCode == 1) {
                                        ShowToast('error', res.retText)
                                    }
                                    UnblockUI();
                                },
                                error: function () {
                                    ShowToast('error', 'Đã xảy ra lỗi, vui lòng thử lại!');
                                    UnblockUI();
                                }
                            });
                        }
                    });
                }
            });
        }
        //  Two Steps Verification
        const numeralMask = document.querySelectorAll('.numeral-mask');

        // Verification masking
        if (numeralMask.length) {
            numeralMask.forEach(e => {
                new Cleave(e, {
                    numeral: true
                });
            });
        }
    })();
});
