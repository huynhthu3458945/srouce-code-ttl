﻿$('#btnSendOTP').click(function () {

    if ($('#username').val() == '' || $('#username').val() == undefined) {
        ShowToast('warning', 'Vui lòng nhập số điện thoại');
        $('#username').focus();
        return;
    }

    $('#btnSendOTP').addClass("disabled")
    $.ajax({
        type: "POST",
        url: "/Register/SendOTP",
        data: { Phone: $('#username').val() },
        dataType: "json",
        encode: true,
        success: function (res) {
            if (res.retCode == 0) {
                ShowToast('success', res.retText);
                $('#OTP').focus();
                var timer2 = "1:1";
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var minutes = parseInt(timer[0], 10);
                    var seconds = parseInt(timer[1], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    //minutes = (minutes < 10) ?  minutes : minutes;
                    $('#btnSendOTP').text(minutes + ':' + seconds);
                    if (minutes < 0) clearInterval(interval);

                    if (minutes == 0 && seconds == 0) {
                        $('#btnSendOTP').text('Gửi OTP')
                        $('#btnSendOTP').removeClass("disabled")
                    };
                    //check if both minutes and seconds are 0
                    if ((seconds <= 0) && (minutes <= 0)) clearInterval(interval);
                    timer2 = minutes + ':' + seconds;
                }, 1000);
            }
            else if (res.retCode = 1) {
                $('#btnSendOTP').removeClass("disabled")
                ShowToast('error', res.retText);
            }
        },
        error: function () {
            $('#btnSendOTP').removeClass("disabled")
            console.log('Error - Failed');
        }
    });
});

/**
 *  Pages Authentication
 */

'use strict';
const formAuthentication = document.querySelector('#frm');
document.addEventListener('DOMContentLoaded', function (e) {
    (function () {
        // Form validation for Add new record
        if (formAuthentication) {
            const fv = FormValidation.formValidation(formAuthentication, {
                fields: {
                    username: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập số điện thoại'
                            },
                            regexp: {
                                regexp: /(84|0[3|5|7|8|9])+([0-9]{8})/,
                                message: 'Số điện thoại không hợp lệ'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập email'
                            },
                            emailAddress: {
                                message: 'Địa chỉ email không hợp lệ'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập mật khẩu'
                            }
                        }
                    },
                    'confirm-password': {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập mật khẩu xác nhận'
                            },
                            identical: {
                                compare: function () {
                                    return formAuthentication.querySelector('[name="password"]').value;
                                },
                                message: 'Mật mã không khớp'
                            }
                        }
                    },
                    terms: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng đồng ý với điều khoản chính sách'
                            }
                        }
                    },
                    OTP: {
                        validators: {
                            notEmpty: {
                                message: 'Vui lòng nhập OTP'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap5: new FormValidation.plugins.Bootstrap5({
                        eleValidClass: '',
                        rowSelector: '.mb-3'
                    }),
                    submitButton: new FormValidation.plugins.SubmitButton(),

                    //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    autoFocus: new FormValidation.plugins.AutoFocus()
                },
                init: instance => {
                    instance.on('plugins.message.placed', function (e) {
                        if (e.element.parentElement.classList.contains('input-group')) {
                            e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
                        }
                    });
                }
            })
            const submitButton = document.getElementById('btnRegister');
            submitButton.addEventListener('click', function (e) {
                // Prevent default button action
                e.preventDefault();

                // Validate form before submit
                if (formAuthentication) {
                    fv.validate().then(function (status) {
                        console.log(status);
                        if (status == 'Valid') {
                            BlockUI();
                            var masterData = GetFormData('frm');

                            $.ajax({
                                type: "POST",
                                url: "/Register/Index",
                                data: masterData,
                                dataType: "json",
                                encode: true,
                                success: function (res) {
                                    if (res.retCode == 0) {
                                        ShowToast('success', res.retText)
                                        window.location = res.url;
                                    }
                                    else if (res.retCode == 1 && res.retText == 'Họ tên không đc bỏ trống.') {
                                        ShowToast('error', res.retText);
                                        $('.fullName').css('display', 'block');
                                        $('#fullName').focus();
                                    }
                                    else if (res.retCode == 1) {
                                        ShowToast('error', res.retText)
                                    }
                                    UnblockUI();
                                },
                                error: function () {
                                    ShowToast('error', 'Đã xảy ra lỗi, vui lòng thử lại!');
                                    UnblockUI();
                                }
                            });
                        }
                    });
                }
            });
        }
        //  Two Steps Verification
        const numeralMask = document.querySelectorAll('.numeral-mask');

        // Verification masking
        if (numeralMask.length) {
            numeralMask.forEach(e => {
                new Cleave(e, {
                    numeral: true
                });
            });
        }
    })();
});
