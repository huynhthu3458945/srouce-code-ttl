﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Helpers;
using AutoMapper;
using TTL_ERP.Models.API;
using TTL_ERP.Helpers.API;
using TTL_ERP.Areas.Administrator.Models;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class AccountController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        public AccountController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int limit = 100, int page = 1)
        {
            try
            {
                var user = GetUser();
                List<AccountModel> listModel = new List<AccountModel>();
                var res = htmlHelper.GetAsync<ResponseList<List<AccountModel>>>($"{UrlAPI.Url_GetAllAccount}?organizationId={user.OrganizationId}&limit={limit}&page={page}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    listModel = res.Data.ListData.ToList();
                }
                ViewData["Account"] = "active";
                return View(listModel);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public IActionResult Create()
        {
            AccountModel model = new AccountModel();
            ViewBag.IsCreate = true;
            ViewData["Account"] = "active";
            return View(model);
        }
        public IActionResult Edit(int id)
        {
            try
            {
                var model = new AccountModel();
                var res = htmlHelper.GetAsync<AccountModel>($"{UrlAPI.Url_GetByIdAccount}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                }
                ViewBag.IsCreate = false;
                ViewData["Account"] = "active";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        [HttpPost]
        public IActionResult Create(AccountModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<AccountModel>($"{UrlAPI.Url_CheckExistUserName}?userName={model.UserName}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Tài khoản đã tồn tại" });
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddAccount, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Account/Edit/{model.Id }", retText = "Thêm mới thành công!" });
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(AccountModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateAccount, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    //TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Account/Edit/{model.Id }", retText = "Cập nhật thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                }
                //return Redirect($"/Organization/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteOrganization}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetTreeRole(int accountId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<List<AcccountRoleModelTree>>($"{UrlAPI.Url_GetTreeRole}?organizationId={user.OrganizationId}&accountId={accountId}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(res.Data);
            }
            else
            {
                return Json(new { retCode = res.RetCode, retText = res.RetText });
            }
        }
        public class AcccountRoleModelTree
        {
            public int id { get; set; }
            public State state { get; set; }
            public string text { get; set; }
            public List<AcccountRoleModelTree> children { get; set; }
            public AcccountRoleModelTree()
            {
                children = new List<AcccountRoleModelTree>();
            }
        }
        public class State
        {
            public bool selected { get; set; }
        }

    }
}
