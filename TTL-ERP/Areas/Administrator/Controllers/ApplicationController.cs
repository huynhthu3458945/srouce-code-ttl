﻿using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class ApplicationController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        public ApplicationController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int limit = 100, int page = 1)
        {
            try
            {
                List<ApplicationModel> listModel = new List<ApplicationModel>();
                var res = htmlHelper.GetAsync<ResponseList<List<ApplicationModel>>>($"{UrlAPI.Url_GetAllApplication}?limit={limit}&page={page}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    listModel = res.Data.ListData.ToList();
                }
                ViewData["Application"] = "active";
                return View(listModel);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public IActionResult Create()
        {
            ApplicationModel model = new ApplicationModel();
            ViewBag.IsCreate = true;
            ViewData["Application"] = "active";
            return View(model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new ApplicationModel();
                var res = htmlHelper.GetAsync<ApplicationModel>($"{UrlAPI.Url_GetByIdApplication}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                }
                ViewBag.IsCreate = false;
                ViewData["Application"] = "active";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Create(ApplicationModel model)
        {
            try
            {
                model.CreateBy = GetUser().Id;
                model.CreateOn = DateTime.Now.Date;
                model.ModifiedOn = DateTime.Now.Date;
                var res = htmlHelper.PostAsync<ApplicationModel>(UrlAPI.Url_AddApplication, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Redirect($"/Application/Edit/{model.Id }");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    return Redirect($"/Application/Edit/{model.Id }");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(ApplicationModel model)
        {
            try
            {
                model.ModifiedBy = GetUser().Id;
                model.ModifiedOn = DateTime.Now.Date;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateApplication, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);

                }
                return Redirect($"/Application/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
               
                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteApplication}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode,  retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
