﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Text.Json;
using TTL_ERP.Models;
using TTL_ERP.Models.SystemMode;
using Microsoft.AspNetCore.Mvc.Rendering;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using TTL_ERP.Areas.Helper;
using TTL_ERP.Areas.Administrator.Models;
using System.Text;
using Microsoft.AspNetCore.Http;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Areas.CRM.Models;
using TTL_ERP.Areas.HRM.Models;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class BaseController : Controller
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        public BaseController(HttpClientHelper htmlHelper, UploadHelper uploadHelper)
        {
            this.htmlHelper = htmlHelper;
            this.uploadHelper = uploadHelper;
        }
        public UserLoginVMs GetUser()
        {
            UserLoginVMs user = new UserLoginVMs();
            if (HttpContext.Request.Cookies.ContainsKey("AdminUser_tll"))
            {
                var userValue = WebUtility.UrlDecode(HttpContext.Request.Cookies["AdminUser_tll"]);
                UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                return userInfo;
            }
            return null;
        }

        public string GetAreaName()
        {
            var areaName = string.Empty;
            object area = null;
            if (RouteData.Values.TryGetValue("area", out area))
            {
                areaName = area.ToString();
            }
            return areaName;
        }

        public async Task<IActionResult> GetJsonUserInfo()
        {
            var user = GetUser();
            return Json(user);
        }

        public string GetScriptShowToast(string status, string msg)
        {
            return $"<script type='text/javascript'>"
                   + $" ShowToast('{status}', '{msg}')"
                   + $" </script>";
        }
        public IEnumerable<SelectListItem> GetComBoxClient(int selectId)
        {
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxClient}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectId);
        }
        public IEnumerable<SelectListItem> GetComBoxApplication(int selectId)
        {
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxApplication}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectId);
        }
        public IEnumerable<SelectListItem> GetComBoxStoreType(int selectId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxStoreType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectId);
        }
        public IEnumerable<SelectListItem> GetComBoxStore(int selectId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxStore}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectId);
        }
        public IEnumerable<SelectListItem> GetComBoxStoreNumber(int selectId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxStoreNumber}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectId);
        }
        public IEnumerable<SelectListItem> GetComBoxStoreByIsPallet(int selectId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxStoreIsPallet}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectId);
        }
        public IEnumerable<SelectListItem> GetComBoxAccount(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxAccount}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxAccount(List<int> selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxAccount}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxProductCategory(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProductCategory}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxProductType(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProductType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxBillCategory(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxBillCategory}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxBillStatus(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxBillStatus}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }

        public IEnumerable<SelectListItem> GetComBoxTreeFunctionSystem(int selectId, int idEdit)
        {
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxTreeFunctionSystem}?id={idEdit}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectId);
        }
        public IEnumerable<SelectListItem> GetComBoxCustomerType(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCustomerType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxCustomerStatus(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCustomerStatus}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxOrderStatus(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxOrderStatus}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxTransport(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxTransport}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxPayType(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxPayType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxPriceList(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllPriceListboxPriceList}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxCustomer(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCustomer}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxGender(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxGender}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxProvince(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProvince}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }

        public IEnumerable<SelectListItem> GetComBoxDistrict(int selectIds, int provinceId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxDistrict}?provinceId={provinceId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxPalletByStoreId(int selectIds, int storeId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxPalletByStoreId}?storeId={storeId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxOrganization(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxOrganization}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);

        }
        public IEnumerable<SelectListItem> GetComBoxUnit(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxUnit}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);

        }
        public IEnumerable<SelectListItem> GetComBoxProduct(int selectIds, int productTypeId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProduct}?organizationId={user.OrganizationId}&productTypeId={productTypeId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxProductIsInventory(int selectIds, int productTypeId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProductIsInventory}?organizationId={user.OrganizationId}&productTypeId={productTypeId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }

        public IEnumerable<SelectListItem> GetComBoxProductNumber(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProductNumber}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxPallet(int selectIds, int storeId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxPallet}?organizationId={user.OrganizationId}&storeId={storeId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxCombo(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxCombo}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);

        }
        public IEnumerable<SelectListItem> GetComBoxAttributes(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxAttributes}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);

        }
        public IEnumerable<SelectListItem> GetComBoxSupplier(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxSupplier}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);

        }
        public IEnumerable<SelectListItem> GetComBoxDepartment(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxDepartment}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxDepartment(List<int> selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxDepartment}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxStaffType(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxStaffType}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxRegency(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxRegency}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public IEnumerable<SelectListItem> GetComBoxStaff(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxStaff}?organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public List<MenuModel> GetMenu(string applicationCode)
        {
            var listMenu = new List<MenuModel>();
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<MenuModel>>>($"{UrlAPI.Url_GetMenu}?applicationCode={applicationCode}&organizationId={user.OrganizationId}&userName={user.Username}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listMenu = res.Data.ListData.ToList();
            }

            return listMenu.ToList();
        }

        public List<ApplicationModel> GetApplicationAll()
        {
            List<ApplicationModel> listModel = new List<ApplicationModel>();
            var res = htmlHelper.GetAsync<ResponseList<List<ApplicationModel>>>($"{UrlAPI.Url_GetAllApplication}?limit={1000}&page={1}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
            }
            return listModel;
        }

        public string GetApplicationStr()
        {
            var listModel = GetApplicationAll();
            string html = string.Empty;
            if (listModel.Count > 0)
            {
                int coutList = listModel.Count;
                int index = 1;

                foreach (var item in listModel)
                {
                    if (index % 2 == 1)
                    {
                        html += "<div class=\"row row-bordered overflow-visible g-0\">";
                    }
                    html += $"<div class=\"dropdown-shortcuts-item col\">"
                         + $"   <span class=\"dropdown-shortcuts-icon rounded-circle mb-2\">"
                         + $"       {item.Icon}"
                         + $"   </span>"
                         + $"   <a href=\"/{item.Code}\" class=\"stretched-link\">{item.Code}</a>"
                         + $"   <small class=\"text-muted mb-0\">{item.Title}</small>"
                         + $"</div>";
                    if (index % 2 == 0 || index == coutList)
                    {
                        html += "</div>";
                    }
                    index++;
                }
            }
            return html;
        }

        public string GetMenuStr(string applicationCode, string currentMenu)
        {
            //var currentMenu = TempData["CurrentMenu"] == null ? "Home" : TempData["CurrentMenu"].ToString();
            string url = null;
            var categories = GetMenu(applicationCode);
            var categoryParents = categories.Where(d => d.ParentId == 0).OrderBy(d => d.Position).ToList();
            var applicationName = GetApplicationAll().FirstOrDefault(z => z.Code == applicationCode).Title;
            StringBuilder strhtml = new StringBuilder();
            //strhtml.Append($"<ul class=\"menu-inner py-1\">");
            strhtml.Append($"<li class=\"menu-header small text-uppercase\"><span class=\"menu-header-text\">{applicationName}</span></li>");

            foreach (var root in categoryParents)
            {
                var childExists = categories.Where(d => d.ParentId > 0 && d.ParentId.Equals(root.Id)).OrderBy(d => d.Position);
                if (!string.IsNullOrEmpty(root.Action))
                {
                    url = "/" + root.Area + "/" + root.Controller + "/" + root.Action;
                }
                else
                {
                    url = "/" + root.Controller + "/" + root.Action;
                }
                if (childExists.Count() > 0)
                {
                    var treeActive = "";
                    if (childExists.Select(x => x.Controller).ToList().Contains(currentMenu))
                    {
                        treeActive = "active open";
                    }
                    strhtml.Append($"<li class=\"menu-item {treeActive}\"><a class=\"menu-link menu-toggle\" href=\"javascript:void(0);\"><i class=\"menu-icon tf-icons {root.Icon} \"></i><div data-i18n=\"{root.Title}\">{root.Title}</div></a>");
                    strhtml.Append(MenuHelper.BuildMenu(categories, root.Id, currentMenu));
                    strhtml.Append("</li>");
                }
                else
                {
                    var treeMenuActive = "";
                    if (root.Controller == currentMenu.ToString())
                    {
                        treeMenuActive = "active";
                    }
                    strhtml.Append($"<li class=\"menu-item {treeMenuActive}\"><a class=\"menu-link\" href=\"{url}\">{root.Title}</a></li>");
                }
            }

            //strhtml.Append("</ul>");

            return strhtml.ToString();
        }
        public async Task<IActionResult> GetDistrictByProvinceId(int provinceId)
        {
            var res = GetComBoxDistrict(0, provinceId);
            return Json(new { Data = res });
        }  
        public async Task<IActionResult> GetProductNumberByOrganizationId(int organizationId)
        {
            var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
            int productTypeId = productType.Data.Id; // Sản phẩm số
            var res =  htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProduct}?organizationId={organizationId}&productTypeId={productTypeId}").GetAwaiter().GetResult();
            return Json(new { Data = res.ToSelectListItems(0) });
        }
        public IEnumerable<SelectListItem> GetProductNumber(int selectIds, int organizationId)
        {
            var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
            int productTypeId = productType.Data.Id; // Sản phẩm số
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxProduct}?organizationId={organizationId}&productTypeId={productTypeId}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public async Task<IActionResult> GetPalletByStoreId(int storeId)
        {
            var res = GetComBoxPalletByStoreId(0, storeId);
            return Json(new { Data = res });
        }

        public async Task<IActionResult> SetOrganization(int organizationId)
        {
            if (HttpContext.Request.Cookies.ContainsKey("AdminUser_tll"))
            {
                var userValue = WebUtility.UrlDecode(HttpContext.Request.Cookies["AdminUser_tll"]);
                UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                userInfo.OrganizationId = organizationId;

                Response.Cookies.Delete("AdminUser_tll");
                //-- add to Cookie User
                HttpContext.Response.Cookies.Append("AdminUser_tll", WebUtility.UrlEncode(JsonSerializer.Serialize(userInfo)));

                //TempData["Message"] = GetScriptShowToast("success", "Áp dụng thành công!");
            }
            return Json(new { Data = "Oke" });
        }

        public async Task<IActionResult> GetOrganization()
        {
            OrganizationModel model = new OrganizationModel();
            var user = GetUser();
            var res = htmlHelper.GetAsync<OrganizationModel>($"{UrlAPI.Url_GetByIdOrganization}?id={user.OrganizationId}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                if (user.OrganizationId != 0)
                    model = res.Data;
            }
            return Json(new { Data = model, retCode = 0, retText = "Thành Công" });
        }
        public async Task<IActionResult> GetUnitComboxJson(int unitId)
        {
            var res = GetComBoxUnit(unitId);
            return Json(new { Data = res });
        }
        public async Task<IActionResult> GetProductComboxJson(int productId, string productTypeCode)
        {
            var user = GetUser();
            if (productTypeCode != null)
            {
                var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code={productTypeCode}").GetAwaiter().GetResult();
                var res = GetComBoxProduct(productId, productType.Data.Id);
                return Json(new { Data = res });
            }
            else
            {
                var res = GetComBoxProduct(productId, 0);
                return Json(new { Data = res });
            }
        }
        public async Task<IActionResult> GetAttributesComboxJson(int attributeId)
        {
            var res = GetComBoxAttributes(attributeId);
            return Json(new { Data = res });
        }
        public async Task<IActionResult> GetComboComboxJson(int comboId)
        {
            var res = GetComBoxCombo(comboId);
            return Json(new { Data = res });
        }
        public async Task<IActionResult> GetCustomerJson(int id)
        {
            var res = htmlHelper.GetAsync<CustomerModel>($"{UrlAPI.Url_GetByIdCustomer}?id={id}").GetAwaiter().GetResult();
            return Json(new { Data = res.Data, retCode = res.RetCode, retText = res.RetText });
        }

        public async Task<IActionResult> GetCustomerByPhoneJson(string phone)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ViewCustomer>>>($"{UrlAPI.Url_GetAllByPhoneStore}?organizationId={user.OrganizationId}&phone={phone}").GetAwaiter().GetResult();
            return Json(new { Data = res.Data.ListData.FirstOrDefault(), retCode = res.RetCode, retText = res.RetText });
        }
        public async Task<IActionResult> GetCustomerByCodeJson(string code)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<CustomerModel>>>($"{UrlAPI.Url_GetAllByPhone}?organizationId={user.OrganizationId}&code={code}").GetAwaiter().GetResult();
            return Json(new { Data = res.Data.ListData.FirstOrDefault(), retCode = res.RetCode, retText = res.RetText });
        }

        public async Task<IActionResult> GetPriceListJson(int id)
        {
            var res = htmlHelper.GetAsync<PriceListModel>($"{UrlAPI.Url_GetByIdPriceList}?id={id}").GetAwaiter().GetResult();
            return Json(new { Data = res.Data, retCode = res.RetCode, retText = res.RetText });
        }
        public async Task<IActionResult> BundledGiftJson(int productId)
        {
            var res = htmlHelper.GetAsync<ProductModel>($"{UrlAPI.Url_GetByIdProduct}?id={productId}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                if (res.Data.IsBundledGift && res.Data.BundledGiftList.Count() > 0)
                    return Json(new { Data = res.Data, retCode = res.RetCode, retText = res.RetText });
                else
                    return Json(new { Data = new object { }, retCode = RetCodeEnum.ApiError, retText = "Không có dữ liệu" });
            }
            return Json(new { Data = new object { }, retCode = res.RetCode, retText = res.RetText });
        }
        public async Task<IActionResult> GetDiscountKindJson(int comboId)
        {
            var res = GetComBoxDiscountKind(comboId);
            return Json(new { Data = res });
        }
        public IEnumerable<SelectListItem> GetComBoxDiscountKind(int selectIds)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<ResponseCombo>>>($"{UrlAPI.Url_GetAllComboboxDiscountKind}").GetAwaiter().GetResult();
            return res.ToSelectListItems(selectIds);
        }
        public StaffModel GetStaffByAccountId(int id)
        {
            StaffModel staffModel = new StaffModel();
            var res = htmlHelper.GetAsync<StaffModel>($"{UrlAPI.Url_GetStafByAccountId}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                staffModel = res.Data;
            }

            return staffModel;
        }

        [HttpPost]
        public IActionResult UploadFile(IList<IFormFile> files, string uploadDirecotroy)
        {

            foreach (IFormFile source in files)
            {
                uploadHelper.UploadDirecotroy = uploadDirecotroy;
                uploadHelper.UploadFile(source);
            }

            return Json(new { retCode = 0, data = $"/{uploadHelper.UploadDirecotroy}/{uploadHelper.FileName}", retText = "Upload thành công!" });
        }

        public string CheckQuantityEnd(ProductFitter fitter, IEnumerable<BillDetailModel> bills)
        {
            var resCheckQuantity = htmlHelper.PostAsyncFitter<ResponseList<List<Product_StoreProcedure>>>($"{UrlAPI.Url_GetAllProductQuantityEnd}" +
               $"?limit={1000000}&page={1}", fitter).GetAwaiter().GetResult();

            string error = string.Empty;
            if (resCheckQuantity.RetCode == RetCodeEnum.Ok)
            {
                var listProduct = resCheckQuantity.Data.ListData.Where(z => bills.Select(z => z.ProductId).Contains(z.Id));
                foreach (var item in bills)
                {
                    var dataProduct = listProduct.Where(z => z.Id == item.ProductId && z.IsInventory).FirstOrDefault();
                    if (dataProduct != null)
                    {
                        if (item.UnitId != dataProduct.UnitId) // Check đơn vị tính
                        {
                            var unitConvert = htmlHelper.GetAsync<UnitConvertedModel>($"{UrlAPI.Url_GetByBasicUnitId}?basicUnitId={dataProduct.UnitId}").GetAwaiter().GetResult();
                            item.Quantity = unitConvert.Data.BasicUnitNumber * item.Quantity;
                            item.QuantityReal = unitConvert.Data.BasicUnitNumber * item.QuantityReal;
                            item.QuantityFail = unitConvert.Data.BasicUnitNumber * item.QuantityFail;
                            if (dataProduct.QuantityReal < item.QuantityReal)
                                error = error + $"{dataProduct.Title} - Tồn kho: {dataProduct.QuantityReal} nhỏ hơn số lượng xuất quy đổi: {item.QuantityReal}.<br/>";
                        }
                        else
                        {
                            if (dataProduct.QuantityReal < item.QuantityReal)
                                error = error + $"{dataProduct.Title} - Tồn kho: {dataProduct.QuantityReal} nhỏ hơn số lượng xuất: {item.QuantityReal}.<br/>";
                        }
                    }
                }
            }
            return error;
        }
    }
}
