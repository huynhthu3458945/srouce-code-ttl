﻿using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class ClientController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public ClientController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                List<ClientModel> listModel = new List<ClientModel>();
                var model = new IndexViewModel<ClientModel>();
                //var res = htmlHelper.GetAsync<ResponseList<List<ClientModel>>>($"{UrlAPI.Url_GetAllClient}?limit={pageItems}&page={page}").GetAwaiter().GetResult();
                //if (res.RetCode == RetCodeEnum.Ok)
                //{
                //    listModel = res.Data.ListData.ToList();

                //    model.ListItems = listModel;
                //    model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/Client/Index?page={0}'");
                //}
               
                ViewData["Client"] = "active";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(int page)
        {
            List<ClientModel> listModel = new List<ClientModel>();
            var model = new IndexViewModel<ClientModel>();
            var res = htmlHelper.GetAsync<ResponseList<List<ClientModel>>>($"{UrlAPI.Url_GetAllClient}?limit={pageItems}&page={page}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel; 
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/Client/Index?page={0}'");
            }
            return PartialView("_TableResult", model);
        }

        public IActionResult Create()
        {
            ClientModel model = new ClientModel();
            ViewData["Client"] = "active";
            ViewBag.IsCreate = true;
            return View(model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new ClientModel();
                var res = htmlHelper.GetAsync<ClientModel>($"{UrlAPI.Url_GetByIdClient}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                }
                ViewBag.IsCreate = false;
                ViewData["Client"] = "active";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Create(ClientModel model)
        {
            try
            {
                model.CreateBy = GetUser().Id;
                model.CreateOn = DateTime.Now.Date;
                model.ModifiedOn = DateTime.Now.Date;
                var res = htmlHelper.PostAsync<ClientModel>(UrlAPI.Url_AddClient, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Redirect($"/Client/Edit/{model.Id }");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    return Redirect($"/Client/Edit/{model.Id }");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(ClientModel model)
        {
            try
            {
                model.ModifiedBy = GetUser().Id;
                model.ModifiedOn = DateTime.Now.Date;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateClient, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);

                }
                return Redirect($"/Client/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteClient}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
