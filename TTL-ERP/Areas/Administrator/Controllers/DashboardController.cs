﻿using TTL_ERP.Helpers;
using TTL_ERP.Models;
using TTL_ERP.Models.API;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Helpers.API;

namespace TTL_ERP.Areas.Administrator.Controllers
{

    [Area("Administrator")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class DashboardController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public DashboardController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public IActionResult Index()
        {
            var user = GetUser();
            if(user.Role.Equals("Administrator_full"))
            {
                return RedirectToAction("IndexSystem");
            }else if (user.Role.Equals("SupperAdmin")) // Supper admin  
            {
                return RedirectToAction("IndexSupperAdmin");
            }
            else if (user.Role.Equals("Normal")) // Nhân viên
            {
                if (user.OrganizationId == 0)
                    return RedirectToAction("Organization");
                return View();
            }
            return View();
        }
        public IActionResult IndexSystem()
        {
            return View();
        }
        public IActionResult IndexSupperAdmin()
        {
            return View();
        }

        public async Task<IActionResult> Organization(int page = 1)
        {
            try
            {
                List<OrganizationModel> listModel = new List<OrganizationModel>();
                var model = new IndexViewModel<OrganizationModel>();
                var user = GetUser();
                var res = htmlHelper.GetAsync<ResponseList<List<OrganizationModel>>>($"{UrlAPI.Url_GetAllOrganizationByUserName}?userName={user.Username}&limit={pageItems}&page={page}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    listModel = res.Data.ListData.ToList();

                    model.ListItems = listModel;
                    model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/Dashboard/Organization?page={0}'");
                    model.OrganizationId = user.OrganizationId;
                }

                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadDataOrganization(int page)
        {
            List<OrganizationModel> listModel = new List<OrganizationModel>();
            var model = new IndexViewModel<OrganizationModel>();
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<OrganizationModel>>>($"{UrlAPI.Url_GetAllOrganizationByUserName}?userName={user.Username}&limit={pageItems}&page={page}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/Dashboard/Organization?page={0}'"); 
                model.OrganizationId = user.OrganizationId;
            }
            return PartialView("_TableOrganizationResult", model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public ActionResult Accessdenied()
        {
            return View("_Accessdenied");
        }
        public ActionResult AccessdeniedJson()
        {
            return Json(new { retCode = RetCodeEnum.ApiError, url = "/Dashboard/Accessdenied", retText = "Bạn không có quyền xem trang này bằng thông tin đăng nhập mà bạn đã cung cấp khi đăng nhập. <br /> Vui lòng liên hệ với quản trị viên trang web của bạn." });
        }
    }
}
