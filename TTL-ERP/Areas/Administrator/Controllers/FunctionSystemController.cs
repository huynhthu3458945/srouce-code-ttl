﻿using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Helper;
using Microsoft.AspNetCore.Mvc.Rendering;
using TTL_ERP.Models;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class FunctionSystemController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public FunctionSystemController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<FunctionSystemModel>();
                model.ParenList = GetComBoxTreeFunctionSystem(0, 10000);
                ViewData["FunctionSystem"] = "active";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(FunctionSystemFitter fitter)
        {
            List<FunctionSystemModel> listModel = new List<FunctionSystemModel>();
            var model = new IndexViewModel<FunctionSystemModel>();
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<FunctionSystemModel>>>($"{UrlAPI.Url_GetAllFunctionSystem}"+
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/Client/Index?page={0}'");
            }
            return PartialView("_TableResult", model);
        }


        public IActionResult Create()
        {
            FunctionSystemModel model = new FunctionSystemModel();
            model.ParenList = GetComBoxTreeFunctionSystem(0, 10000);
            model.ApplicationList = GetComBoxApplication(0);
            ViewBag.IsCreate = true;
            ViewData["FunctionSystem"] = "active";
            return View(model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new FunctionSystemModel();
                var res = htmlHelper.GetAsync<FunctionSystemModel>($"{UrlAPI.Url_GetByIdFunctionSystem}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.ParenList = GetComBoxTreeFunctionSystem(model.ParentId, id);
                    model.ParenList = model.ParenList.Where(z => !z.Value.Equals(id.ToString()));
                    model.ApplicationList = GetComBoxApplication(model.ApplicationId);
                    model.ApplicationSelectId = model.ApplicationId.ToString();
                    model.ParentIdSelectId = model.ParentId.ToString();
                }
                ViewBag.IsCreate = false;
                ViewData["FunctionSystem"] = "active";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Create(FunctionSystemModel model)
        {
            try
            {
                model.CreateBy = GetUser().Id;
                model.CreateOn = DateTime.Now.Date;
                model.ModifiedOn = DateTime.Now.Date;
                model.ApplicationId = int.Parse(model.ApplicationSelectId);
                int parentId = 0;
                Int32.TryParse(model.ParentIdSelectId, out parentId);
                model.ParentId = parentId;
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddFunctionSystem, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Redirect($"/FunctionSystem/Edit/{model.Id }");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    return Redirect($"/FunctionSystem/Edit/{model.Id }");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(FunctionSystemModel model)
        {
            try
            {
                model.ModifiedBy = GetUser().Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.ApplicationId = int.Parse(model.ApplicationSelectId);
                int parentId = 0;
                Int32.TryParse(model.ParentIdSelectId, out parentId);
                model.ParentId = parentId;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateFunctionSystem, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);

                }
                return Redirect($"/FunctionSystem/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
               
                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteFunctionSystem}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode,  retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
