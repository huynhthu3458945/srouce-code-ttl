﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Text.Json;
using TTL_ERP.Models;
using System.Diagnostics;
using System.Text;
using System.Net.Http;
using TTL_ERP.Common;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using TTL_ERP.Models.SystemMode;
using TTL_ERP.Helpers;
using Microsoft.AspNetCore.Mvc.Rendering;
using TTL_ERP.Areas.Helper;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class LoginController : BaseController
    {
        private HttpClientHandler clientHandler = new HttpClientHandler();
        private readonly HttpClientHelper htmlHelper;
        private readonly AppSettings appSettings;
        private UserLoginVMs userVMs;
        public LoginController(AppSettings _appsettings, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            clientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            appSettings = _appsettings;
            this.htmlHelper = htmlHelper;
        }

        public ActionResult Index()
        {
            return View();
        }

        public class UserLogin
        {
            public string userName { get; set; }
            public string passWord { get; set; }
            public int OrganizationId { get; set; }
        }
        [HttpPost]
        public async Task<IActionResult> Index(string u, string p, int o, string url)
        {
            var login = new UserLogin()
            {
                userName = u,
                passWord = p,
                OrganizationId = o
            };
            byte[] btoken;

            if (HttpContext.Session.TryGetValue("access_token", out btoken))
            {
                return RedirectToAction("Index", "Dashboard", new { area = "Administrator" });
            }
            try
            {
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();

                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    HttpResponseMessage response = await httpClient.PostAsJsonAsync($"{appSettings.DomainAPI}{appSettings.PortAPI}{UrlAPI.Url_Login}", login);
                    if (response.IsSuccessStatusCode)
                    {
                        var user = response.Content.ReadAsAsync<GetResponse<UserLoginVMs>>(new[] { new JsonMediaTypeFormatter() }).Result;
                        //if (!string.IsNullOrEmpty(token.Data.DivisionID))
                        //{
                        //    HttpContext.Session.Set("curr_DivisionID", Encoding.ASCII.GetBytes(token.Data.DivisionID));
                        //}
                        //if (token.Data.Permissions != null)
                        //{
                        //    var dataPermission = JsonConvert.SerializeObject(token.Data.Permissions.FirstOrDefault());
                        //    var dataArray = Encoding.UTF8.GetBytes(dataPermission);
                        //    HttpContext.Session.Set("user_permission", dataArray);
                        //}
                        if (user.RetCode == RetCodeEnum.Ok)
                        {
                            if (!string.IsNullOrEmpty(user.Data.Token))
                            {
                                HttpContext.Session.Set("access_token", Encoding.ASCII.GetBytes(user.Data.Token));
                            }
                            //-- add to Cookie User
                            HttpContext.Response.Cookies.Append("AdminUser_tll", WebUtility.UrlEncode(JsonSerializer.Serialize(user.Data)));
                            ////-- add to Cookie User + 
                            if (user.Data.Role.Equals("Administrator_full")) // Admin hệ thống
                                return Json(new { retCode = user.RetCode, url = "/Administrator/Dashboard/IndexSystem", retText = "Đăng nhập thành công!" });
                            else if (user.Data.Role.Equals("SupperAdmin")) // Supper admin  
                                return Json(new { retCode = user.RetCode, url = "/Administrator/Dashboard/IndexSupperAdmin", retText = "Đăng nhập thành công!" });
                            else if (user.Data.Role.Equals("Normal")) // Nhân viên
                                return Json(new { retCode = user.RetCode, url = "/Administrator/Dashboard/Index", retText = "Đăng nhập thành công!" });
                        }
                        else
                            return Json(new { retCode = user.RetCode, url = "", retText = user.RetText });
                    }
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi hệ thống" });
            }
        }
        public ActionResult Logout()
        {
            if (HttpContext.Request.Cookies["AdminUser_tll"] != null)
            {
                Response.Cookies.Delete("AdminUser_tll");
            }
            byte[] btoken;
            if (HttpContext.Session.TryGetValue("access_token", out btoken))
            {
                HttpContext.Session.Clear();
            }
            return RedirectToAction("Index", "Login", new { area = "Administrator" });
        }
        public async Task<IActionResult> GetOrganizationByUserName(string userName)
        {
            var res = await GetComBoxOrganizationByUserName(userName);
            return Json(new { Data = res });
        }
        public async Task<GetResponse<ResponseList<List<ResponseCombo>>>> GetAPIOrganizationByUserName(string userName)
        {
            using (var httpClient = new HttpClient(clientHandler, false))
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                httpClient.Timeout = TimeSpan.FromMinutes(10);

                var response = await httpClient.GetAsync($"{appSettings.DomainAPI}{appSettings.PortAPI}{UrlAPI.Url_GetAllComBoxOrganizationByUserName}?userName={userName}");

                var resResponse = await response.Content.ReadAsAsync<GetResponse<ResponseList<List<ResponseCombo>>>>();
                return resResponse;
            }
        }

        public async Task<IEnumerable<SelectListItem>> GetComBoxOrganizationByUserName(string userName)
        {
            var res = await GetAPIOrganizationByUserName(userName);
            return res.ToSelectListItems(0);
        }
    }
}
