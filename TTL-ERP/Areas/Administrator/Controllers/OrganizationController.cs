﻿using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Helper;
using Microsoft.AspNetCore.Mvc.Rendering;
using TTL_ERP.Models;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class OrganizationController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        public OrganizationController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int limit = 100, int page = 1)
        {
            try
            {
                List<OrganizationModel> listModel = new List<OrganizationModel>();
                var res = htmlHelper.GetAsync<ResponseList<List<OrganizationModel>>>($"{UrlAPI.Url_GetAllOrganization}?limit={limit}&page={page}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    foreach (var item in res.Data.ListData.ToList())
                    {
                        item.ClientName = htmlHelper.GetAsync<ClientModel>($"{UrlAPI.Url_GetByIdClient}?id={item.ClientId}").GetAwaiter().GetResult().Data.Title;
                    }
                    listModel = res.Data.ListData.ToList();
                }
                ViewData["Organization"] = "active";
                return View(listModel);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public IActionResult Create()
        {
            OrganizationModel model = new OrganizationModel();
            model.ClientList = GetComBoxClient(0);
            var applicationList = GetComBoxApplication(0).ToList();
            applicationList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ApplicationList = applicationList;
            ViewBag.IsCreate = true;
            ViewData["Organization"] = "active";
            return View(model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new OrganizationModel();
                var res = htmlHelper.GetAsync<OrganizationModel>($"{UrlAPI.Url_GetByIdOrganization}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.ClientList = GetComBoxClient(id);
                    model.ClientIdSelectId = model.ClientId.ToString();
                    var applicationList = GetComBoxApplication(0).ToList();
                    applicationList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                    model.ApplicationList = applicationList;
                }
                ViewBag.IsCreate = false;
                ViewData["Organization"] = "active";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Create(OrganizationModel model)
        {
            try
            {
                model.CreateBy = GetUser().Id;
                model.CreateOn = DateTime.Now.Date;
                model.ModifiedOn = DateTime.Now.Date;
                model.ClientId = int.Parse(model.ClientIdSelectId);
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<OrganizationModel>($"{UrlAPI.Url_GetByCodeOrganization}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã công ty đã tồn tại" });
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddOrganization, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = "Thêm mới thành công!" });
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(OrganizationModel model)
        {
            try
            {
                model.ModifiedBy = GetUser().Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.ClientId = int.Parse(model.ClientIdSelectId);
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateOrganization, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    //TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = "Cập nhật thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                }
                //return Redirect($"/Organization/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteOrganization}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetGetTreeFunctionSysTem(int organizationId, int applicationId)
        {
            var res = htmlHelper.GetAsync<List<FunctionSystemModelTree>>($"{UrlAPI.Url_GetTreeFunctionSysTem}?organizationId={organizationId}&applicationId={applicationId}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(res.Data);
            }
            else
            {
                return Json(new { retCode = res.RetCode, retText = res.RetText });
            }
        }

        [HttpGet]
        public async Task<IActionResult> CheckExistUserName(int id, string userName)
        {
            if (id == 0)
            {
                var res = htmlHelper.GetAsync<AccountModel>($"{UrlAPI.Url_CheckExistUserName}?userName={userName}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    if (res.Data != null)
                    {
                        return Json(new { retCode = 1, retText = "Tài khoản đã tồn tại" });
                    }
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            return Json(new { retCode = RetCodeEnum.Ok, retText = "Sửa không cần check" });
        }


        public class FunctionSystemModelTree
        {
            public int id { get; set; }
            public int ParentId { get; set; }
            public State state { get; set; }
            public string text { get; set; }
            public List<FunctionSystemModelTree> children { get; set; }
            public FunctionSystemModelTree()
            {
                children = new List<FunctionSystemModelTree>();
            }
        }
        public class State
        {
            public bool selected { get; set; }
            public bool opened { get { return true; } }
        }
    }
}
