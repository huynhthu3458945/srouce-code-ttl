﻿using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Helper;
using Microsoft.AspNetCore.Mvc.Rendering;
using TTL_ERP.Models;
using TTL_ERP.Areas.WM.Models;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class ProductMapsController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public ProductMapsController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<ProductMapsModel>();
                //var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
                //int productTypeId = productType.Data.Id; // Sản phẩm số
                //model.ProductList = GetComBoxProduct(0, productTypeId);

                var productList = new List<SelectListItem>();
                productList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.ProductList = productList;

                var keyProductList = GetComBoxProductNumber(0).ToList();
                keyProductList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.KeyProductList = keyProductList;

                var organizationList = GetComBoxOrganization(0).ToList();
                organizationList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.OrganizationList = organizationList;

                ViewData["ProductMaps"] = "active";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(ProductMapsFitter fitter)
        {
            List<ProductMapsModel> listModel = new List<ProductMapsModel>();
            var model = new IndexViewModel<ProductMapsModel>();
            fitter.OrganizationId = fitter.OrganizationIdSearch;
            fitter.ProductId = fitter.ProductIdSearch;
            fitter.KeyProductId = fitter.KeyProductIdSearch;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ProductMapsModel>>>($"{UrlAPI.Url_GetAllProductMaps}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='");
            }
            return PartialView("_TableResult", model);
        }


        public IActionResult Create()
        {
            ProductMapsModel model = new ProductMapsModel();
            //var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
            //int productTypeId = productType.Data.Id; // Sản phẩm số
            model.ProductList = new List<SelectListItem>();
            var listProduct = GetComBoxProductNumber(0);

            ProductMapsFitter fitter = new ProductMapsFitter();
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ProductMapsModel>>>($"{UrlAPI.Url_GetAllProductMaps}" +
            $"?limit=1000&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                var allProcutNumber = res.Data.ListData.ToList();
                if(allProcutNumber.Count > 0)
                {
                    listProduct = listProduct.Where(z => !allProcutNumber.Select(x => x.KeyProductId.ToString()).Contains(z.Value));
                }
            }

            model.KeyProductList = listProduct;
            model.OrganizationList = GetComBoxOrganization(0);
            ViewBag.IsCreate = true;
            ViewData["ProductMaps"] = "active";
            return PartialView("_AddOrEdit", model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new ProductMapsModel();
                var res = htmlHelper.GetAsync<ProductMapsModel>($"{UrlAPI.Url_GetByIdProductMaps}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
                    int productTypeId = productType.Data.Id; // Sản phẩm số
                    model.ProductList = GetProductNumber(model.ProductId, model.OrganizationId); 
                    var listProduct = GetComBoxProductNumber(0).ToList();
                    var tempProduct = listProduct.FirstOrDefault(z => z.Value == model.KeyProductId.ToString());

                    ProductMapsFitter fitter = new ProductMapsFitter();
                    var resAllProduct = htmlHelper.PostAsyncFitter<ResponseList<List<ProductMapsModel>>>($"{UrlAPI.Url_GetAllProductMaps}" +
                    $"?limit=10000&page={fitter.Page}", fitter).GetAwaiter().GetResult();
                    if (resAllProduct.RetCode == RetCodeEnum.Ok)
                    {
                        var allProcutNumber = resAllProduct.Data.ListData.ToList();
                        if (allProcutNumber.Count > 0)
                        {
                            listProduct = listProduct.Where(z => !allProcutNumber.Select(x => x.KeyProductId.ToString()).Contains(z.Value)).ToList();
                            listProduct.Add(tempProduct);
                        }
                    }

                    model.KeyProductList = listProduct;
                    model.OrganizationList = GetComBoxOrganization(model.OrganizationId);
                }
                ViewBag.IsCreate = false;
                ViewData["ProductMaps"] = "active";
                return PartialView("_AddOrEdit", model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        [HttpPost]
        public IActionResult AddOrEdit(ProductMapsModel model)
        {
            try
            {
                if (model.Id == 0) // add
                {
                    var user = GetUser();
                    model.CreateBy = GetUser().Id;
                    model.CreateOn = DateTime.Now.Date;
                    model.ModifiedOn = DateTime.Now.Date;
                    //model.OrganizationId = user.OrganizationId;
                    // check tồn tại mã
                    var resCheck = htmlHelper.GetAsync<ProductMapsModel>($"{UrlAPI.Url_GetByProductMapsbyPorductId}?productId={model.ProductId}").GetAwaiter().GetResult();
                    if (resCheck.Data != null)
                        return Json(new { retCode = 1, retText = "Sản phẩm đã liên kết" });
                    var res = htmlHelper.PostAsync<ProductMapsModel>(UrlAPI.Url_AddProductMaps, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        model = res.Data;
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Thêm mới thành công!" });
                        //return Redirect($"?id={model.Id }");
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                        //return Redirect($"?id={model.Id }");
                    }

                }
                else // edit
                {
                    model.ModifiedBy = GetUser().Id;
                    model.ModifiedOn = DateTime.Now.Date;
                    var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateChannel, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Cập nhật thành công!" });
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        [HttpPost]
        public IActionResult Create(ProductMapsModel model)
        {
            try
            {
                model.CreateBy = GetUser().Id;
                model.CreateOn = DateTime.Now.Date;
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddProductMaps, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Redirect($"/ProductMaps/Edit/{model.Id }");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    return Redirect($"/ProductMaps/Edit/{model.Id }");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(ProductMapsModel model)
        {
            try
            {
                model.ModifiedBy = GetUser().Id;
                model.ModifiedOn = DateTime.Now.Date;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateProductMaps, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);

                }
                return Redirect($"/ProductMaps/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteProductMaps}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
