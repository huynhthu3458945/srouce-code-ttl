﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.Administrator.Controllers
{
    [ServiceFilter(typeof(AuthorizeClient))]
    public class RoleController : BaseController
    {
        private readonly IMapper mapper;
        private readonly HttpClientHelper htmlHelper;
        public RoleController(IMapper mapper, HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.mapper = mapper;
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int limit = 100, int page = 1)
        {
            try
            {
                var user = GetUser();
                List<RoleModel> listModel = new List<RoleModel>();
                var res = htmlHelper.GetAsync<ResponseList<List<RoleModel>>>($"{UrlAPI.Url_GetAllRole}?organizationId={user.OrganizationId}&limit={limit}&page={page}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    listModel = res.Data.ListData.ToList();
                }
                ViewData["Role"] = "active";
                return View(listModel);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public IActionResult Create()
        {
            RoleModel model = new RoleModel();
            var applicationList = GetComBoxApplication(0).ToList();
            applicationList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ApplicationList = applicationList;
            ViewBag.IsCreate = true;
            ViewData["Role"] = "active";
            return View(model);
        }
        public IActionResult Edit(int id)
        {
            try
            {
                var model = new RoleModel();
                var res = htmlHelper.GetAsync<RoleModel>($"{UrlAPI.Url_GetByIdRole}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    var applicationList = GetComBoxApplication(0).ToList();
                    applicationList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                    model.ApplicationList = applicationList;
                }
                ViewBag.IsCreate = false;
                ViewData["Role"] = "active";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        [HttpPost]
        public IActionResult Create(RoleModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<RoleModel>($"{UrlAPI.Url_GetByCodeRole}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã vai trò đã tồn tại" });
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddRole, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Role/Edit/{model.Id }", retText = "Thêm mới thành công!" });
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(RoleModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateRole, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    //TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/Role/Edit/{model.Id }", retText = "Cập nhật thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                }
                //return Redirect($"/Organization/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteOrganization}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetTreeFunctionRole(int roleId, int applicationId)
        {
            var user = GetUser();
            var res = htmlHelper.GetAsync<List<FunctionOrganizationModelTree>>($"{UrlAPI.Url_GetTreeFunctionOrganization}?roleId={roleId}&applicationId={applicationId}&organizationId={user.OrganizationId}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(res.Data);
            }
            else
            {
                return Json(new { retCode = res.RetCode, retText = res.RetText });
            }
        }

        public class FunctionOrganizationModelTree
        {
            public int id { get; set; }
            public State state { get; set; }
            public string text { get; set; }
            public List<FunctionOrganizationModelTree> children { get; set; }
            public FunctionOrganizationModelTree()
            {
                children = new List<FunctionOrganizationModelTree>();
            }
        }
        public class State
        {
            public bool selected { get; set; }
            public bool opened { get { return true; } }
        }
    }
}
