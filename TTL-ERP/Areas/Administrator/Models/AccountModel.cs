﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.Administrator.Models
{
    public class AccountModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
        public List<TreeRoleAcc> TreeRoleAccs { get; set; }
    }
    public class TreeRoleAcc
    {
        public int Id { get; set; }
        public bool IsCheck { get; set; }
    }
}
