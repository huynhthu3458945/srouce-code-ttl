﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Areas.Administrator.Models
{
    public class AccountPortalRep
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Roles { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
        public int? CreateBy { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Identitycard { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
        public string CityBank { get; set; }
    }
}
