﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.Administrator.Models
{
    public class FunctionSystemModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public string Note { get; set; }
        public string LinkName { get; set; }
        public int Position { get; set; }
        public bool Status { get; set; }
        public bool IsShow { get; set; }
        public int ParentId { get; set; }
        public string ParentIdSelectId { get; set; }
        public string ParentName { get; set; }
        public string LinkParen { get; set; }
        public IEnumerable<SelectListItem> ParenList { get; set; }
        public int ApplicationId { get; set; }
        public string ApplicationSelectId { get ; set; }
        public IEnumerable<SelectListItem> ApplicationList { get; set; }
    }
    public class FunctionSystemTree
    {
        public int Id { get; set; }
        public string Link { get; set; }
    }
    public class FunctionSystemFitter
    {
        public string Title { get; set; }
        public int Page { get; set; }
        public int ParentId { get; set; }
    }
}
