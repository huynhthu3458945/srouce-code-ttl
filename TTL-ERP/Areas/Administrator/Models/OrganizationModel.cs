﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.Administrator.Models
{
    public class OrganizationModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string OwnerName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientIdSelectId { get; set; }
        public string ParentName { get; set; }
        public string LinkParen { get; set; }
        public string Logo { get; set; }
        public IFormFile LogoImg { get; set; }
        public string Icon { get; set; }
        public List<RoleModel> Roles { get; set; }
        public IEnumerable<SelectListItem> ClientList { get; set; }
        public int ApplicationSelectId { get; set; }
        public IEnumerable<SelectListItem> ApplicationList { get; set; }
        public List<TreeFuncSystem> TreeFuncSystems { get; set; }
    }

   public class TreeFuncSystem
    {
        public int Id { get; set; }
        public bool IsCheck { get; set; }
    }
}
