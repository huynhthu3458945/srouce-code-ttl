﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.Administrator.Models
{
    public class ProductMapsModel : BaseEntityModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public IEnumerable<SelectListItem> ProductList { get; set; }
        public int KeyProductId { get; set; }
        public string KeyProductName { get; set; }
        public IEnumerable<SelectListItem> KeyProductList { get; set; }
        public int OrganizationId { get; set; }
        public IEnumerable<SelectListItem> OrganizationList { get; set; }
        public bool IsDelete { get; set; }
    }

    public class ProductMapsFitter
    {
        public int ProductId { get; set; }
        public int ProductIdSearch { get; set; }
        public int KeyProductId { get; set; }
        public int KeyProductIdSearch { get; set; }
        public int OrganizationId { get; set; }
        public int OrganizationIdSearch { get; set; }
        public int Page { get; set; }
    }
}
