﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.Administrator.Models
{
    public class RoleModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Position { get; set; }
        public int ApplicationSelectId { get; set; }
        public IEnumerable<SelectListItem> ApplicationList { get; set; }
        public int OrganizationId { get; set; }
        public List<TreeFunc> TreeFuncs { get; set; }
    }

    public class TreeFunc
    {
        public int Id { get; set; }
        public bool IsCheck { get; set; }
    }
}
