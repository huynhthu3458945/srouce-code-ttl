﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.CRM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.CRM.Controllers
{
    [Area("CRM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class CustomerController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public CustomerController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public IActionResult Index()
        {
            List<CustomerModel> listModel = new List<CustomerModel>();
            var model = new IndexViewModel<CustomerModel>();
            var customerTypeList = GetComBoxCustomerType(0).ToList();
            customerTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.CustomerTypeList = customerTypeList;

            var customerStatusList = GetComBoxCustomerStatus(0).ToList();
            customerStatusList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.CustomerStatusList = customerStatusList;

            TempData["CurrentMenu"] = "Customer";
            return View(model);
        }
        public PartialViewResult LoadData(CustomerFitter customerFitter)
        {
            List<CustomerModel> listModel = new List<CustomerModel>();
            var model = new IndexViewModel<CustomerModel>();
            var user = GetUser();
            customerFitter.OrganizationId = user.OrganizationId;
            customerFitter.CreateBy = user.Id;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<CustomerModel>>>($"{UrlAPI.Url_GetAllCustomer}" +
                $"?limit={pageItems}&page={customerFitter.Page}", customerFitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, customerFitter.Page, "href='/CustomerStatus/Index?page={0}'");
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult Create()
        {

            TempData["CurrentMenu"] = "Customer";
            CustomerModel model = new CustomerModel();
            model.CustomerTypeList = GetComBoxCustomerType(0);
            model.CustomerStatusList = GetComBoxCustomerStatus(0);
            model.GenderId = 1;
            model.GenderList = GetComBoxGender(0);
            model.ProvinceList = GetComBoxProvince(model.GenderId);
            model.DistrictList = GetComBoxDistrict(0, 0);
            model.ContactList = new List<ContactModel>();
            var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeCustomer}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.Code = res.Data;
            }
            ViewBag.IsCreate = true;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(CustomerModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.Ref_Id = user.Id;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<CustomerModel>($"{UrlAPI.Url_GetByCodeCustomer}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã khách hàng đã tồn tại" });
                model.ContactList = model.Details;
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddCustomer, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/CRM/Customer/Edit?id={model.Id }", urlIndex = "/CRM/Customer", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public IActionResult Edit(int id)
        {
            try
            {
                TempData["CurrentMenu"] = "Customer";
                var model = new CustomerModel();
                var res = htmlHelper.GetAsync<CustomerModel>($"{UrlAPI.Url_GetByIdCustomer}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.Details = res.Data.ContactList;
                    model.CustomerTypeList = GetComBoxCustomerType(model.CustomerTypeId);
                    model.CustomerStatusList = GetComBoxCustomerStatus(model.CustomerStatusId);
                    model.ProvinceList = GetComBoxProvince(model.ProvinceId);
                    model.DistrictList = GetComBoxDistrict(model.DistrictId, model.ProvinceId);
                    model.GenderList = GetComBoxGender(model.GenderId);
                }
                ViewBag.IsCreate = false;
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Edit(CustomerModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.Ref_Id = user.Id;
                model.ContactList = model.Details;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateCustomer, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/CRM/Customer/Edit?id={model.Id}", urlIndex = "/CRM/Customer", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteCustomer}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetCodeMax()
        {
            try
            {
                var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeCustomer}").GetAwaiter().GetResult();
                return Json(new { retCode = res.RetCode, retText = res.RetText, code = res.Data });
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
