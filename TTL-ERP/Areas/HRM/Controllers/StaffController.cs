﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Areas.HRM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.HRM.Controllers
{
    [Area("HRM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class StaffController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public StaffController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public IActionResult Index()
        {
            List<StaffModel> listModel = new List<StaffModel>();
            var model = new IndexViewModel<StaffModel>();
            var departmentList = GetComBoxDepartment(0).ToList();
            departmentList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.DepartmentList = departmentList;

            var staffTypeList = GetComBoxStaffType(0).ToList();
            staffTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.StaffTypeList = staffTypeList;

            var regencyList = GetComBoxRegency(0).ToList();
            regencyList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.RegencyList = regencyList;

            TempData["CurrentMenu"] = "Staff";
            return View(model);
        }

        public PartialViewResult LoadData(StaffFitter staffFitter)
        {
            List<StaffModel> listModel = new List<StaffModel>();
            var model = new IndexViewModel<StaffModel>();
            var user = GetUser();
            staffFitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<StaffModel>>>($"{UrlAPI.Url_GetAllStaff}" +
                $"?limit={pageItems}&page={staffFitter.Page}", staffFitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, staffFitter.Page, "href=''");
            }
            return PartialView("_TableResult", model);
        }

        public IActionResult Create()
        {

            TempData["CurrentMenu"] = "Staff";
            StaffModel model = new StaffModel();

            var newListSelect = new List<int>();
            model.DepartmentList = GetComBoxDepartment(newListSelect);
            model.StaffTypeList = GetComBoxStaffType(0);
            model.GenderId = 1;
            model.GenderList = GetComBoxGender(0);
            model.RegencyList = GetComBoxRegency(0);
            model.ManagerList = GetComBoxStaff(0);
            
            ViewBag.IsCreate = true;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(StaffModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<StaffModel>($"{UrlAPI.Url_GetByCodeStaff}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã nhân viên đã tồn tại" });

                // check tồn tại mã
                var resCheckUserName = htmlHelper.GetAsync<AccountModel>($"{UrlAPI.Url_CheckUserNameStaff}?userName={model.UserName}").GetAwaiter().GetResult();
                if (resCheckUserName.Data != null)
                    return Json(new { retCode = 1, retText = "Tài khoản đã tồn tại" });

                model.Departments = model.DepartmentSelectId.Select(int.Parse).ToList();
                var res = htmlHelper.PostAsync(UrlAPI.Url_AddStaff, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/HRM/Staff/Edit?id={model.Id }", urlIndex = "/HRM/Staff", retText = "Thêm mới thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public IActionResult Edit(int id)
        {
            try
            {
                TempData["CurrentMenu"] = "Staff";
                var model = new StaffModel();
                var res = htmlHelper.GetAsync<StaffModel>($"{UrlAPI.Url_GetByIdStaff}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.DepartmentList = GetComBoxDepartment(model.Departments);
                    model.StaffTypeList = GetComBoxStaffType(model.StaffTypeId);
                    model.RegencyList = GetComBoxRegency(model.RegencyId);
                    model.ManagerList = GetComBoxStaff(model.ManagerId);
                    model.GenderList = GetComBoxGender(model.GenderId);
                }
                ViewBag.IsCreate = false;
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Edit(StaffModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.Departments = model.DepartmentSelectId.Select(int.Parse).ToList();
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateStaff, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/HRM/Staff/Edit?id={model.Id}", urlIndex = "/HRM/Staff", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteStaff}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
