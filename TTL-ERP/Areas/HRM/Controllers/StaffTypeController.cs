﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.HRM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.HRM.Controllers
{
    [Area("HRM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class StaffTypeController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public StaffTypeController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<StaffTypeModel>();
                TempData["CurrentMenu"] = "StaffType";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(int page, string search = "")
        {
            List<StaffTypeModel> listModel = new List<StaffTypeModel>();
            var model = new IndexViewModel<StaffTypeModel>();
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<StaffTypeModel>>>($"{UrlAPI.Url_GetAllStaffType}?organizationId={user.OrganizationId}&limit={pageItems}&page={page}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.Where(z =>
                (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Code.Contains(search)))
                || (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Title.Contains(search))))
                    .ToList()
                ;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/StaffType/Index?page={0}'");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }

        public IActionResult Create()
        {
            StaffTypeModel model = new StaffTypeModel();
            TempData["CurrentMenu"] = "StaffType";
            ViewBag.IsCreate = true;
            return PartialView("_AddOrEdit", model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new StaffTypeModel();
                var res = htmlHelper.GetAsync<StaffTypeModel>($"{UrlAPI.Url_GetByIdStaffType}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                }
                ViewBag.IsCreate = false;
                TempData["CurrentMenu"] = "StaffType";
                return PartialView("_AddOrEdit", model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult AddOrEdit(StaffTypeModel model)
        {
            try
            {
                if (model.Id == 0) // add
                {
                    var user = GetUser();
                    model.CreateBy = GetUser().Id;
                    model.CreateOn = DateTime.Now.Date;
                    model.ModifiedOn = DateTime.Now.Date;
                    model.OrganizationId = user.OrganizationId;
                    // check tồn tại mã
                    var resCheck = htmlHelper.GetAsync<StaffTypeModel>($"{UrlAPI.Url_GetByCodeStaffType}?code={model.Code}").GetAwaiter().GetResult();
                    if (resCheck.Data != null)
                        return Json(new { retCode = 1, retText = "Mã loại tồn tại" });
                    var res = htmlHelper.PostAsync<StaffTypeModel>(UrlAPI.Url_AddStaffType, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        model = res.Data;
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Thêm mới thành công!" });
                        //return Redirect($"?id={model.Id }");
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                        //return Redirect($"?id={model.Id }");
                    }

                }
                else // edit
                {
                    model.ModifiedBy = GetUser().Id;
                    model.ModifiedOn = DateTime.Now.Date;
                    var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateStaffType, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Cập nhật thành công!" });
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });

                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteStaffType}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
