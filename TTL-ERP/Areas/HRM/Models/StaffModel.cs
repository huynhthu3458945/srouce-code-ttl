﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.HRM.Models
{
    public class StaffModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public IFormFile AvatarImg { get; set; }
        public string Thumb { get; set; }
        public IFormFile ThumbImg { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Skype { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Viber { get; set; }
        public string Zalo { get; set; }
        public bool Status { get; set; }
        public int GenderId { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }
        public string Birthday { get; set; }
        public int StaffTypeId { get; set; }
        public IEnumerable<SelectListItem> StaffTypeList { get; set; }
        public int OrganizationId { get; set; }
        public int RegencyId { get; set; }
        public IEnumerable<SelectListItem> RegencyList { get; set; }
        public int ManagerId { get; set; }
        public IEnumerable<SelectListItem> ManagerList { get; set; }
        public int AccountId { get; set; }
        public string UserName { get; set; }
        public bool HasAccount { get; set; }
        public string GenderName { get; set; }
        public string StaffTypeName { get; set; }
        public List<int> Departments { get; set; }
        public List<string> DepartmentSelectId { get; set; }
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
    }
    public class StaffFitter
    {
        public int Page { get; set; }
        public int DepartmentId { get; set; }
        public int StaffTypeId { get; set; }
        public int RegencyId { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
    }
}
