﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.Helper
{
    public static class SelectListExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectListItems(this GetResponse<ResponseList<List<ResponseCombo>>> respondataList, int selectedId)
        {
            if (respondataList.RetCode == RetCodeEnum.Ok)
            {
                return respondataList.Data.ListData.Select(item => new SelectListItem
                {
                    Selected = (item.Id == selectedId),
                    Text = item.Text,
                    Value = item.Id.ToString()
                });
            }
            else
            {
                return new List<SelectListItem>();
            }
            
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this GetResponse<ResponseList<List<ResponseCombo>>> respondataList, List<int> selectedId)
        {
            if (respondataList.RetCode == RetCodeEnum.Ok)
            {
                return respondataList.Data.ListData.Select(item => new SelectListItem
                {
                    Selected = selectedId.Contains(item.Id),
                    Text = item.Text,
                    Value = item.Id.ToString()
                });
            }
            else
            {
                return new List<SelectListItem>();
            }

        }
    }
}
