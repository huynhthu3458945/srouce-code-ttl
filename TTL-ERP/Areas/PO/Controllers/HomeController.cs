﻿using TTL_ERP.Areas.Administrator.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Helpers;

namespace TTL_ERP.Areas.PO.Controllers
{
    [Area("PO")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class HomeController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        public HomeController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public IActionResult Index()
        {
            TempData["CurrentMenu"] = "Home";
            return View();
        }
    }
}
