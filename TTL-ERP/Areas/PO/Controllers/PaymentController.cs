﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.PO.Models;
using TTL_ERP.Common;
using TTL_ERP.Extensions;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.PO.Controllers
{
    [Area("PO")]
    public class PaymentController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public PaymentController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int id)
        {
            try
            {
                List<OrderDetailView> model = new List<OrderDetailView>();
                var res = htmlHelper.GetAsync<ResponseList<List<OrderDetailView>>>($"{UrlAPI.Url_GetViewById}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data.ListData.ToList();
                }
                var resConfigPayment = htmlHelper.GetAsync<ResponseList<List<ConfigPaymentModel>>>($"{UrlAPI.Url_GetAllConfigPayment}").GetAwaiter().GetResult();
                if (resConfigPayment.RetCode == RetCodeEnum.Ok)
                {
                    ViewBag.ConfigPayment = resConfigPayment.Data.ListData.ToList();
                }
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpGet]
        public async Task<IActionResult> UpdateOrder(string vpc_Command, string vpc_Locale, string vpc_CurrencyCode, string vpc_MerchTxnRef,
            string vpc_Merchant, string vpc_OrderInfo, string vpc_Amount, string vpc_TxnResponseCode, string vpc_TransactionNo,
            string vpc_Message, string vpc_Card, string vpc_CardNum, string vpc_PayChannel, string vpc_CardUid, string vpc_CardHolderName, string vpc_ItaBank,
            string vpc_ItaFeeAmount, string vpc_ItaTime, string vpc_OrderAmount, string vpc_ItaMobile, string vpc_ItaEmail, string vpc_SecureHash, string vpc_Version)
        {
            try
            {
                var order = GetOrder(vpc_OrderInfo);
                if(order.OrderStatusCode != "600") // Đã thanh toán
                {
                    var resCheck = htmlHelper.GetAsync<string>($"{UrlAPI.Url_ResponeOnePay}" +
                        $"?vpc_Command={vpc_Command}&vpc_Locale={vpc_Locale}&vpc_CurrencyCode={vpc_CurrencyCode}&vpc_MerchTxnRef={vpc_MerchTxnRef}" +
                        $"&vpc_Merchant={vpc_Merchant}&vpc_OrderInfo={vpc_OrderInfo}&vpc_Amount={vpc_Amount}&vpc_TxnResponseCode={vpc_TxnResponseCode}&vpc_TransactionNo={vpc_TransactionNo}" +
                        $"&vpc_Message={vpc_Message}&vpc_Card={vpc_Card}&vpc_CardNum={vpc_CardNum}&vpc_PayChannel={vpc_PayChannel}&vpc_CardUid={vpc_CardUid}&vpc_CardHolderName={vpc_CardHolderName}&vpc_ItaBank={vpc_ItaBank}" +
                        $"&vpc_ItaFeeAmount={vpc_ItaFeeAmount}&vpc_ItaTime={vpc_ItaTime}&vpc_OrderAmount={vpc_OrderAmount}&vpc_ItaMobile={vpc_ItaMobile}&vpc_ItaEmail={vpc_ItaEmail}&vpc_SecureHash={vpc_SecureHash}" +
                        $"&vpc_Version={vpc_Version}").GetAwaiter().GetResult();
                    if (resCheck.RetCode == RetCodeEnum.Ok)
                    {

                        if (resCheck.Data == "SUCCESS") // thanh toán thành công!
                        {
                            return View("Success");
                        }
                        else // thanh toán không thành công!
                        {
                            return View("Error");
                        }
                    }
                    else
                    {
                        return View("Error");
                    }
                }
                return RedirectToAction("Index", new { id = order.Id });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpGet]
        public async Task<IActionResult> UpdateOrderNganLuong(string orderId, string order_code, string transaction_info, string receiver, string price,
           string payment_id, string payment_type, string error_text, string secure_code)
        {
            var order = GetOrder(order_code);
            if (order.OrderStatusCode != "600") // Đã thanh toán
            {
                var resCheck = htmlHelper.GetAsync<string>($"{UrlAPI.Url_ResponeNganLuong}?orderId={orderId}&order_code={order_code}&transaction_info={transaction_info}" +
                    $"&receiver={receiver}&price={price}&payment_id={payment_id}&payment_type={payment_type}&error_text={error_text}&secure_code={secure_code}").GetAwaiter().GetResult();
                if (resCheck.RetCode == RetCodeEnum.Ok)
                {

                    if (resCheck.Data == "SUCCESS") // thanh toán thành công!
                    {
                        return View("Success");
                    }
                    else // thanh toán không thành công!
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }
            }
            return RedirectToAction("Index", new { id = order.Id });
        }
            [HttpPost]
        public async Task<IActionResult> UpdateStatus(int id, int orderStatusCode, string cardList)
        {
            try
            {
                var resCheckQuantity = htmlHelper.PostAsyncStr($"{UrlAPI.Url_CheckEndQuantityProductNumber}?id={id}&orderStatusCode={orderStatusCode}", id).GetAwaiter().GetResult();
                if (resCheckQuantity.RetCode == RetCodeEnum.Ok)
                {
                    if (!string.IsNullOrEmpty(resCheckQuantity.Data))
                    {
                        return Json(new { retCode = 2, urlIndex = "", retText = resCheckQuantity.Data });
                    }
                    else
                    {
                        // tạo link thanh toán
                        var resLinkPayMent = htmlHelper.PostAsyncStr($"{UrlAPI.Url_CreatePayMent}?id={id}&cardList={cardList}", id).GetAwaiter().GetResult();
                        if (resLinkPayMent.RetCode == RetCodeEnum.Ok)
                        {
                            return Json(new { retCode = 0, url = resLinkPayMent.Data, urlIndex = resLinkPayMent.Data, retText = "Lấy link thành công!" });
                        }
                        return Json(new { retCode = 0, url = $"", urlIndex = "", retText = "Thành công!" });
                    }
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        private OrderModel GetOrder(string code)
        {
            OrderModel orderModel = new OrderModel();
            var res = htmlHelper.GetAsync<OrderModel>($"{UrlAPI.Url_GetByCodeOrder}?code={code}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                orderModel = res.Data;
            }
            return orderModel;
        }
    }
}
