﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.PO.Models
{
    public class ChannelModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public bool Status { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int OrganizationId { get; set; }
        public IEnumerable<SelectListItem> AccountList { get; set; }
    }
}
