﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Areas.PO.Models
{
    public class ConfigPaymentModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public bool Status { get; set; }
    }
}
