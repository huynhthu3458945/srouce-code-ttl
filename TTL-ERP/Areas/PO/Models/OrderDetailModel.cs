﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Areas.PO.Models
{
    public class OrderDetailModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public IEnumerable<SelectListItem> ProductList { get; set; }
        public int UnitId { get; set; }
        public IEnumerable<SelectListItem> UnitList { get; set; }
        public int ComboId { get; set; }
        public IEnumerable<SelectListItem> ComboList { get; set; }
        public int DT_DiscountKindId { get; set; }
        public IEnumerable<SelectListItem> DiscountKindList { get; set; }
        public int Quantity { get; set; }
        public string QuantityStr { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get; set; }
        public decimal TotalPrice { get; set; }
        public string TotalPriceStr { get; set; }
        public string Note { get; set; }
        public decimal OriginalPrice { get; set; }
        public string OriginalPriceStr { get; set; }
        public decimal PriceDiscount { get; set; }
        public string PriceDiscountStr { get; set; }
    }
    public class OrderDetailView : OrderDetailModel
    {
        public string CreateDateStr { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CreateName { get; set; }
        public decimal TotalAmount { get; set; }
        public string ProductName { get; set; }
        public string URLRequestOnePay { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}
