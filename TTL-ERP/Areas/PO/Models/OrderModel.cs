﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.PO.Models
{
    public class OrderModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CustomerId { get; set; }
        public string CustomerCodeParent { get; set; }
        public string CustomerPhoneParent { get; set; }
        public string SaleCode { get; set; }
        public string SalePhone { get; set; }
        public string AcceptedCode { get; set; }
        public string AcceptedPhone { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public IEnumerable<SelectListItem> CustomerList { get; set; }

        public int OrderStatusId { get; set; }
        public string OrderStatusName { get; set; }
        public IEnumerable<SelectListItem> OrderStatusList { get; set; }

        public OrderTypeEnum OrderType { get; set; }
        public int PriceId { get; set; }
        public IEnumerable<SelectListItem> PriceList { get; set; }
        public int OrganizationId { get; set; }
        public int DiscountValue { get; set; }
        public string Note { get; set; }
        public bool IsDelivery { get; set; }
        public decimal DeliveryFee { get; set; }
        public string DeliveryFeeStr { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalAmountStr { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public string TotalDiscountAmountStr { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public string Address { get; set; }
        public int PayTypeId { get; set; }
        public IEnumerable<SelectListItem> PayTypeList { get; set; }
        public string NoteDeliveryFee { get; set; }
        public bool IsDiscountPercent { get; set; }
        public string NoteDiscount { get; set; }
        public int ParentId { get; set; }
        public bool IsVat { get; set; }
        public decimal InvoiceValue { get; set; }
        public string InvoiceValueStr { get; set; }
        public string InvoiceCode { get; set; }
        public DateTime? InvoiceDay { get; set; }
        public int ChannelId { get; set; }
        public decimal TotalCustomerToPay { get; set; }
        public string TotalCustomerToPayStr { get; set; }
        public decimal ExtraMoney { get; set; }
        public string ExtraMoneyStr { get; set; }
        public int Ref_Id { get; set; }
        public string Ref_Code { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyAddress { get; set; }
        public int TransportId { get; set; }
        public IEnumerable<SelectListItem> TransportList { get; set; }
        public int MT_DiscountKindId { get; set; }
        public IEnumerable<SelectListItem> DiscountKindList { get; set; }
        public string CreateOnStr { get; set; }
        public IEnumerable<OrderDetailModel> OrderDetail { get; set; }
        public IEnumerable<OrderDetailModel> Details { get; set; }
        public string OrderStatusCode { get; set; }
        public string LinkPayment { get; set; }
        public string URLRequestOnePay { get; set; }
        public string OrderCodeSS { get; set; }
    }
    public class OrderFitter
    {
        public int CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string Phone { get; set; }
        public int OrderStatusId { get; set; }
        public int OrganizationId { get; set; }
        public string CustomerPhoneParent { get; set; }
        public int CreateBy { get; set; }
        public string FromDateStr { get; set; }
        public DateTime? FromDate { get; set; }
        public string ToDateStr { get; set; }
        public DateTime? ToDate { get; set; }
        public int Page { get; set; }
    }

    public  enum OrderTypeEnum : int
    {
        [Description("Bán lẻ")]
        Retail = 1,
        [Description("Bán sỉ")]
        WholeSale = 2
    }
}
