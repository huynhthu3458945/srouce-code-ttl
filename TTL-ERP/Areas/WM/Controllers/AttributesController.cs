﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class AttributesController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public AttributesController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<AttributesModel>();
                TempData["CurrentMenu"] = "Attributes";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(int page, string search = "")
        {
            List<AttributesModel> listModel = new List<AttributesModel>();
            var model = new IndexViewModel<AttributesModel>();
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<AttributesModel>>>($"{UrlAPI.Url_GetAllAttributes}?organizationId={user.OrganizationId}&limit={pageItems}&page={page}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.Where(z =>
                (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Code.Contains(search)))
                || (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Title.Contains(search))))
                    .ToList()
                ;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/Attributes/Index?page={0}'");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }

        public IActionResult Create()
        {
            AttributesModel model = new AttributesModel();
            TempData["CurrentMenu"] = "Attributes";
            ViewBag.IsCreate = true;
            return PartialView("_AddOrEdit", model);
        }
      
        public IActionResult Edit(int id)
        {
            try
            {
                var model = new AttributesModel();
                var res = htmlHelper.GetAsync<AttributesModel>($"{UrlAPI.Url_GetByIdAttributes}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                }
                ViewBag.IsCreate = false;
                TempData["CurrentMenu"] = "Attributes";
                return PartialView("_AddOrEdit", model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult AddOrEdit(AttributesModel model)
        {
            try
            {
               if(model.Id == 0) // add
                {
                    var user = GetUser();
                    model.CreateBy = GetUser().Id;
                    model.CreateOn = DateTime.Now.Date;
                    model.ModifiedOn = DateTime.Now.Date;
                    model.OrganizationId = user.OrganizationId;
                    // check tồn tại mã
                    var resCheck = htmlHelper.GetAsync<AttributesModel>($"{UrlAPI.Url_GetByCodeAttributes}?code={model.Code}").GetAwaiter().GetResult();
                    if (resCheck.Data != null)
                        return Json(new { retCode = 1, retText = "Mã thuộc tính đã tồn tại" });
                    var res = htmlHelper.PostAsync<AttributesModel>(UrlAPI.Url_AddAttributes, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        model = res.Data;
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Thêm mới thành công!" });
                        //return Redirect($"?id={model.Id }");
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                        //return Redirect($"?id={model.Id }");
                    }

                }
                else // edit
                {
                    model.ModifiedBy = GetUser().Id;
                    model.ModifiedOn = DateTime.Now.Date;
                    var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateAttributes, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Cập nhật thành công!" });
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });

                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteAttributes}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
