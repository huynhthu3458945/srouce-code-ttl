﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class ComboController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public ComboController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<ComboModel>();

                TempData["CurrentMenu"] = "Combo";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(ComboFitter fitter)
        {
            List<ComboModel> listModel = new List<ComboModel>();
            var model = new IndexViewModel<ComboModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ComboModel>>>($"{UrlAPI.Url_GetAllCombo}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/Combo/Index?page={0}'");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult CreatePopUp()
        {
            var model = new IndexViewModel<ProductModel>(); 
            var productCategoryList = GetComBoxProductCategory(0).ToList();
            productCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductCategoryList = productCategoryList;

            var productTypeList = GetComBoxProductType(0).ToList();
            productTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductTypeList = productTypeList;
            return PartialView("_CreatePopUp", model);
        }

        public PartialViewResult LoadDataProduct(ProductFitter fitter)
        {
            var model = new IndexViewModel<ProductModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId; 
            fitter.ProductCategoryId = fitter.PopupProductCategoryId;
            fitter.ProductTypeId = fitter.PopupProductTypeId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ProductModel>>>($"{UrlAPI.Url_GetAllProduct}" +
                 $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/Combo/Index?page={0}'");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopup", model);
        }


        public IActionResult Create()
        {
            TempData["CurrentMenu"] = "Combo";
            ComboModel model = new ComboModel();
            model.ComboProductList = new List<Combo_ProductModel>();
            ViewBag.IsCreate = true;
            return View(model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new ComboModel();
                var res = htmlHelper.GetAsync<ComboModel>($"{UrlAPI.Url_GetByIdCombo}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.SaleDeadLineStr = string.Format("{0:dd/MM/yyyy}", model.SaleDeadLine);
                    model.ComboProductList.ToList().ForEach(z =>
                    {
                        z.ProductList = GetComBoxProduct(z.ProductId, 0); 
                        z.UnitIdList = GetComBoxUnit(z.UnitId);
                    }
                        );
                }
                ViewBag.IsCreate = false;
                TempData["CurrentMenu"] = "Combo";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        [HttpPost]
        public async Task<IActionResult> Create(ComboModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.ComboProductList = model.Details;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<ComboModel>($"{UrlAPI.Url_GetByCodeCombo}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã combo đã tồn tại" });

                if (!string.IsNullOrEmpty(model.SaleDeadLineStr))
                    model.SaleDeadLine = DateTime.Parse(StringUtil.ConvertStringToDate(model.SaleDeadLineStr));
                else
                    model.SaleDeadLine = null;

                var res = htmlHelper.PostAsync(UrlAPI.Url_AddCombo, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/WM/Combo/Edit?id={model.Id }", urlIndex = "/WM/Combo", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Edit(ComboModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.ComboProductList = model.Details;
                if (!string.IsNullOrEmpty(model.SaleDeadLineStr))
                    model.SaleDeadLine = DateTime.Parse(StringUtil.ConvertStringToDate(model.SaleDeadLineStr));
                else
                    model.SaleDeadLine = null;
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateCombo, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/WM/Combo/Edit?id={model.Id}", urlIndex = "/WM/Combo", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteCombo}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
