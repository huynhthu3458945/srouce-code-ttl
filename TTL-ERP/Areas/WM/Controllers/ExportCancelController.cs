﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class ExportCancelController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public ExportCancelController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public IActionResult Create()
        {
            TempData["CurrentMenu"] = "Warehouse";
            BillModel model = new BillModel();
            model.BillType = BillTypeEnum.ExportWareHouse;
            model.BillCategoryId = GetBillCategory("XK").Id;
            model.BillStatusId = GetBillStatus("100").Id;
            model.BillStatusList = GetComBoxBillStatus(0);
            model.BillDetail = new List<BillDetailModel>();
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date);
            model.StoreList = GetComBoxStore(0);
            model.StoreExportList = GetComBoxStore(0);
            model.DiscountKindList = GetComBoxDiscountKind(0);
            var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeBill}?type=PX").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.Code = res.Data;
            }
            ViewBag.IsCreate = true;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(BillModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.BillDetail = model.Details;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByCodeBill}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Phiếu xuất đã tồn tại" });

                if (!string.IsNullOrEmpty(model.CreateOnStr))
                    model.CreateOn = DateTime.Now;
                else
                    model.CreateOn = null;

                var res = htmlHelper.PostAsync(UrlAPI.Url_AddBill, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    //TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/WM/Warehouse", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public IActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            BillModel model = new BillModel();
            var res = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.BillType = BillTypeEnum.ExportWareHouse;
                model.BillStatusList = GetComBoxBillStatus(model.BillStatusId);
                model.StoreList = GetComBoxStore(model.StoreExportId);
                model.StoreExportList = GetComBoxStore(model.StoreExportId);
                model.DiscountKindList = GetComBoxDiscountKind(model.MT_DiscountKindId);
                model.BillDetail.ToList().ForEach(z =>
                {
                    z.UnitList = GetComBoxUnit(z.UnitId);
                    z.ProductList = GetComBoxProduct(z.ProductId, 0);
                    z.DiscountKindList = GetComBoxDiscountKind(z.DT_DiscountKindId);
                });
            }
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", model.CreateOn);
            ViewBag.IsCreate = false;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(BillModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.BillDetail = model.Details;
                
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateBill, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/WM/Warehouse", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public IActionResult Detail(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            BillModel model = new BillModel();
            var res = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.BillStatusList = GetComBoxBillStatus(model.BillStatusId);
                model.StoreList = GetComBoxStore(model.StoreExportId);
                model.DiscountKindList = GetComBoxDiscountKind(model.MT_DiscountKindId);
                model.BillDetail.ToList().ForEach(z =>
                {
                    z.UnitList = GetComBoxUnit(z.UnitId);
                    z.ProductList = GetComBoxProduct(z.ProductId, 0);
                    z.DiscountKindList = GetComBoxDiscountKind(z.DT_DiscountKindId);
                });
            }
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", model.CreateOn);
            ViewBag.IsCreate = false;
            return View(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteBill}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

       

        private BillCategoryModel GetBillCategory(string code)
        {
            BillCategoryModel billCategoryModel = new BillCategoryModel();
            var res = htmlHelper.GetAsync<BillCategoryModel>($"{UrlAPI.Url_GetByCodeBillCategory}?code={code}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                billCategoryModel = res.Data;
            }

            return billCategoryModel;
        }
        private BillStatusModel GetBillStatus(string code)
        {
            BillStatusModel billStatusModel = new BillStatusModel();
            var res = htmlHelper.GetAsync<BillStatusModel>($"{UrlAPI.Url_GetBillStatusByCode}?code={code}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                billStatusModel = res.Data;
            }

            return billStatusModel;
        }
    }
}
