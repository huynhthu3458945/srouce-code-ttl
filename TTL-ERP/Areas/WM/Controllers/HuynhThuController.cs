﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class HuynhThuController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 1;
        public HuynhThuController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public IActionResult Index()
        {
            List<Master> listModel = new List<Master>();
            var model = new IndexViewModel<Master>();
            model.StoreList = GetComBoxStore(0);
            TempData["CurrentMenu"] = "HuynhThu";
            return View(model);
        }
        public PartialViewResult LoadData(int page, string search, int storeId)
        {
            List<Master> listModel = new List<Master>();
            var model = new IndexViewModel<Master>();
            var user = GetUser();
            listModel = GetMasters();
            model.StoreList = GetComBoxStore(storeId);
            model.ListItems = listModel.Where(z =>
                (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Code.Contains(search)))
                || (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Title.Contains(search))))
                    .ToList();
            model.PagingViewModel = new PagingModel(listModel.Count, pageItems, page, "href='/HuynhThu/Index?page={0}'");
            return PartialView("_TableResult", model);
        }

        public List<Master> GetMasters()
        {
            var list = new List<Master>();
            list.Add(
                new Master()
                {
                    Id = 1,
                    Code = "Code1",
                    Title = "Test1",
                    ParentId = 1,
                    Details = new List<Detail>()
                    {
                        new Detail()
                        {
                            InventoryId = "InventoryId1.1",
                            InventoryName = "InventoryName1.1",
                            UnitId = "UnitId1.1"
                        },
                        new Detail()
                        {
                            InventoryId = "InventoryId1.2",
                            InventoryName = "InventoryName1.2",
                            UnitId = "UnitId1.2"
                        }
                    }
                }
                );
            list.Add(
                new Master()
                {
                    Id = 2,
                    Code = "Code2",
                    Title = "Test2",
                    ParentId = 2,
                    Details = new List<Detail>()
                    {
                        new Detail()
                        {
                            InventoryId = "InventoryId2.1",
                            InventoryName = "InventoryName2.1",
                            UnitId = "UnitId2.1"
                        },
                        new Detail()
                        {
                            InventoryId = "InventoryId2.2",
                            InventoryName = "InventoryName2.2",
                            UnitId = "UnitId2.2"
                        }
                    }
                }
                );
            return list;
        }

        public List<SelectListItem> GetCombox(string inventoryId)
        {
            var list = new List<SelectListItem>() {
                new SelectListItem()
                {
                    Value = "InventoryId1.1",
                    Text = "InventoryName1.1",
                    Selected = inventoryId == "InventoryId1.1"
                },
                new SelectListItem()
                {
                    Value = "InventoryId2.1",
                    Text = "InventoryName2.1",
                    Selected = inventoryId == "InventoryId2.1"
                }
            };
            return list;
        }
        public List<Inventory> GetInventory()
        {
            var list = new List<Inventory>() {
                new Inventory()
                {
                    InventoryId = "InventoryId1.1",
                    InventoryName = "InventoryName1.1",
                   
                },
                new Inventory()
                {
                    InventoryId = "InventoryId2.1",
                    InventoryName = "InventoryName2.1"
                }
            };
            return list;
        }

        public IActionResult GetComboxJson(string inventoryId)
        {
            var list = new List<SelectListItem>() {
                new SelectListItem()
                {
                    Value = "InventoryId1.1",
                    Text = "InventoryName1.1",
                    Selected = inventoryId == "InventoryId1.1"
                },
                new SelectListItem()
                {
                    Value = "InventoryId2.1",
                    Text = "InventoryName2.1",
                    Selected = inventoryId == "InventoryId2.1"
                }
            };
            return Json(list);
        }

        public IActionResult Create()
        {
            Master model = new Master();
            model.ParentList = GetCombox("");
            model.InventoryList = GetCombox("");
            model.Details = new List<Detail>() ;
            model.Details.Add(new Detail()
            {
                InventoryId = "InventoryId1.1",
                InventoryName = "InventoryName1.1"
            });
            ViewBag.IsCreate = true;
            return View(model);
        }
        public IActionResult Create1()
        {
            Master model = new Master();
            model.ParentList = GetCombox("");
            model.InventoryList = GetCombox("");
            model.Details = new List<Detail>();
            model.Details.Add(new Detail()
            {
                InventoryId = "InventoryId1.1",
                InventoryName = "InventoryName1.1"
            });
            ViewBag.IsCreate = true;
            return View(model);
        }
        public IActionResult CreatePopUp(string search, int page)
        {
            var model = new IndexViewModel<BillCategoryModel>();
            return PartialView("_CreatePopUp", model);
        }
        public PartialViewResult LoadData2(int page, string search = "")
        {
            List<Inventory> listModel = new List<Inventory>();
            var model = new IndexViewModel<Inventory>();
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<BillCategoryModel>>>($"{UrlAPI.Url_GetAllBillCategory}?organizationId={user.OrganizationId}&limit={pageItems}&page={page}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/HuynhThu/Index?page={0}'");
                model.ListItems = GetInventory();
            }
            return PartialView("_TableResultPopup", model);
        }
        public IActionResult Edit(int id)
        {
            try
            {
                var model = new Master();
                ViewBag.IsCreate = false;
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Create(Master model)
        {
            try
            {
                if (false)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Redirect($"/WM/HuynhThu/Edit/{model.Id }");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", "Lỗi");
                    return Redirect($"/WM/HuynhThu/Edit/{model.Id }");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult Edit(Master model)
        {
            try
            {

                if (true)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                }
                else
                {
                    TempData["Message"] = GetScriptShowToast("error", "Lỗi");
                }
                return Redirect($"/WM/HuynhThu/Edit/{model.Id }");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

    }
}
