﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class ImportNumberController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private readonly IHostingEnvironment hostingEnvironment;
        private int pageItems = 20;
        public ImportNumberController(HttpClientHelper htmlHelper, UploadHelper uploadHelper, IHostingEnvironment environment) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
            hostingEnvironment = environment;
        }
        public IActionResult Create()
        {
            TempData["CurrentMenu"] = "Warehouse";
            BillModel model = new BillModel();
            model.BillType = BillTypeEnum.ImportWareHouse;
            model.BillCategoryId = GetBillCategory("NHSPS").Id;
            model.BillStatusId = GetBillStatus("100").Id;
            model.BillStatusList = GetComBoxBillStatus(0);
            model.StoreDetail = new List<StoreDetailModel>();
            //// ------------
            //model.StoreDetail = new List<StoreDetailModel>()
            //{
            //    new StoreDetailModel() { CodeValue = "123456789", SeriNumber ="S00001", Title = "Ghi chú 1"},
            //    new StoreDetailModel() { CodeValue = "987654321", SeriNumber ="S00002", Title = "Ghi chú 2" }
            //};
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date);
            var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
            int productTypeId = productType.Data.Id; // Sản phẩm số
            model.ProductList = GetComBoxProductIsInventory(0, productTypeId);
            model.StoreList = GetComBoxStore(0);
            var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeBill}?type=PN").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.Code = res.Data;
            }
            ViewBag.IsCreate = true;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(BillModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.StoreDetail = model.Details1;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByCodeBill}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Phiếu nhập đã tồn tại" });

                if (!string.IsNullOrEmpty(model.CreateOnStr))
                    model.CreateOn = DateTime.Now;
                else
                    model.CreateOn = null;

                var res = htmlHelper.PostAsync(UrlAPI.Url_AddBill, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    //TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/WM/Warehouse", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public IActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            BillModel model = new BillModel();
            var res = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.BillType = BillTypeEnum.ImportWareHouse;
                model.BillStatusList = GetComBoxBillStatus(model.BillStatusId);
                model.StoreList = GetComBoxStore(model.StoreId);
                var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
                int productTypeId = productType.Data.Id; // Sản phẩm số
                model.ProductId = res.Data.StoreDetail.FirstOrDefault().ProductId;
                model.ProductList = GetComBoxProductIsInventory(model.ProductId, productTypeId);

            }
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", model.CreateOn);
            ViewBag.IsCreate = false;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(BillModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.StoreDetail = model.Details1;

                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateBill, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/WM/Warehouse", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public IActionResult Detail(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            BillModel model = new BillModel();
            var res = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.BillType = BillTypeEnum.ImportWareHouse;
                model.BillStatusList = GetComBoxBillStatus(model.BillStatusId);
                model.StoreList = GetComBoxStore(model.StoreId);
                var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=SPS").GetAwaiter().GetResult();
                int productTypeId = productType.Data.Id; // Sản phẩm số
                model.ProductId = res.Data.StoreDetail.FirstOrDefault().ProductId;
                model.ProductList = GetComBoxProductIsInventory(model.ProductId, productTypeId);
            }
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", model.CreateOn);
            ViewBag.IsCreate = false;
            return View(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteBill}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
        public IActionResult DownloadTemplateExcel()
        {

            string fileName = "ImportNumber.xlsx";
            string path = Path.Combine(Directory.GetCurrentDirectory(), "Areas/WM/TemplateExecl/ImportNumber/ImportNumber.xlsx");
            //Read the File data into Byte Array.
            byte[] bytes = System.IO.File.ReadAllBytes(path);

            //Send the File to Download.
            return File(bytes, "application/octet-stream", fileName);
            //return File(bytes, "", fileName);
        }

        public async Task<IActionResult> ImportExcelFile(IFormFile file)
        {
            try
            {
                //get file name
                var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

                //get path
                // var MainPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");
                var mainPath = Path.Combine(hostingEnvironment.ContentRootPath, "Areas/WM/File/ImportNumber");

                //create directory "Uploads" if it doesn't exists
                if (!Directory.Exists(mainPath))
                {
                    Directory.CreateDirectory(mainPath);
                }

                //get file path 
                var filePath = Path.Combine(mainPath, file.FileName);
                using (Stream stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                //get extension
                string extension = Path.GetExtension(filename);

                string conString = string.Empty;

                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                    case ".xlsx": //Excel 07 and above.
                        conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                }

                var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", conString);
                var ds = new DataSet();
                adapter.Fill(ds, "ExcelTable");
                DataTable dtable = ds.Tables["ExcelTable"];
                int index = 0;
                string strHtml = string.Empty;
                foreach (DataRow item in dtable.Rows)
                {

                    strHtml += $"<tr>"
                            + $" <td class=\"text-center\">"
                            + $"        <p>{index + 1}</p>"
                            + $"    </td>"
                            + $"    <td>"
                            + $"        <div class=\"col-md-12\">"
                            + $"            <input class=\"form-control SeriNumber\" accesskey=\"KeyFocus\" name=\"CodeValue\" placeholder=\"\" type=\"text\" value=\"{item["CodeValue"]?.ToString() ?? ""}\" />"
                            + $"        </div>"
                            + $"    </td>"
                            + $"    <td>"
                            + $"        <div class=\"col-md-12\">"
                            + $"            <input class=\"form-control SeriNumber\" accesskey=\"KeyFocus\" name=\"SeriNumber\" placeholder=\"\" type=\"text\"  value=\"{item["SeriNumber"]?.ToString() ?? ""}\"/>"
                            + $"        </div>"
                            + $"    </td>"
                            + $"    <td>"
                            + $"        <div class=\"col-md-12\">"
                            + $"            <input class=\"form-control\" accesskey=\"KeyFocus\" name=\"Title\" placeholder=\"\" type=\"text\"  value=\"{item["Title"]?.ToString() ?? ""}\"/>"
                            + $"        </div>"
                            + $"    </td>"
                            + $"    <td class=\"text-center\">"
                            + $"        <a href=\"javascript:void(0);\" name=\"btnRemoveLine\"  data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"Xóa dòng\"><i class=\"ti ti-trash me-1\"></i></a>"
                            + $"    </td>"
                            + $"</tr>";
                    index++;
                }
                return Json(new { success = true, html = strHtml });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, html = ex.ToString() }); ;
            }
        }
        private BillCategoryModel GetBillCategory(string code)
        {
            BillCategoryModel billCategoryModel = new BillCategoryModel();
            var res = htmlHelper.GetAsync<BillCategoryModel>($"{UrlAPI.Url_GetByCodeBillCategory}?code={code}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                billCategoryModel = res.Data;
            }

            return billCategoryModel;
        }
        private BillStatusModel GetBillStatus(string code)
        {
            BillStatusModel billStatusModel = new BillStatusModel();
            var res = htmlHelper.GetAsync<BillStatusModel>($"{UrlAPI.Url_GetBillStatusByCode}?code={code}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                billStatusModel = res.Data;
            }

            return billStatusModel;
        }
    }
}
