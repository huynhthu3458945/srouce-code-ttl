﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class InventoryController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public InventoryController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int storeId, int palletId)
        {
            try
            {
                var model = new IndexViewModel<InventoryModel>();
                var storeList = GetComBoxStore(storeId).ToList();
                storeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.StoreList = storeList;

                TempData["CurrentMenu"] = "Inventory";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(InventoryFitter fitter)
        {
            List<InventoryModel> listModel = new List<InventoryModel>();
            var model = new IndexViewModel<InventoryModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<InventoryModel>>>($"{UrlAPI.Url_GetAllInventory}" +
               $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult Create()
        {
            TempData["CurrentMenu"] = "Inventory";
            InventoryModel model = new InventoryModel();
            model.InventoryDateStr = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date);
            model.StoreList = GetComBoxStore(0);
            model.InventoryDetail = new List<InventoryDetailModel>();

            var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeInventory}?type=KK").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.Code = res.Data;
            }
            ViewBag.IsCreate = true;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(InventoryModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.InventoryDetail = model.Details;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<InventoryModel>($"{UrlAPI.Url_GetByCodeInventory}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Phiếu kiểm kê đã tồn tại" });

                // check tồn kho
                ProductFitter fitter = new ProductFitter();
                fitter.StoreId = model.StoreId;
                fitter.OrganizationId = model.OrganizationId;

                //string error = CheckQuantityEnd(fitter, model.BillDetail);

                //if (!string.IsNullOrEmpty(error))
                //    return Json(new { retCode = RetCodeEnum.ApiError, retText = error });

                if (!string.IsNullOrEmpty(model.InventoryDateStr))
                    model.InventoryDate = DateTime.Parse(StringUtil.ConvertStringToDate(model.InventoryDateStr));
                else
                    model.InventoryDate = null;

                var res = htmlHelper.PostAsync(UrlAPI.Url_AddInventory, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    //TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/WM/Inventory", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public IActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "Inventory";
            InventoryModel model = new InventoryModel();
            var res = htmlHelper.GetAsync<InventoryModel>($"{UrlAPI.Url_GetByIdInventory}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.StoreList = GetComBoxStore(model.StoreId);
                model.InventoryDetail.ToList().ForEach(z =>
                {
                    z.ProductList = GetComBoxProduct(z.ProductId, 0);
                });
            }
            model.InventoryDateStr = string.Format("{0:dd/MM/yyyy}", model.InventoryDate);
            ViewBag.IsCreate = false;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(InventoryModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now;
                model.OrganizationId = user.OrganizationId;
                model.InventoryDetail = model.Details;

                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateInventory, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"", urlIndex = "/WM/Inventory", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public IActionResult Detail(int id)
        {
            TempData["CurrentMenu"] = "Inventory";
            InventoryModel model = new InventoryModel();
            var res = htmlHelper.GetAsync<InventoryModel>($"{UrlAPI.Url_GetByIdInventory}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.StoreList = GetComBoxStore(model.StoreId);
                model.InventoryDetail.ToList().ForEach(z =>
                {
                    z.ProductList = GetComBoxProduct(z.ProductId, 0);
                });
            }
            model.InventoryDateStr = string.Format("{0:dd/MM/yyyy}", model.InventoryDate);
            ViewBag.IsCreate = false;
            return View(model);
        }
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteInventory}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }


        [HttpGet]
        public async Task<IActionResult> GetCodeMax(string type)
        {
            try
            {
                var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeInventory}?type={type}").GetAwaiter().GetResult();
                return Json(new { retCode = res.RetCode, retText = res.RetText, code = res.Data });
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

        public async Task<IActionResult> GetProductByStoreId(int storeId)
        {
            var user = GetUser();
            var model = new List<InventoryDetailModel>();
            ProductFitter fitter = new ProductFitter();
            fitter.StoreId = storeId;
            fitter.OrganizationId = user.OrganizationId;

            var resProduct = htmlHelper.PostAsyncFitter<ResponseList<List<Product_StoreProcedure>>>($"{UrlAPI.Url_GetAllProductQuantityEnd}" +
               $"?limit={1000000}&page={1}", fitter).GetAwaiter().GetResult();

            string strHtml = string.Empty;
            if (resProduct.RetCode == RetCodeEnum.Ok)
            {
                int index = 0;
                var listProduct = resProduct.Data.ListData.Where(z => z.QuantityReal > 0);
                foreach (var item in listProduct)
                {
                    strHtml += $"<tr>"
                           + $" <td class=\"text-center\">"
                           + $"        <p>{index + 1}</p>"
                           + $"    </td>"
                           + $"    <td>"
                           + $"        <div class=\"col-md-12\">"
                           + $"            <p>{item.Title}</p>"
                           + $"            <input class=\"form-control\" hidden accesskey=\"KeyFocus\" name=\"ProductId\" placeholder=\"\" type=\"number\" value=\"{item.Id}\" />"
                           + $"            <input class=\"form-control\" hidden accesskey=\"KeyFocus\" name=\"CostPrice\" placeholder=\"\" type=\"number\" value=\"{item.CostPrice}\" />"
                           + $"            <input class=\"form-control\" hidden accesskey=\"KeyFocus\" name=\"Price\" placeholder=\"\" type=\"number\" value=\"{item.Price}\" />"
                           + $"        </div>"
                           + $"    </td>"
                           + $"    <td>"
                           + $"        <div class=\"col-md-12\">"
                           + $"            <input class=\"form-control\" accesskey=\"KeyFocus\" hidden name=\"Quantity\" placeholder=\"\" type=\"number\" value=\"{item.QuantityReal}\" />"
                           + $"            <input class=\"form-control Quantity text-end\" accesskey=\"KeyFocus\" name=\"QuantityStr\" placeholder=\"\" type=\"text\" value=\"{item.QuantityReal}\" disabled =\"disabled\" /> "
                           + $"        </div>"
                           + $"    </td>"
                           + $"    <td>"
                           + $"        <div class=\"col-md-12\">"
                           + $"            <input class=\"form-control\" accesskey=\"KeyFocus\" hidden name=\"QuantityActual\" placeholder=\"\" type=\"number\" value=\"0\" />"
                           + $"            <input class=\"form-control QuantityActual text-end\" accesskey=\"KeyFocus\" name=\"QuantityActualStr\" placeholder=\"\" type=\"text\" value=\"0\" /> "
                           + $"        </div>"
                           + $"    </td>"
                           + $"    <td>"
                           + $"        <div class=\"col-md-12\">"
                           + $"            <input class=\"form-control\" accesskey=\"KeyFocus\" hidden name=\"QuantityDiff\" placeholder=\"\" type=\"number\" value=\"0\" />"
                           + $"            <input class=\"form-control QuantityDiff text-end\" accesskey=\"KeyFocus\" name=\"QuantityDiffStr\" placeholder=\"\" type=\"text\" value=\"0\" disabled =\"disabled\" /> "
                           + $"        </div>"
                           + $"    </td>"
                           + $"    <td>"
                           + $"        <div class=\"col-md-12\">"
                           + $"            <input class=\"form-control\" accesskey=\"KeyFocus\" hidden name=\"DifferenceValue\" placeholder=\"\" type=\"number\" value=\"0\" />"
                           + $"            <input class=\"form-control DifferenceValue text-end\" accesskey=\"KeyFocus\" name=\"DifferenceValueStr\" placeholder=\"\" type=\"text\" value=\"0\" disabled =\"disabled\"  /> "
                           + $"        </div>"
                           + $"    </td>"
                           + $"    <td class=\"text-center\">"
                           + $"        <a href=\"javascript:void(0);\" name=\"btnRemoveLine\"  data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"Xóa dòng\"><i class=\"ti ti-trash me-1\"></i></a>"
                           + $"    </td>"
                           + $"</tr>";
                    index++;
                }
            }

            return Json(new { success = true, html = strHtml });
        }
        [HttpPost]
        public async Task<IActionResult> Balance(int id)
        {
            try
            {
                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_Balance}?inventoryId={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {

                    TempData["Message"] = GetScriptShowToast("success", "Đã cân bằng kho thành công!");
                    return Json(new { retCode = res.RetCode, retText = "Đã cân bằng kho thành công!" });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }

    }
}
