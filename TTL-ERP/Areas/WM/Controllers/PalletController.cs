﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class PalletController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public PalletController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                List<PalletModel> listModel = new List<PalletModel>();
                var model = new IndexViewModel<PalletModel>();
                model.StoreList = GetComBoxStoreByIsPallet(0);
                TempData["CurrentMenu"] = "Pallet";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(int page, string search, int storeIdSearch)
        {
            List<PalletModel> listModel = new List<PalletModel>();
            var model = new IndexViewModel<PalletModel>();
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<PalletModel>>>($"{UrlAPI.Url_GetAllPallet}" +
                $"?organizationId={user.OrganizationId}&storeId={storeIdSearch}&search={search}&limit={pageItems}&page={page}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.ToList();
                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/Pallet/Index?page={0}'");
            }
            return PartialView("_TableResult", model);
        }

        public IActionResult Create()
        {
            PalletModel model = new PalletModel();
            TempData["CurrentMenu"] = "Pallet";
            model.StoreList = GetComBoxStoreByIsPallet(0);
            ViewBag.IsCreate = true;
            return PartialView("_AddOrEdit", model);
        }
      
        public IActionResult Edit(int id)
        {
            try
            {
                var model = new PalletModel();
                var res = htmlHelper.GetAsync<PalletModel>($"{UrlAPI.Url_GetByIdPallet}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.StoreList = GetComBoxStoreByIsPallet(model.StoreId);
                }
                ViewBag.IsCreate = false;
                TempData["CurrentMenu"] = "Pallet";
                return PartialView("_AddOrEdit", model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult AddOrEdit(PalletModel model)
        {
            try
            {
               if(model.Id == 0) // add
                {
                    var user = GetUser();
                    model.CreateBy = GetUser().Id;
                    model.CreateOn = DateTime.Now.Date;
                    model.ModifiedOn = DateTime.Now.Date;
                    model.OrganizationId = user.OrganizationId;
                    // check tồn tại mã
                    var resCheck = htmlHelper.GetAsync<PalletModel>($"{UrlAPI.Url_GetByCodePallet}?code={model.Code}").GetAwaiter().GetResult();
                    if (resCheck.Data != null)
                        return Json(new { retCode = 1, retText = "Mã Pallet tồn tại" });
                    var res = htmlHelper.PostAsync<PalletModel>(UrlAPI.Url_AddPallet, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        model = res.Data;
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Thêm mới thành công!" });
                        //return Redirect($"?id={model.Id }");
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                        //return Redirect($"?id={model.Id }");
                    }

                }
                else // edit
                {
                    model.ModifiedBy = GetUser().Id;
                    model.ModifiedOn = DateTime.Now.Date;
                    var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdatePallet, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Cập nhật thành công!" });
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeletePallet}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
