﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class PalletProductController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public PalletProductController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int storeId, int palletId)
        {
            try
            {
                var model = new IndexViewModel<Store_Pallet_ProductModel>();
                var storeList = GetComBoxStoreByIsPallet(storeId).ToList();
                storeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.StoreList = storeList;

                var palletList = GetComBoxPallet(palletId, storeId).ToList();
                palletList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.PalletList = palletList;

                TempData["CurrentMenu"] = "PalletProduct";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(PalletProductFitter fitter)
        {
            List<Store_Pallet_ProductModel> listModel = new List<Store_Pallet_ProductModel>();
            var model = new IndexViewModel<Store_Pallet_ProductModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<Store_Pallet_ProductModel>>>($"{UrlAPI.Url_GetAllPalletProduct}" +
               $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/PalletProduct/Index?page={0}'");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult CreatePopUp(int storeId, int palletId)
        {
            var model = new IndexViewModel<ProductModel>();
            var productCategoryList = GetComBoxProductCategory(0).ToList();
            productCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductCategoryList = productCategoryList;
            model.StoreIdScreenPallet = storeId;
            model.PalletIdScreenPallet = palletId;
            var productTypeList = GetComBoxProductType(0).ToList();
            productTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductTypeList = productTypeList;

            return PartialView("_CreatePopUp", model);
        }

        public PartialViewResult LoadDataProduct(ProductFitter fitter)
        {
            var model = new IndexViewModel<ProductModel>();
            var user = GetUser();
            fitter.ProductCategoryId = fitter.PopupProductCategoryId;
            fitter.ProductTypeId = fitter.PopupProductTypeId;
            fitter.OrganizationId = user.OrganizationId;
            
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ProductModel>>>($"{UrlAPI.Url_GetAllProduct}" +
                 $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/Combo/Index?page={0}'");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopup", model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Store_Pallet_ProductModel model)
        {
            var user = GetUser();
            model.CreateBy = user.Id;
            model.CreateOn = DateTime.Now.Date;
            var res = htmlHelper.PostAsync<Store_Pallet_ProductModel>($"{UrlAPI.Url_AddPalletProduct}", model).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                return Json(new { retCode = RetCodeEnum.Ok, retText = res.RetText });
            }
            return Json(new { retCode = RetCodeEnum.ApiError, retText = res.RetText });
        }
              
        [HttpPost]
        public async Task<IActionResult> CheckProductExistPallet(Store_Pallet_ProductModel model)
        {
            var user = GetUser();
            var res = htmlHelper.PostAsyncFitter<Store_Pallet_ProductModel>($"{UrlAPI.Url_GetPalletProduct}", model).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                if (res.Data == null)
                    return Json(new { retCode = RetCodeEnum.Ok, retText = res.RetText });
                else
                    return Json(new { retCode = RetCodeEnum.ApiError, retText = $"{res.Data.ProductCode} đã có trong danh sách" });
            }
            return Json(new { retCode = RetCodeEnum.ApiError, retText = res.RetText });
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeletePalletProduct}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
