﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class PriceListController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public PriceListController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<PriceListModel>();

                TempData["CurrentMenu"] = "PriceList";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(PriceListFitter fitter)
        {
            List<PriceListModel> listModel = new List<PriceListModel>();
            var model = new IndexViewModel<PriceListModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<PriceListModel>>>($"{UrlAPI.Url_GetAllPriceList}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/PriceList/Index?page={0}'");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult CreatePopUp()
        {
            var model = new IndexViewModel<ProductModel>(); 
            var productCategoryList = GetComBoxProductCategory(0).ToList();
            productCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductCategoryList = productCategoryList;

            var productTypeList = GetComBoxProductType(0).ToList();
            productTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductTypeList = productTypeList;
            return PartialView("_CreatePopUp", model);
        }

        public PartialViewResult LoadDataProduct(ProductFitter fitter)
        {
            var model = new IndexViewModel<ProductModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId; 
            fitter.ProductCategoryId = fitter.PopupProductCategoryId;
            fitter.ProductTypeId = fitter.PopupProductTypeId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ProductModel>>>($"{UrlAPI.Url_GetAllProduct}" +
                 $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/PriceList/Index?page={0}'");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopup", model);
        }

        public IActionResult CreatePopUpCombo()
        {
            var model = new IndexViewModel<ComboModel>();
            return PartialView("_CreatePopUpCombo", model);
        }

        public PartialViewResult LoadDataCombo(ComboFitter fitter)
        {
            var model = new IndexViewModel<ComboModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ComboModel>>>($"{UrlAPI.Url_GetAllCombo}" +
                 $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/PriceList/Index?page={0}'");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopupCombo", model);
        }

        public IActionResult Create()
        {
            TempData["CurrentMenu"] = "PriceList";
            PriceListModel model = new PriceListModel();
            model.PriceListDetail = new List<PriceListDetailModel>();
            ViewBag.IsCreate = true;
            return View(model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new PriceListModel();
                var res = htmlHelper.GetAsync<PriceListModel>($"{UrlAPI.Url_GetByIdPriceList}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.StartdateStr = string.Format("{0:dd/MM/yyyy}", model.Startdate);
                    model.EnddateStr = string.Format("{0:dd/MM/yyyy}", model.Enddate);
                    model.PriceListDetail.ToList().ForEach(z =>
                    {
                        z.ProductList = GetComBoxProduct(z.ProductId ?? 0, 0); 
                        z.ComboList = GetComBoxCombo(z.ComboId ?? 0);
                    }
                        );
                }
                ViewBag.IsCreate = false;
                TempData["CurrentMenu"] = "PriceList";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        [HttpPost]
        public async Task<IActionResult> Create(PriceListModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.PriceListDetail = model.Details;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<PriceListModel>($"{UrlAPI.Url_GetByCodePriceList}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã bảng giá đã tồn tại" });

                if (!string.IsNullOrEmpty(model.StartdateStr))
                    model.Startdate = DateTime.Parse(StringUtil.ConvertStringToDate(model.StartdateStr));

                if (!string.IsNullOrEmpty(model.EnddateStr))
                    model.Enddate = DateTime.Parse(StringUtil.ConvertStringToDate(model.EnddateStr));

                var res = htmlHelper.PostAsync(UrlAPI.Url_AddPriceList, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/WM/PriceList/Edit?id={model.Id }", urlIndex = "/WM/PriceList", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Edit(PriceListModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.PriceListDetail = model.Details;
                
                if (!string.IsNullOrEmpty(model.StartdateStr))
                    model.Startdate = DateTime.Parse(StringUtil.ConvertStringToDate(model.StartdateStr));

                if (!string.IsNullOrEmpty(model.EnddateStr))
                    model.Enddate = DateTime.Parse(StringUtil.ConvertStringToDate(model.EnddateStr));

                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdatePriceList, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/WM/PriceList/Edit?id={model.Id}", urlIndex = "/WM/PriceList", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeletePriceList}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
