﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class ProductController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public ProductController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<ProductModel>();
                var productCategoryList = GetComBoxProductCategory(0).ToList();
                productCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.ProductCategoryList = productCategoryList;

                var productTypeList = GetComBoxProductType(0).ToList();
                productTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.ProductTypeList = productTypeList;

                TempData["CurrentMenu"] = "Product";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(ProductFitter fitter)
        {
            List<ProductModel> listModel = new List<ProductModel>();
            var model = new IndexViewModel<ProductModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ProductModel>>>($"{UrlAPI.Url_GetAllProduct}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/Product/Index?page={0}'");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult CreatePopUp()
        {
            var model = new IndexViewModel<ProductModel>();
            var productCategoryList = GetComBoxProductCategory(0).ToList();
            productCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductCategoryList = productCategoryList;

            var productTypeList = GetComBoxProductType(0).ToList();
            productTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductTypeList = productTypeList;

            return PartialView("_CreatePopUp", model);
        }

        public PartialViewResult LoadDataProduct(ProductFitter fitter)
        {
            var model = new IndexViewModel<ProductModel>();
            var user = GetUser();
            fitter.ProductCategoryId = fitter.PopupProductCategoryId;
            fitter.ProductTypeId = fitter.PopupProductTypeId;
            fitter.OrganizationId = user.OrganizationId;
            //var productType = htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=QT").GetAwaiter().GetResult();
            //fitter.ProductTypeId = productType.Data.Id; // QT
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<ProductModel>>>($"{UrlAPI.Url_GetAllProduct}" +
                 $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "href='/Combo/Index?page={0}'");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopup", model);
        }


        public IActionResult Create()
        {
            TempData["CurrentMenu"] = "Product";
            ProductModel model = new ProductModel();
            model.ProductCategoryList = GetComBoxProductCategory(0);
            model.ProductTypeList = GetComBoxProductType(0);
            model.UnitList = GetComBoxUnit(0);
            model.SupplierList = GetComBoxSupplier(0);
            model.StoreList = GetComBoxStoreNumber(0);
            model.BundledGiftList = new List<BundledGiftModel>();
            model.AttributeList = new List<Product_AttributeModel>();
            ViewBag.IsCreate = true;
            return View(model);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var model = new ProductModel();
                var res = htmlHelper.GetAsync<ProductModel>($"{UrlAPI.Url_GetByIdProduct}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.ProductCategoryList = GetComBoxProductCategory(model.ProductCategoryId);
                    model.ProductTypeList = GetComBoxProductType(model.ProductTypeId);
                    model.UnitList = GetComBoxUnit(model.UnitId);
                    model.SupplierList = GetComBoxSupplier(model.SupplierId ?? 0);
                    model.StoreList = GetComBoxStoreNumber(model.StoreId);
                    model.SaleDeadLineStr = string.Format("{0:dd/MM/yyyy}", model.SaleDeadLine);

                    model.BundledGiftList.ToList().ForEach(z =>
                    {
                        //z.ProductBundledList = GetComBoxProduct(z.ProductBundled, htmlHelper.GetAsync<ProductTypeModel>($"{UrlAPI.Url_GetByCodeProductType}?code=QT").GetAwaiter().GetResult().Data.Id);
                        z.ProductBundledList = GetComBoxProduct(z.ProductBundled, 0);
                        z.UnitIdBundled = z.UnitId;
                        z.UnitIdBundledList = GetComBoxUnit(z.UnitId);
                    });
                        
                    model.AttributeList.ToList().ForEach(z =>
                    {
                        z.AttributeList  = GetComBoxAttributes(z.AttributeId);
                    });

                }
                ViewBag.IsCreate = false;
                TempData["CurrentMenu"] = "Product";
                return View(model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }

        [HttpPost]
        public async Task<IActionResult> Create(ProductModel model)
        {
            try
            {
                var user = GetUser();
                model.CreateBy = user.Id;
                model.CreateOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.BundledGiftList = model.Details;
                model.AttributeList = model.Details1;
                // check tồn tại mã
                var resCheck = htmlHelper.GetAsync<ProductModel>($"{UrlAPI.Url_GetByCodeProduct}?code={model.Code}").GetAwaiter().GetResult();
                if (resCheck.Data != null)
                    return Json(new { retCode = 1, retText = "Mã sản phẩm đã tồn tại" });

                if (!string.IsNullOrEmpty(model.SaleDeadLineStr))
                    model.SaleDeadLine = DateTime.Parse(StringUtil.ConvertStringToDate(model.SaleDeadLineStr));
                else
                    model.SaleDeadLine = null;
                if (model.BundledGiftList != null)
                {
                    model.BundledGiftList.ToList().ForEach(z => z.UnitId = z.UnitIdBundled);
                }

                var res = htmlHelper.PostAsync(UrlAPI.Url_AddProduct, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    TempData["Message"] = GetScriptShowToast("success", "Thêm mới thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/WM/Product/Edit?id={model.Id }", urlIndex = "/WM/Product", retText = "Thêm mới thành công!" });
                }
                else
                {
                    //TempData["Message"] = GetScriptShowToast("error", res.RetText);
                    //return Json(new { retCode = res.RetCode, url = $"/Organization/Edit/{model.Id }", retText = res.RetText });
                    return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Edit(ProductModel model)
        {
            try
            {
                var user = GetUser();
                model.ModifiedBy = user.Id;
                model.ModifiedOn = DateTime.Now.Date;
                model.OrganizationId = user.OrganizationId;
                model.BundledGiftList = model.Details;
                model.AttributeList = model.Details1;
                if (!string.IsNullOrEmpty(model.SaleDeadLineStr))
                    model.SaleDeadLine = DateTime.Parse(StringUtil.ConvertStringToDate(model.SaleDeadLineStr));
                else
                    model.SaleDeadLine = null;
                if (model.BundledGiftList != null)
                {
                    model.BundledGiftList.ToList().ForEach(z => z.UnitId = z.UnitIdBundled);
                }
                var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateProduct, model).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    TempData["Message"] = GetScriptShowToast("success", "Cập nhật thành công!");
                    return Json(new { retCode = res.RetCode, url = $"/WM/Product/Edit?id={model.Id}", urlIndex = "/WM/Product", retText = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteProduct}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
