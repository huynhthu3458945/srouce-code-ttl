﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class StoreController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private int pageItems = 20;
        public StoreController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                List<StoreModel> listModel = new List<StoreModel>();
                var model = new IndexViewModel<StoreModel>();
                TempData["CurrentMenu"] = "Store";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(int page, string search = "")
        {
            List<StoreModel> listModel = new List<StoreModel>();
            var model = new IndexViewModel<StoreModel>();
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<StoreModel>>>($"{UrlAPI.Url_GetAllStore}?organizationId={user.OrganizationId}&limit={pageItems}&page={page}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData.Where(z =>
                (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Code.Contains(search)))
                || (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Title.Contains(search))))
                    .ToList()
                ;

                model.ListItems = listModel;
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, page, "href='/Store/Index?page={0}'");
            }
            return PartialView("_TableResult", model);
        }

        public IActionResult Create()
        {
            StoreModel model = new StoreModel();
            TempData["CurrentMenu"] = "Store";
            model.StoreTypeList = GetComBoxStoreType(0);
            model.KeyList = GetComBoxAccount(0);
            var newListSelect = new List<int>();
            model.EmployeedList = GetComBoxAccount(newListSelect);
            ViewBag.IsCreate = true;
            return PartialView("_AddOrEdit", model);
        }
      
        public IActionResult Edit(int id)
        {
            try
            {
                var model = new StoreModel();
                var res = htmlHelper.GetAsync<StoreModel>($"{UrlAPI.Url_GetByIdStore}?id={id}").GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    model = res.Data;
                    model.StoreTypeList = GetComBoxStoreType(model.StoreTypeId);
                    model.KeyList = GetComBoxAccount(model.KeyId);
                    model.EmployeedList = GetComBoxAccount(model.EmployeedId);
                }
                ViewBag.IsCreate = false;
                TempData["CurrentMenu"] = "Store";
                return PartialView("_AddOrEdit", model);
            }

            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public IActionResult AddOrEdit(StoreModel model)
        {
            try
            {
               if(model.Id == 0) // add
                {
                    var user = GetUser();
                    model.CreateBy = GetUser().Id;
                    model.CreateOn = DateTime.Now.Date;
                    model.ModifiedOn = DateTime.Now.Date;
                    model.OrganizationId = user.OrganizationId;
                    model.EmployeedId = model.EmployeedSelectId.Select(int.Parse).ToList();
                    // check tồn tại mã
                    var resCheck = htmlHelper.GetAsync<StoreModel>($"{UrlAPI.Url_GetByCodeStore}?code={model.Code}").GetAwaiter().GetResult();
                    if (resCheck.Data != null)
                        return Json(new { retCode = 1, retText = "Mã kho đã tồn tại" });
                    var res = htmlHelper.PostAsync<StoreModel>(UrlAPI.Url_AddStore, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        model = res.Data;
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Thêm mới thành công!" });
                        //return Redirect($"?id={model.Id }");
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                        //return Redirect($"?id={model.Id }");
                    }

                }
                else // edit
                {
                    model.ModifiedBy = GetUser().Id;
                    model.ModifiedOn = DateTime.Now.Date;
                    model.EmployeedId = model.EmployeedSelectId.Select(int.Parse).ToList();
                    var res = htmlHelper.PostAsyncStr(UrlAPI.Url_UpdateStore, model).GetAwaiter().GetResult();
                    if (res.RetCode == RetCodeEnum.Ok)
                    {
                        return Json(new { retCode = res.RetCode, url = $"", retText = "Cập nhật thành công!" });
                    }
                    else
                    {
                        return Json(new { retCode = RetCodeEnum.ApiError, url = "", retText = "Lỗi Hệ Thống" });
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {

                var res = htmlHelper.PostAsyncStr($"{UrlAPI.Url_DeleteStore}?id={id}", id).GetAwaiter().GetResult();
                if (res.RetCode == RetCodeEnum.Ok)
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
                else
                {
                    return Json(new { retCode = res.RetCode, retText = res.RetText });
                }
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
    }
}
