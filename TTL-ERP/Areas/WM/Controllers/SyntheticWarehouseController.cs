﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.Areas.WM.Models.StoredProcedure;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class SyntheticWarehouseController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public SyntheticWarehouseController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<SyntheticWarehouse>();

                var storeList = GetComBoxStore(0).ToList();
                //storeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.StoreList = storeList;

                var productList = GetComBoxProduct(0, 0).ToList();
                productList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.ProductList = productList;

                TempData["CurrentMenu"] = "SyntheticWarehouse";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(SyntheticWarehouseFillter fitter)
        {
            List<SyntheticWarehouse> listModel = new List<SyntheticWarehouse>();
            var model = new IndexViewModel<SyntheticWarehouse>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;

            if (!string.IsNullOrEmpty(fitter.FromDateStr))
                fitter.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fitter.FromDateStr));
            else
                fitter.FromDate = null;

            if (!string.IsNullOrEmpty(fitter.ToDateStr))
                fitter.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(fitter.ToDateStr));
            else
                fitter.ToDate = null;

            fitter.IsDownLoad = false;

            var res = htmlHelper.PostAsyncFitter<ResponseList<List<SyntheticWarehouse>>>($"{UrlAPI.Url_GetSyntheticWarehouse}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }

        public  IEnumerable<SelectListItem> GetEnumSelectList<T>()
        {
            var list = new List<SelectListItem>();
            foreach (BillTypeEnum colorEnum in Enum.GetValues(typeof(BillTypeEnum)))
            {
                list.Add(new SelectListItem()
                {
                    Text = StringUtil.GetDescriptionEnum(colorEnum),
                    Value = colorEnum.ToString()
                });
            };
            return list;
        }
    }
}
