﻿using Aspose.Cells;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Areas.WM.Models;
using TTL_ERP.ExeclOfficeEngine;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP.Areas.WM.Controllers
{
    [Area("WM")]
    [ServiceFilter(typeof(AuthorizeClient))]
    public class WarehouseController : BaseController
    {
        private readonly HttpClientHelper htmlHelper;
        private readonly UploadHelper uploadHelper;
        private int pageItems = 20;
        public WarehouseController(HttpClientHelper htmlHelper, UploadHelper uploadHelper) : base(htmlHelper, uploadHelper)
        {
            this.htmlHelper = htmlHelper;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var model = new IndexViewModel<BillModel>();
                var billCategoryList = GetComBoxBillCategory(0).ToList();
                billCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.BillCategoryList = billCategoryList;

                var billStatusList = GetComBoxBillStatus(0).ToList();
                billStatusList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.BillStatusList = billStatusList;

                var storeList = GetComBoxStore(0).ToList();
                storeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.StoreList = storeList;

                var storeExportList = GetComBoxStore(0).ToList();
                storeExportList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.StoreExportList = storeExportList;

                var supplierList = GetComBoxSupplier(0).ToList();
                supplierList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
                model.SupplierList = supplierList;

                model.FunctionList = GetListFunctionWareHouse();
                TempData["CurrentMenu"] = "Warehouse";
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public PartialViewResult LoadData(BillFitter fitter)
        {
            List<BillModel> listModel = new List<BillModel>();
            var model = new IndexViewModel<BillModel>();
            var user = GetUser();
            fitter.OrganizationId = user.OrganizationId;

            if (!string.IsNullOrEmpty(fitter.FromDateStr))
                fitter.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fitter.FromDateStr));
            else
                fitter.FromDate = null;

            if (!string.IsNullOrEmpty(fitter.ToDateStr))
                fitter.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(fitter.ToDateStr));
            else
                fitter.ToDate = null;

            var res = htmlHelper.PostAsyncFitter<ResponseList<List<BillModel>>>($"{UrlAPI.Url_GetAllBill}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                listModel = res.Data.ListData;

                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "");
                model.ListItems = listModel;
            }
            return PartialView("_TableResult", model);
        }
        public IActionResult CreatePopUp()
        {
            var model = new IndexViewModel<ProductModel>();
            var productCategoryList = GetComBoxProductCategory(0).ToList();
            productCategoryList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductCategoryList = productCategoryList;

            var productTypeList = GetComBoxProductType(0).ToList();
            productTypeList.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = true });
            model.ProductTypeList = productTypeList;

            return PartialView("_CreatePopUp", model);
        }

        public PartialViewResult LoadDataProduct(ProductFitter fitter)
        {
            var model = new IndexViewModel<Product_StoreProcedure>();
            var user = GetUser();
            fitter.ProductCategoryId = fitter.PopupProductCategoryId;
            fitter.ProductTypeId = fitter.PopupProductTypeId;
            fitter.OrganizationId = user.OrganizationId;
            // Lấy tên kho
            var resStore = htmlHelper.GetAsync<StoreModel>($"{UrlAPI.Url_GetByIdStore}?id={fitter.StoreId}").GetAwaiter().GetResult();
            ViewBag.StoreName = resStore.Data?.Title;
            var res = htmlHelper.PostAsyncFitter<ResponseList<List<Product_StoreProcedure>>>($"{UrlAPI.Url_GetAllProductQuantityEnd}" +
                $"?limit={pageItems}&page={fitter.Page}", fitter).GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model.PagingViewModel = new PagingModel(res.Data.Paging.TotalRows, pageItems, fitter.Page, "");
                model.ListItems = res.Data.ListData.Where(z => z.Status).ToList();
            }
            return PartialView("_TableResultPopup", model);
        }
        public IActionResult Create(string contr)
        {
            TempData["CurrentMenu"] = "Warehouse";
            return Redirect($"/WM/{contr}/Create");
        }
        public IActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            string contr = string.Empty;
            var listFunc = GetListFunctionWareHouse();
            contr = GetController(id);
            return Redirect($"/WM/{contr}/Edit?id={id}");
        }
        public IActionResult Detail(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            string contr = string.Empty;
            var listFunc = GetListFunctionWareHouse();
            contr = GetController(id);
            return Redirect($"/WM/{contr}/Detail?id={id}");
        }

        private string GetController(int id)
        {
            string contr = "Warehouse";
            // get Phiếu
            var resBill = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
            if (resBill.RetCode == RetCodeEnum.Ok)
            {
                // get BillCategory
                var resCategory = htmlHelper.GetAsync<BillCategoryModel>($"{UrlAPI.Url_GetByIdBillCategory}?id={resBill.Data.BillCategoryId}").GetAwaiter().GetResult();
                switch (resCategory.Data.Code)
                {
                    case "NHNB":
                        contr = "ImportInternal";
                        break;
                    case "NHNCC":
                        contr = "ImportSupplier";
                        break;
                    case "NHKK":
                        contr = "ImportWareHouse";
                        break;
                    case "NHSPS":
                        contr = "ImportNumber";
                        break;
                    case "CK":
                        contr = "ExportTransfer";
                        break;
                    case "XK":
                        contr = "ExportCancel";
                        break;
                    case "XKTD":
                        contr = "ExportSell";
                        break;
                    case "XKSPS":
                        contr = "ExportNumber";
                        break;
                }
            }

            return contr;
        }

        public IActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            string contr = string.Empty;
            var listFunc = GetListFunctionWareHouse();
            contr = GetController(id);
            return Redirect($"/WM/{contr}/Delete?id={id}");
        }
        public IActionResult DetailInventory(int id)
        {
            TempData["CurrentMenu"] = "Warehouse";
            BillModel model = new BillModel();
            var res = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
                model.BillStatusList = GetComBoxBillStatus(model.BillStatusId);
                model.StoreList = GetComBoxStore(model.StoreId);
                model.BillDetail.ToList().ForEach(z =>
                {
                    z.UnitList = GetComBoxUnit(z.UnitId);
                    z.ProductList = GetComBoxProduct(z.ProductId, 0);
                });
            }
            model.CreateOnStr = string.Format("{0:dd/MM/yyyy}", model.CreateOn);
            ViewBag.IsCreate = false;
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetCodeMax(string type)
        {
            try
            {
                var res = htmlHelper.GetAsync<string>($"{UrlAPI.Url_GenerateCodeBill}?type={type}").GetAwaiter().GetResult();
                return Json(new { retCode = res.RetCode, retText = res.RetText, code = res.Data });
            }
            catch (Exception ex)
            {
                return Json(new { retCode = RetCodeEnum.ApiError, retText = "Lỗi hệ thống" });
            }
        }
        [HttpPost]
        public async Task<IActionResult> UpdateStatus300(int id, string billStatusCode)
        {
            try
            {
                return UpdateStatus(id, billStatusCode);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> UpdateStatus600(int id, string billStatusCode)
        {
            try
            {
                return UpdateStatus(id, billStatusCode);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        private IActionResult UpdateStatus(int id, string billStatusCode)
        {
            var user = GetUser();

            // Detail
            BillModel model = new BillModel();
            var res = htmlHelper.GetAsync<BillModel>($"{UrlAPI.Url_GetByIdBill}?id={id}").GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                model = res.Data;
            }
            // Check chưa chọn kho
            if (model.StoreId == 0)
                return Json(new { retCode = 9, url = $"", urlIndex = "/WM/Warehouse", retText = "Phiếu chưa chọn kho. Vui lòng chọn kho trước khi duyệt!" });
            // check tồn kho
            if (model.BillType == BillTypeEnum.ExportWareHouse || model.BillType == BillTypeEnum.TranferWareHouse) // Xuất kho -- Chuyển
            {
                ProductFitter fitter = new ProductFitter();
                fitter.StoreId = model.BillType == BillTypeEnum.TranferWareHouse ? model.StoreExportId : model.StoreId;
                fitter.OrganizationId = user.OrganizationId;

                string error = CheckQuantityEnd(fitter, model.BillDetail);

                if (!string.IsNullOrEmpty(error))
                    return Json(new { retCode = 8, retText = error });
            }

            var resCheck = htmlHelper.PostAsyncStr($"{UrlAPI.Url_UpdateBillStatus}?id={id}&billStatusCode={billStatusCode}", id).GetAwaiter().GetResult();
            if (resCheck.RetCode == RetCodeEnum.Ok)
            {
                if (billStatusCode == "300")
                    TempData["Message"] = GetScriptShowToast("success", "Duyệt thành công!");
                else if (billStatusCode == "600")
                {
                    if(model.BillType == BillTypeEnum.ImportWareHouse)
                        TempData["Message"] = GetScriptShowToast("success", "Đã nhập kho!");
                    else
                        TempData["Message"] = GetScriptShowToast("success", "Đã xuất kho!");
                }
                return Json(new { retCode = resCheck.RetCode, url = $"", urlIndex = "/WM/Warehouse", retText = "Duyệt thành công!" });
            }
            else
            {
                return Json(new { retCode = RetCodeEnum.ApiError, urlIndex = "", retText = "Lỗi Hệ Thống" });
            }
        }

        private IEnumerable<FunctionSystemModel> GetListFunctionWareHouse()
        {
            // get Function List WareHouse
            var user = GetUser();
            var res = htmlHelper.GetAsync<ResponseList<List<FunctionSystemModel>>>($"{UrlAPI.Url_GetRuleWareHouseAction}?applicationCode=&organizationId={user.OrganizationId}&userName={user.Username}&area={GetAreaName()}&controller=Warehouse")
               .GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                var listFunction = res.Data.ListData.Distinct().Where(z => !string.IsNullOrEmpty(z.Action) && z.Action != "Index").OrderBy(z=>z.Position).ToList();
                return listFunction;
            }
            return new List<FunctionSystemModel>();
        }

        public IActionResult Print()
        {
            List<ProductModel> list = new List<ProductModel>();
            list.Add(new ProductModel()
            {
                Code = "MH1",
                Title = "Mặt Hàng 1"
            });
            list.Add(new ProductModel()
            {
                Code = "MH2",
                Title = "Mặt Hàng 2"
            });
            var dataModel = list.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            excelItems.Add(new KeyValuePair<string, object>("Code", "PN0001"));
            excelItems.Add(new KeyValuePair<string, object>("CreateOn", "Ngày 04 tháng 03 năm 2023"));
            Excel.ExcelItems = excelItems;

            var stream = Excel.ExportReport(dataModel, Path.Combine(Directory.GetCurrentDirectory(), "Areas/WM/Report/ImportInternal/ImportInternal.xlsx"), false, OfficeType.XLSX);

            string fileName = "ImportInternal.xlsx";
            //return File(bytes, "application/octet-stream", fileName);
            return File(stream, OfficeContentType.PDF, fileName);
        }
    }
}
