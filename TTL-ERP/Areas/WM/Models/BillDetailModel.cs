﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class BillDetailModel : BaseEntityModel
    {
        public int Id { get; set; }
        public int BillId { get; set; }
        public int ProductId { get; set; }
        public IEnumerable<SelectListItem> ProductList { get; set; }
        public int UnitId { get; set; }
        public IEnumerable<SelectListItem> UnitList { get; set; }
        public int Quantity { get; set; }
        public string QuantityStr { get; set; }
        public int QuantityEnd { get; set; }
        public string QuantityEndStr { get; set; }
        public int QuantityReal { get; set; }
        public string QuantityRealStr { get; set; }
        public int QuantityFail { get; set; }
        public string QuantityFailStr { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get; set; }
        public decimal PriceDiscount { get; set; }
        public string PriceDiscountStr { get; set; }
        public decimal TotalPrice { get; set; }
        public string TotalPriceStr { get; set; }
        public string Note { get; set; }
        public bool Status { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public bool IsUnlimited { get; set; }
        public int OrganizationId { get; set; }
        public int DT_DiscountKindId { get; set; }
        public IEnumerable<SelectListItem> DiscountKindList { get; set; }
    }
}
