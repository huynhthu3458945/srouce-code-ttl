﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class BillModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName{ get; set; }
        public IEnumerable<SelectListItem> SupplierList { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public IEnumerable<SelectListItem> StoreList { get; set; }
        public int StoreExportId { get; set; }
        public string StoreExportName { get; set; }
        public IEnumerable<SelectListItem> StoreExportList { get; set; }
        public int BillCategoryId { get; set; }
        public string BillCategoryName { get; set; }
        public IEnumerable<SelectListItem> BillCategoryList { get; set; }
        public BillTypeEnum BillType { get; set; }
        public int OrganizationId { get; set; }
        public decimal TotalAmount { get; set; }
        public string TotalAmountStr { get; set; }
        public int BillStatusId { get; set; }
        public string BillStatusCode { get; set; }
        public string BillStatusName { get; set; }
        public IEnumerable<SelectListItem> BillStatusList { get; set; }
        public int DiscountValue { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public string TotalDiscountAmountStr { get; set; }
        public int OrderId { get; set; }
        public string Note { get; set; }
        public bool IsDelivery { get; set; }
        public decimal DeliveryFee { get; set; }
        public string DeliveryFeeStr { get; set; }
        public string CreateOnStr { get; set; }
        public IEnumerable<BillDetailModel> BillDetail { get; set; }
        public IEnumerable<BillDetailModel> Details { get; set; }
        public IEnumerable<StoreDetailModel> StoreDetail { get; set; }
        public IEnumerable<StoreDetailModel> Details1 { get; set; }

        public string OrderCode { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string QuantityStr { get; set; }
        public IEnumerable<SelectListItem> ProductList { get; set; }
        public int MT_DiscountKindId { get; set; }
        public IEnumerable<SelectListItem> DiscountKindList { get; set; }
        
    }
    public class BillFitter
    {
        public int SupplierId { get; set; }
        public int StoreId { get; set; }
        public int StoreExportId { get; set; }
        public int BillCategoryId { get; set; }
        public int BillStatusId { get; set; }
        public int OrganizationId { get; set; }
        public string FromDateStr { get; set; }
        public DateTime? FromDate { get; set; }
        public string ToDateStr { get; set; }
        public DateTime? ToDate { get; set; }
        public int Page { get; set; }
    }

    public  enum BillTypeEnum : int
    {
        [Description("Nhập kho")]
        ImportWareHouse = 1,
        [Description("Xuất kho")]
        ExportWareHouse = 2,
        [Description("Chuyển kho")]
        TranferWareHouse = 3
    }
}
