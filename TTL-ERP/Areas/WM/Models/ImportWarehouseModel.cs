﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Areas.WM.Models
{
    public class ImportWarehouseModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get; set; }
        public decimal CostPrice { get; set; }
        public string CostPriceStr { get; set; }
        public int Sale { get; set; }
        public DateTime? SaleDeadLine { get; set; }
        public string SaleDeadLineStr { get; set; }
        public int UnitId { get; set; }
        public int OrganizationId { get; set; }
        public bool Status { get; set; }
        public IEnumerable<Combo_ProductModel> ComboProductList { get; set; }
        public IEnumerable<Combo_ProductModel> Details { get; set; }
    }
    public class ImportWarehouseFitter
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public int OrganizationId { get; set; }
        public int Page { get; set; }
    }
}
