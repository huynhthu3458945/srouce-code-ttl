﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class InventoryDetailModel : BaseEntityModel
    {
        public int InventoryId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal CostPrice { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string QuantityStr { get; set; }
        public int QuantityActual { get; set; }
        public string QuantityActualStr { get; set; }
        public int QuantityDiff { get; set; }
        public string QuantityDiffStr { get; set; }
        public decimal DifferenceValue { get; set; }
        public string DifferenceValueStr { get; set; }
        public IEnumerable<SelectListItem> ProductList { get; set; }
    }
}
