﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class InventoryModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public int TotalQuantity { get; set; }
        public string TotalQuantityStr { get; set; }
        public int TotalQuantityActual { get; set; }
        public string TotalQuantityActualStr { get; set; }
        public int TotalQuantityDiff { get; set; }
        public string TotalQuantityDiffStr { get; set; }
        public DateTime? InventoryDate { get; set; }
        public string InventoryDateStr { get; set; }
        public int OrganizationId { get; set; }
        public bool Status { get; set; }
        public bool IsDifference { get; set; }
        public bool HandleDifference { get; set; }
        public string NoteDifference { get; set; }
        public int BillId { get; set; }
        public IEnumerable<InventoryDetailModel> InventoryDetail { get; set; }
        public IEnumerable<InventoryDetailModel> Details { get; set; }
        public IEnumerable<SelectListItem> StoreList { get; set; }
    }

    public class InventoryFitter
    {
        public int StoreId { get; set; }
        public int OrganizationId { get; set; }
        public string FromDateStr { get; set; }
        public DateTime? FromDate { get; set; }
        public string ToDateStr { get; set; }
        public DateTime? ToDate { get; set; }
        public int Page { get; set; }
    }

}
