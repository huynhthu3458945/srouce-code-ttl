﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Areas.WM.Models
{
    public class Master
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int ParentId { get; set; }
        public int ParentId1 { get; set; }
        public int ParentId2 { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }
        public List<SelectListItem> InventoryList { get; set; }
        public List<SelectListItem> ParentList { get; set; }
        public List<Detail> Details { get; set; }
    }

    public class Detail
    {
        public string InventoryId { get; set; }
        public string InventoryName { get; set; }
        public string UnitId { get; set; }
    }
    public class Inventory
    {
        public string InventoryId { get; set; }
        public string InventoryName { get; set; }
    }
}
