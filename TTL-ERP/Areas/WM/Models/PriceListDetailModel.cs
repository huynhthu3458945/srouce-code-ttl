﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class PriceListDetailModel : BaseEntityModel
    {
        public int Id { get; set; }
        public int PriceListId { get; set; }
        public int? ProductId { get; set; }
        public IEnumerable<SelectListItem> ProductList { get; set; }
        public int? ComboId { get; set; }
        public IEnumerable<SelectListItem> ComboList { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get; set; }
        public decimal CostPrice { get; set; }
        public string CostPriceStr { get; set; }
        public decimal PriceNew { get; set; }
        public string PriceNewStr { get; set; }
        public int OrganizationId { get; set; }
    }
}
