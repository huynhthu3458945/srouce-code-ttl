﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class PriceListModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public bool Status { get; set; }
        public DateTime? Startdate { get; set; }
        public string StartdateStr { get; set; }
        public DateTime? Enddate { get; set; }
        public string? EnddateStr { get; set; }
        public bool IsUnlimited { get; set; }
        public bool IsAgency { get; set; }
        public int OrganizationId { get; set; }
        public IEnumerable<PriceListDetailModel> PriceListDetail { get; set; }
        public IEnumerable<PriceListDetailModel> Details { get; set; }
    }
    public class PriceListFitter
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public int OrganizationId { get; set; }
        public int Page { get; set; }
    }
}
