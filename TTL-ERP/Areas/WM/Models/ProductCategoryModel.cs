﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class ProductCategoryModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public int ParentId { get; set; }
        public IEnumerable<SelectListItem> ParentList { get; set; }
        public string ParentName { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
    }
}
