﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class ProductModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public IFormFile AvatarImg { get; set; }
        public string Thumb { get; set; }
        public IFormFile ThumbImg { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ProductCategoryId { get; set; }
        public IEnumerable<SelectListItem> ProductCategoryList { get; set; }
        public int ProductTypeId { get; set; }
        public IEnumerable<SelectListItem> ProductTypeList { get; set; }
        public string Content { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get; set; }
        public decimal CostPrice { get; set; }
        public string CostPriceStr { get; set; }
        public int Sale { get; set; }
        public DateTime? SaleDeadLine { get; set; }
        public string SaleDeadLineStr { get; set; }
        //public string SaleDeadLineStr { get { return string.Format("{0:dd/MM/yyyy}", SaleDeadLine); } }
        public bool IsInventory { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public IEnumerable<SelectListItem> UnitList { get; set; }
        public bool IsBundledGift { get; set; }
        public int OrganizationId { get; set; }
        public int? SupplierId { get; set; }
        public int Mass { get; set; }
        public int WidthX{ get; set; }
        public int WidthY { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int StoreId { get; set; }
        public IEnumerable<SelectListItem> StoreList { get; set; }
        public IEnumerable<SelectListItem> SupplierList { get; set; }
        public bool Status { get; set; }
        public IEnumerable<BundledGiftModel> BundledGiftList { get; set; }
        public IEnumerable<BundledGiftModel> Details { get; set; }
        public IEnumerable<Product_AttributeModel> AttributeList { get; set; }
        public IEnumerable<Product_AttributeModel> Details1 { get; set; }
        public int QuantityEnd { get; set; }
    }

    public class BundledGiftModel
    {
        public int ProductId { get; set; }
        public int ProductBundled { get; set; }
        public IEnumerable<SelectListItem> ProductBundledList { get; set; }
        public int Quantity { get; set; }
        public string QuantityStr { get; set; }
        public int UnitIdBundled { get; set; }
        public int UnitId { get; set; }
        public IEnumerable<SelectListItem> UnitIdBundledList { get; set; }
        public string Note { get; set; }
    }

    public class ProductFitter
    {
        public int StoreId { get; set; }
        public int ProductCategoryId { get; set; }
        public int PopupProductCategoryId { get; set; }
        public int ProductTypeId { get; set; }
        public int PopupProductTypeId { get; set; }
        public string ProductTypeCode { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int OrganizationId { get; set; }
        public int Page { get; set; }
    }
}
