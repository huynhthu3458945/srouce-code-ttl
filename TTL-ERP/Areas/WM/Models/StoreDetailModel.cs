﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class StoreDetailModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string CodeValue { get; set; }
        public string Title { get; set; }
        public string SeriNumber { get; set; }
        public int StoreId { get; set; }
        public int BillId { get; set; }
        public int BillExportId { get; set; }
        public int ProductId { get; set; }
        public bool Status { get; set; }
    }
}
