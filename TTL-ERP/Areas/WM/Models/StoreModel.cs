﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class StoreModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string Address { get; set; }
        public bool IsPallet { get; set; }
        public int OrganizationId { get; set; }
        public int StoreTypeId { get; set; }
        public string StoreTypeName { get; set; }
        public string StoreTypeSelectId { get; set; }
        public IEnumerable<SelectListItem> StoreTypeList { get; set; }
        public int KeyId { get; set; }
        public string KeySelectId { get; set; }
        public IEnumerable<SelectListItem> KeyList { get; set; }
        public List<int> EmployeedId { get; set; }
        public List<string> EmployeedSelectId { get; set; }
        public IEnumerable<SelectListItem> EmployeedList { get; set; }

    }
}
