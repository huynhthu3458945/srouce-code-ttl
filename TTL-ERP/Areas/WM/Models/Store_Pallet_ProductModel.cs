﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class Store_Pallet_ProductModel : BaseEntityModel
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public int PalletId { get; set; }
        public string PalletName { get; set; }
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Avatar { get; set; }
        public string Note { get; set; }
    }
    public class PalletProductFitter
    {
        public int StoreId { get; set; }
        public int PalletId { get; set; }
        public string Code { get; set; }
        public int OrganizationId { get; set; }
        public int Page { get; set; }
    }
}
