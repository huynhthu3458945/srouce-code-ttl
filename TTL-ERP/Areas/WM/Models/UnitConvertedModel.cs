﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Models.SystemMode;

namespace TTL_ERP.Areas.WM.Models
{
    public class UnitConvertedModel : BaseEntityModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int BasicUnitId { get; set; }
        public IEnumerable<SelectListItem> BasicUnitList { get; set; }
        public int BasicUnitNumber { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
    }
}
