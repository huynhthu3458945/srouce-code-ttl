﻿using TTL_ERP.Common;
using TTL_ERP.Models.SystemMode;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using TTL_ERP.Helpers;
using TTL_ERP.Helpers.API;
using TTL_ERP.Areas.Administrator.Models;
using TTL_ERP.Models;
using TTL_ERP.Models.API;

namespace TTL_ERP
{
    public class AuthorizeClient : IAsyncAuthorizationFilter
    {
        readonly AppContext appSetting;
        readonly HttpClientHelper htmlHelper;

        public AuthorizeClient(AppContext appSetting, HttpClientHelper htmlHelper)
        {
            this.appSetting = appSetting;
            this.htmlHelper = htmlHelper;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext actionContext)
        {
            UserLoginVMs user = new UserLoginVMs();
            if (!actionContext.HttpContext.Request.Cookies.ContainsKey("AdminUser_tll"))
            {
                var userValue = WebUtility.UrlDecode(actionContext.HttpContext.Request.Cookies["AdminUser_tll"]);
                //UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                if (userValue == null)
                {
                    actionContext.Result = new RedirectToActionResult("Index", "Login", new { area = "Administrator" });
                }
            }
            else
            {
                var userValue = WebUtility.UrlDecode(actionContext.HttpContext.Request.Cookies["AdminUser_tll"]);
                UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                var areaName = string.Empty;
                object area = null;
                if (actionContext.RouteData.Values.TryGetValue("area", out area))
                {
                    areaName = area.ToString();
                }
                var controllerName = string.Empty;
                object controller = null;
                if (actionContext.RouteData.Values.TryGetValue("controller", out controller))
                {
                    controllerName = controller.ToString();
                }
                var actionName = string.Empty;
                object action = null;
                if (actionContext.RouteData.Values.TryGetValue("action", out action))
                {
                    actionName = action.ToString();
                }
                // get Rule
                if (userInfo.Role == "Normal")
                {
                    // Quyền của Administrator_full
                    var listControllerAdministrator_full = new List<string>() { "Client", "Application", "FunctionSystem", "Organization", "ProductMaps" };
                    // Quyền của SupperAdmin
                    var listControllerSupperAdmin = new List<string>() { "Account", "Role" };
                    if (areaName == "Administrator" && listControllerAdministrator_full.Contains(controllerName) || listControllerSupperAdmin.Contains(controllerName))
                    {
                        actionContext.Result = new RedirectToActionResult("Accessdenied", "Dashboard", new { area = "Administrator" });
                    }
                    else if (areaName != "Administrator" && controllerName != "Home")
                    {
                        RedirectAccessdenied(actionContext, userInfo, areaName, controllerName, actionName);
                    }
                }
                else if (userInfo.Role == "SupperAdmin")
                {
                    // Quyền của Administrator_full
                    var listControllerAdministrator_full = new List<string>() { "Client", "Application", "FunctionSystem", "Organization", "ProductMaps" };
                    if (areaName == "Administrator" && listControllerAdministrator_full.Contains(controllerName))
                    {
                        actionContext.Result = new RedirectToActionResult("Accessdenied", "Dashboard", new { area = "Administrator" });
                    }
                    else if (areaName != "Administrator" && controllerName != "Home")
                    {
                        RedirectAccessdenied(actionContext, userInfo, areaName, controllerName, actionName);
                    }
                }
                else if (userInfo.Role == "Administrator_full")
                {
                    // Quyền của SupperAdmin
                    var listControllerSupperAdmin = new List<string>() { "Account", "Role" };
                    if (areaName == "Administrator" && listControllerSupperAdmin.Contains(controllerName))
                    {
                        actionContext.Result = new RedirectToActionResult("Accessdenied", "Dashboard", new { area = "Administrator" });
                    }
                }

            }
        }

        private void RedirectAccessdenied(AuthorizationFilterContext actionContext, UserLoginVMs userInfo, string areaName, string controllerName, string actionName)
        {
            var res = htmlHelper.GetAsync<ResponseList<List<FunctionSystemModel>>>($"{UrlAPI.Url_GetRuleAction}?applicationCode=&organizationId={userInfo.OrganizationId}&userName={userInfo.Username}&area={areaName}&controller={controllerName}&action={actionName}")
                    .GetAwaiter().GetResult();
            if (res.RetCode == RetCodeEnum.Ok)
            {
                if (res.Data.ListData.Count == 0) // chưa đc phân quyền
                {
                    // hàm chung
                    List<string> listNotActionCheck = new List<string>() {"AddOrEdit", "LoadData", "LoadDataProduct", "LoadDataCombo", "CreatePopUp", "CreatePopUpCombo", "DownloadTemplateExcel", "ImportExcelFile", "CheckProductExistPallet", "GetCodeMax", "Detail", "GetProductByStoreId" };
                    if (!listNotActionCheck.Contains(actionName))//&& controllerName != "Warehouse") // wareHouse
                    {
                        // List Action Ajax return Json
                        var listActionAjax = new List<string>() { "Delete", "UpdateStatus", "UpdateStatus300", "UpdateStatus600" };
                        if (listActionAjax.Contains(actionName)) // Xóa thì check khác
                            actionContext.Result = new RedirectToActionResult("AccessdeniedJson", "Dashboard", new { area = "Administrator" });
                        else
                            actionContext.Result = new RedirectToActionResult("Accessdenied", "Dashboard", new { area = "Administrator" });
                    }

                }
                //else
                //{
                //    var listRuleAction = new List<FunctionSystemModel>();
                //    listRuleAction = res.Data.ListData.Where(z => !string.IsNullOrEmpty(z.Action) && z.Action.Equals(actionName)).ToList();
                //    if (listRuleAction.Count == 0)
                //    {
                //        if (actionName == "Delete") // Xóa thì check khác
                //            actionContext.Result = new RedirectToActionResult("AccessdeniedJson", "Dashboard", new { area = "Administrator" });
                //        else
                //            actionContext.Result = new RedirectToActionResult("Accessdenied", "Dashboard", new { area = "Administrator" });
                //    }
                //}

            }
        }
    }
}
