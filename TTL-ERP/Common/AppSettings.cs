﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Common
{
    public class AppSettings
    {
        public string DomainAPI { set; get; }
        public string PortAPI { set; get; }
    }
}
