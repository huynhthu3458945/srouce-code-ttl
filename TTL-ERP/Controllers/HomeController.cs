﻿using TTL_ERP.Areas.Administrator.Controllers;
using TTL_ERP.Models;
using TTL_ERP.Models.SystemMode;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace TTL_ERP.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var user = GetUser();
            if (user != null && user.Role.Equals("Administrator_full"))
                return RedirectToAction("IndexSystem", "Dashboard", new { area = "administrator" });
            else
                return RedirectToAction("Index", "Dashboard", new { area = "administrator" });
        }
        public UserLoginVMs GetUser()
        {
            UserLoginVMs user = new UserLoginVMs();
            if (HttpContext.Request.Cookies.ContainsKey("AdminUser_tll"))
            {
                var userValue = WebUtility.UrlDecode(HttpContext.Request.Cookies["AdminUser_tll"]);
                UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                return userInfo;
            }
            return null;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult PageNotFound()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
