﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.ExeclOfficeEngine
{
    /// <summary>
    /// Constant MINE content type
    /// </summary>
    public class OfficeContentType
    {
        #region ---- Const ----

        /// <summary>
        /// Content Type - Excel 2003 - 97
        /// application/vnd.ms-excel
        /// </summary>
        public const string XLS = "application/vnd.ms-excel";

        /// <summary>
        /// Content Type - Excel 2007 trở lên
        /// application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        /// </summary>
        public const string XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        /// <summary>
        /// Content Type - Excel template 2007 trở lên
        /// application/vnd.openxmlformats-officedocument.spreadsheetml.template
        /// </summary>
        public const string XLTX = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";

        /// <summary>
        /// application/msword
        /// </summary>
        public const string DOC = "application/msword";

        /// <summary>
        /// application/vnd.openxmlformats-officedocument.wordprocessingml.document
        /// </summary>
        public const string DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

        /// <summary>
        /// application/pdf
        /// </summary>
        public const string PDF = "application/pdf";

        #endregion ---- Const ----
    }

    /// <summary>
    /// Office Extension
    /// </summary>
    public class OfficeExtension
    {
        /// <summary>
        /// Excel 97-2003
        /// </summary>
        public const string XLS = "xls";

        /// <summary>
        /// Excel 2007 or upto
        /// </summary>
        public const string XLSX = "xlsx";

        /// <summary>
        /// PDF
        /// </summary>
        public const string PDF = "pdf";
    }

    /// <summary>
    /// Office Extension
    /// </summary>
    public enum OfficeType
    {
        /// <summary>
        /// Excel 97-2003
        /// </summary>
        XLS = 1,

        /// <summary>
        /// Excel 2007 or upto
        /// </summary>
        XLSX = 2,

        /// <summary>
        /// PDF
        /// </summary>
        PDF = 3
    }
}
