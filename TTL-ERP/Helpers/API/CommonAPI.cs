﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Helpers.API
{
    public class CommonAPI
    {
        private readonly IMapper mapper;

        private readonly HttpClientHelper httpHelper;

        public CommonAPI(HttpClientHelper _httpHelper, IMapper _mapper)
        {
            this.mapper = _mapper;
            this.httpHelper = _httpHelper;
        }
    }
}
