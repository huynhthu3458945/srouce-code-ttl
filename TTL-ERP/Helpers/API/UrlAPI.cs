﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Helpers.API
{
    public class UrlAPI
    {
        public const string Url_Login = "/api/vi-VN/User/Login";
        public const string Url_LoginAccountPortal = "/api/vi-VN/AccountPortal/Login";

        // Common 
        public const string Url_GetAllComboboxGender = "/api/vi-VN/Common/ComboboxGender";
        public const string Url_GetAllComboboxProvince = "/api/vi-VN/Common/GetAllComboboxProvince";
        public const string Url_GetAllComboboxDistrict = "/api/vi-VN/Common/GetAllComboboxDistrict";
        public const string Url_GetAllComBoxOrganizationByUserName = "/api/vi-VN/Common/GetAllComBoxOrganizationByUserName";
        public const string Url_GetAllOrganizationByUserName = "/api/vi-VN/Common/GetAllOrganizationByUserName";
        public const string Url_GetAllComboboxPalletByStoreId = "/api/vi-VN/Common/GetAllPalletByStoreId";
        public const string Url_GetAllComboboxBillStatus = "/api/vi-VN/Common/GetAllComboboxBillStatus";
        public const string Url_GetAllComboboxOrderStatus = "/api/vi-VN/Common/GetAllComboboxOrderStatus";
        public const string Url_GetAllComboboxTransport = "/api/vi-VN/Common/GetAllComboboxTransport";
        public const string Url_GetAllComboboxPayType = "/api/vi-VN/Common/GetAllComboboxPayType";
        public const string Url_GetAllComboboxProductNumber = "/api/vi-VN/Common/GetAllComboboxProductNumber";
        public const string Url_GetBillStatusByCode = "/api/vi-VN/Common/GetBillStatusByCode";
        public const string Url_GetBillOrderByCode = "/api/vi-VN/Common/GetOrderStatusByCode";
        public const string Url_GetAllProductQuantityEnd = "/api/vi-VN/Common/GetAllProductQuantityEnd";
        public const string Url_GetAllConfigPayment = "/api/vi-VN/Common/GetAllConfigPayment";
        public const string Url_GetAllComboboxDiscountKind = "/api/vi-VN/Common/GetAllComboboxDiscountKind";

        // Client
        public const string Url_GetAllClient = "/api/vi-VN/Client/GetAll";
        public const string Url_GetAllComboboxClient = "/api/vi-VN/Client/GetAllCombobox";
        public const string Url_GetByIdClient = "/api/vi-VN/Client/GetById";
        public const string Url_AddClient = "/api/vi-VN/Client/Add";
        public const string Url_UpdateClient = "/api/vi-VN/Client/Update";
        public const string Url_DeleteClient = "/api/vi-VN/Client/Delete";

        // Application
        public const string Url_GetAllApplication = "/api/vi-VN/Application/GetAll";
        public const string Url_GetAllComboboxApplication = "/api/vi-VN/Application/GetAllCombobox";
        public const string Url_GetApplication = "/api/vi-VN/Application/GetAll";
        public const string Url_GetByIdApplication = "/api/vi-VN/Application/GetById";
        public const string Url_AddApplication = "/api/vi-VN/Application/Add";
        public const string Url_UpdateApplication = "/api/vi-VN/Application/Update";
        public const string Url_DeleteApplication = "/api/vi-VN/Application/Delete";

        // FunctionSystem
        public const string Url_GetAllFunctionSystem = "/api/vi-VN/FunctionSystem/GetAll";
        public const string Url_GetAllComboboxTreeFunctionSystem = "/api/vi-VN/FunctionSystem/GetAllTreeCombobox";
        public const string Url_GetByIdFunctionSystem = "/api/vi-VN/FunctionSystem/GetById";
        public const string Url_AddFunctionSystem = "/api/vi-VN/FunctionSystem/Add";
        public const string Url_UpdateFunctionSystem = "/api/vi-VN/FunctionSystem/Update";
        public const string Url_DeleteFunctionSystem = "/api/vi-VN/FunctionSystem/Delete";
        public const string Url_GetMenu = "/api/vi-VN/FunctionSystem/GetMenu";
        public const string Url_GetRuleAction = "/api/vi-VN/FunctionSystem/GetRuleAction";
        public const string Url_GetRuleWareHouseAction = "/api/vi-VN/FunctionSystem/GetRuleWareHouseAction";

        // Organization
        public const string Url_GetAllOrganization = "/api/vi-VN/Organization/GetAll";
        public const string Url_GetByIdOrganization = "/api/vi-VN/Organization/GetById";
        public const string Url_GetAllComboboxOrganization = "/api/vi-VN/Organization/GetAllCombobox";
        public const string Url_GetByCodeOrganization = "/api/vi-VN/Organization/GetByCode";
        public const string Url_AddOrganization = "/api/vi-VN/Organization/Add";
        public const string Url_UpdateOrganization = "/api/vi-VN/Organization/Update";
        public const string Url_DeleteOrganization = "/api/vi-VN/Organization/Delete";
        public const string Url_GetTreeFunctionSysTem = "/api/vi-VN/Organization/GetTreeFunctionSysTem";
        public const string Url_CheckExistUserName = "/api/vi-VN/Organization/CheckExistUserName";

        // Role
        public const string Url_GetAllRole = "/api/vi-VN/Role/GetAll";
        public const string Url_GetByCodeRole = "/api/vi-VN/Role/GetByCode";
        public const string Url_GetByIdRole = "/api/vi-VN/Role/GetById";
        public const string Url_AddRole = "/api/vi-VN/Role/Add";
        public const string Url_UpdateRole = "/api/vi-VN/Role/Update";
        public const string Url_DeleteRole = "/api/vi-VN/Role/Delete";
        public const string Url_GetTreeFunctionOrganization = "/api/vi-VN/Role/GetTreeFunctionOrganization";

        // Account
        public const string Url_GetAllAccount = "/api/vi-VN/Account/GetAll";
        public const string Url_GetByIdAccount = "/api/vi-VN/Account/GetById";
        public const string Url_GetAllComboboxAccount = "/api/vi-VN/Account/GetAllAccount";
        public const string Url_AddAccount = "/api/vi-VN/Account/Add";
        public const string Url_UpdateAccount = "/api/vi-VN/Account/Update";
        public const string Url_DeleteAccount = "/api/vi-VN/Account/Delete";
        public const string Url_GetTreeRole = "/api/vi-VN/Account/GetTreeRole";

        // AccountPortal
        public const string Url_GetListCustomerByPhone = "/api/vi-VN/AccountPortal/GetListCustomerByPhone";
        public const string Url_AccountPortalAdd = "/api/vi-VN/AccountPortal/Add";
        public const string Url_AccountPortalGetById = "/api/vi-VN/AccountPortal/GetById";
        public const string Url_AccountPortalUpdate = "/api/vi-VN/AccountPortal/Update";
        public const string Url_AccountPortalChangePassword = "/api/vi-VN/AccountPortal/ChangePassword";
        public const string Url_AccountPortalSendOTP = "/api/vi-VN/AccountPortal/SendOTP";
        public const string Url_AccountPortalSendOTPCreate = "/api/vi-VN/AccountPortal/SendOTPCreate";

        // ProductMaps
        public const string Url_GetAllProductMaps = "/api/vi-VN/ProductMaps/GetAll";
        public const string Url_GetByProductMapsbyPorductId = "/api/vi-VN/ProductMaps/GetByProductMapsbyPorductId";
        public const string Url_GetProductMaps = "/api/vi-VN/ProductMaps/GetAll";
        public const string Url_GetByIdProductMaps = "/api/vi-VN/ProductMaps/GetById";
        public const string Url_AddProductMaps = "/api/vi-VN/ProductMaps/Add";
        public const string Url_UpdateProductMaps = "/api/vi-VN/ProductMaps/Update";
        public const string Url_DeleteProductMaps = "/api/vi-VN/ProductMaps/Delete";

        // StoreType 
        public const string Url_GetAllStoreType = "/api/vi-VN/StoreType/GetAll";
        public const string Url_GetByIdStoreType = "/api/vi-VN/StoreType/GetById";
        public const string Url_GetAllComboboxStoreType = "/api/vi-VN/StoreType/GetAllStoreType";
        public const string Url_GetByCodeStoreType = "/api/vi-VN/StoreType/GetByCode";
        public const string Url_AddStoreType = "/api/vi-VN/StoreType/Add";
        public const string Url_UpdateStoreType = "/api/vi-VN/StoreType/Update";
        public const string Url_DeleteStoreType = "/api/vi-VN/StoreType/Delete";

        // Store 
        public const string Url_GetAllStore = "/api/vi-VN/Store/GetAll";
        public const string Url_GetByIdStore = "/api/vi-VN/Store/GetById";
        public const string Url_GetAllComboboxStore = "/api/vi-VN/Store/GetAllStore";
        public const string Url_GetAllComboboxStoreNumber = "/api/vi-VN/Store/GetAllStoreNumber";
        public const string Url_GetAllComboboxStoreIsPallet = "/api/vi-VN/Store/GetAllStoreIsPallet";
        public const string Url_GetByCodeStore = "/api/vi-VN/Store/GetByCode";
        public const string Url_AddStore = "/api/vi-VN/Store/Add";
        public const string Url_UpdateStore = "/api/vi-VN/Store/Update";
        public const string Url_DeleteStore = "/api/vi-VN/Store/Delete";

        // Pallet 
        public const string Url_GetAllPallet = "/api/vi-VN/Pallet/GetAll";
        public const string Url_GetAllPalletProduct = "/api/vi-VN/Pallet/GetAllPalletProduct";
        public const string Url_GetAllComboboxPallet = "/api/vi-VN/Pallet/GetAllPallet";
        public const string Url_GetByIdPallet = "/api/vi-VN/Pallet/GetById";
        public const string Url_GetByCodePallet = "/api/vi-VN/Pallet/GetByCode";
        public const string Url_GetPalletProduct = "/api/vi-VN/Pallet/GetPalletProduct";
        public const string Url_AddPallet = "/api/vi-VN/Pallet/Add";
        public const string Url_AddPalletProduct = "/api/vi-VN/Pallet/AddPalletProduct";
        public const string Url_UpdatePallet = "/api/vi-VN/Pallet/Update";
        public const string Url_DeletePallet = "/api/vi-VN/Pallet/Delete";
        public const string Url_DeletePalletProduct = "/api/vi-VN/Pallet/DeletePalletProduct";

        // ProductCategory 
        public const string Url_GetAllProductCategory = "/api/vi-VN/ProductCategory/GetAll";
        public const string Url_GetByIdProductCategory = "/api/vi-VN/ProductCategory/GetById";
        public const string Url_GetAllComboboxProductCategory = "/api/vi-VN/ProductCategory/GetAllProductCategory";
        public const string Url_GetByCodeProductCategory = "/api/vi-VN/ProductCategory/GetByCode";
        public const string Url_AddProductCategory = "/api/vi-VN/ProductCategory/Add";
        public const string Url_UpdateProductCategory = "/api/vi-VN/ProductCategory/Update";
        public const string Url_DeleteProductCategory = "/api/vi-VN/ProductCategory/Delete";

        // ProductType 
        public const string Url_GetAllProductType = "/api/vi-VN/ProductType/GetAll";
        public const string Url_GetByIdProductType = "/api/vi-VN/ProductType/GetById";
        public const string Url_GetAllComboboxProductType = "/api/vi-VN/ProductType/GetAllProductType";
        public const string Url_GetByCodeProductType = "/api/vi-VN/ProductType/GetByCode";
        public const string Url_AddProductType = "/api/vi-VN/ProductType/Add";
        public const string Url_UpdateProductType = "/api/vi-VN/ProductType/Update";
        public const string Url_DeleteProductType = "/api/vi-VN/ProductType/Delete";

        // BillCategory 
        public const string Url_GetAllBillCategory = "/api/vi-VN/BillCategory/GetAll";
        public const string Url_GetByIdBillCategory = "/api/vi-VN/BillCategory/GetById";
        public const string Url_GetAllComboboxBillCategory = "/api/vi-VN/BillCategory/GetAllBillCategory";
        public const string Url_GetByCodeBillCategory = "/api/vi-VN/BillCategory/GetByCode";
        public const string Url_AddBillCategory = "/api/vi-VN/BillCategory/Add";
        public const string Url_UpdateBillCategory = "/api/vi-VN/BillCategory/Update";
        public const string Url_DeleteBillCategory = "/api/vi-VN/BillCategory/Delete";

        // Unit 
        public const string Url_GetAllUnit = "/api/vi-VN/Unit/GetAll";
        public const string Url_GetByIdUnit = "/api/vi-VN/Unit/GetById";
        public const string Url_GetAllComboboxUnit = "/api/vi-VN/Unit/GetAllUnit";
        public const string Url_GetByCodeUnit = "/api/vi-VN/Unit/GetByCode";
        public const string Url_AddUnit = "/api/vi-VN/Unit/Add";
        public const string Url_UpdateUnit = "/api/vi-VN/Unit/Update";
        public const string Url_DeleteUnit = "/api/vi-VN/Unit/Delete";

        // UnitConverted 
        public const string Url_GetAllUnitConverted = "/api/vi-VN/UnitConverted/GetAll";
        public const string Url_GetByIdUnitConverted = "/api/vi-VN/UnitConverted/GetById";
        public const string Url_GetByBasicUnitId = "/api/vi-VN/UnitConverted/GetByBasicUnitId";
        public const string Url_GetAllComboboxUnitConverted = "/api/vi-VN/UnitConverted/GetAllUnitConverted";
        public const string Url_GetByCodeUnitConverted = "/api/vi-VN/UnitConverted/GetByCode";
        public const string Url_AddUnitConverted = "/api/vi-VN/UnitConverted/Add";
        public const string Url_UpdateUnitConverted = "/api/vi-VN/UnitConverted/Update";
        public const string Url_DeleteUnitConverted = "/api/vi-VN/UnitConverted/Delete";

        // Supplier 
        public const string Url_GetAllSupplier = "/api/vi-VN/Supplier/GetAll";
        public const string Url_GetByIdSupplier = "/api/vi-VN/Supplier/GetById";
        public const string Url_GetAllComboboxSupplier = "/api/vi-VN/Supplier/GetAllSupplier";
        public const string Url_GetByCodeSupplier = "/api/vi-VN/Supplier/GetByCode";
        public const string Url_AddSupplier = "/api/vi-VN/Supplier/Add";
        public const string Url_UpdateSupplier = "/api/vi-VN/Supplier/Update";
        public const string Url_DeleteSupplier = "/api/vi-VN/Supplier/Delete";

        // Product 
        public const string Url_GetAllProduct = "/api/vi-VN/Product/GetAll";
        public const string Url_GetByIdProduct = "/api/vi-VN/Product/GetById";
        public const string Url_GetAllComboboxProduct = "/api/vi-VN/Product/GetAllProduct";
        public const string Url_GetAllComboboxProductIsInventory = "/api/vi-VN/Product/GetAllProductIsInventory";
        public const string Url_GetByCodeProduct = "/api/vi-VN/Product/GetByCode";
        public const string Url_AddProduct = "/api/vi-VN/Product/Add";
        public const string Url_UpdateProduct = "/api/vi-VN/Product/Update";
        public const string Url_DeleteProduct = "/api/vi-VN/Product/Delete";

        // Combo 
        public const string Url_GetAllCombo = "/api/vi-VN/Combo/GetAll";
        public const string Url_GetByIdCombo = "/api/vi-VN/Combo/GetById";
        public const string Url_GetAllComboboxCombo = "/api/vi-VN/Combo/GetAllCombo";
        public const string Url_GetByCodeCombo = "/api/vi-VN/Combo/GetByCode";
        public const string Url_AddCombo = "/api/vi-VN/Combo/Add";
        public const string Url_UpdateCombo = "/api/vi-VN/Combo/Update";
        public const string Url_DeleteCombo = "/api/vi-VN/Combo/Delete";


        // Attributes 
        public const string Url_GetAllAttributes = "/api/vi-VN/Attributes/GetAll";
        public const string Url_GetByIdAttributes = "/api/vi-VN/Attributes/GetById";
        public const string Url_GetAllComboboxAttributes = "/api/vi-VN/Attributes/GetAllAttributes";
        public const string Url_GetByCodeAttributes = "/api/vi-VN/Attributes/GetByCode";
        public const string Url_AddAttributes = "/api/vi-VN/Attributes/Add";
        public const string Url_UpdateAttributes = "/api/vi-VN/Attributes/Update";
        public const string Url_DeleteAttributes = "/api/vi-VN/Attributes/Delete";

        // PriceList 
        public const string Url_GetAllPriceList = "/api/vi-VN/PriceList/GetAll";
        public const string Url_GetByIdPriceList = "/api/vi-VN/PriceList/GetById";
        public const string Url_GetAllPriceListboxPriceList = "/api/vi-VN/PriceList/GetAllPriceList";
        public const string Url_GetAllPriceListAgencyPriceList = "/api/vi-VN/PriceList/GetAllPriceAgencyList";
        public const string Url_GetByCodePriceList = "/api/vi-VN/PriceList/GetByCode";
        public const string Url_AddPriceList = "/api/vi-VN/PriceList/Add";
        public const string Url_UpdatePriceList = "/api/vi-VN/PriceList/Update";
        public const string Url_DeletePriceList = "/api/vi-VN/PriceList/Delete";

        // Bill 
        public const string Url_GetAllBill = "/api/vi-VN/Bill/GetAll";
        public const string Url_GetByIdBill = "/api/vi-VN/Bill/GetById";
        public const string Url_GetByCodeBill = "/api/vi-VN/Bill/GetByCode";
        public const string Url_GenerateCodeBill = "/api/vi-VN/Bill/GenerateCode";
        public const string Url_AddBill = "/api/vi-VN/Bill/Add";
        public const string Url_UpdateBill = "/api/vi-VN/Bill/Update";
        public const string Url_UpdateBillStatus = "/api/vi-VN/Bill/UpdateStatus";
        public const string Url_DeleteBill = "/api/vi-VN/Bill/Delete";
        public const string Url_GetAllProductNumber = "/api/vi-VN/Bill/GetAllProductNumber";

        // Inventory 
        public const string Url_GetAllInventory = "/api/vi-VN/Inventory/GetAll";
        public const string Url_GetByIdInventory = "/api/vi-VN/Inventory/GetById";
        public const string Url_GetByCodeInventory = "/api/vi-VN/Inventory/GetByCode";
        public const string Url_GenerateCodeInventory = "/api/vi-VN/Inventory/GenerateCode";
        public const string Url_AddInventory = "/api/vi-VN/Inventory/Add";
        public const string Url_UpdateInventory = "/api/vi-VN/Inventory/Update";
        public const string Url_DeleteInventory = "/api/vi-VN/Inventory/Delete";
        public const string Url_Balance = "/api/vi-VN/Inventory/Balance";

        // Report 
        public const string Url_GetImportExportList = "/api/vi-VN/WM/Report/GetImportExportList";
        public const string Url_GetSyntheticWarehouse = "/api/vi-VN/WM/Report/GetSyntheticWarehouse";


        // CRM ---------------------------------------- CRM
        //1. CustomerType 
        public const string Url_GetAllCustomerType = "/api/vi-VN/CustomerType/GetAll";
        public const string Url_GetAllComboboxCustomerType = "/api/vi-VN/CustomerType/GetAllCustomerType";
        public const string Url_GetByIdCustomerType = "/api/vi-VN/CustomerType/GetById";
        public const string Url_GetByCodeCustomerType = "/api/vi-VN/CustomerType/GetByCode";
        public const string Url_AddCustomerType = "/api/vi-VN/CustomerType/Add";
        public const string Url_UpdateCustomerType = "/api/vi-VN/CustomerType/Update";
        public const string Url_DeleteCustomerType = "/api/vi-VN/CustomerType/Delete";

        //2. CustomerStatus 
        public const string Url_GetAllCustomerStatus = "/api/vi-VN/CustomerStatus/GetAll";
        public const string Url_GetAllComboboxCustomerStatus = "/api/vi-VN/CustomerStatus/GetAllCustomerStatus";
        public const string Url_GetByIdCustomerStatus = "/api/vi-VN/CustomerStatus/GetById";
        public const string Url_GetByCodeCustomerStatus = "/api/vi-VN/CustomerStatus/GetByCode";
        public const string Url_AddCustomerStatus = "/api/vi-VN/CustomerStatus/Add";
        public const string Url_UpdateCustomerStatus = "/api/vi-VN/CustomerStatus/Update";
        public const string Url_DeleteCustomerStatus = "/api/vi-VN/CustomerStatus/Delete";

        //3. Customer
        public const string Url_GetAllCustomer = "/api/vi-VN/Customer/GetAll";
        public const string Url_GetAllComboboxCustomer = "/api/vi-VN/Customer/GetAllCustomer";
        public const string Url_GetAllByPhone = "/api/vi-VN/Customer/GetAllByPhone";
        public const string Url_GetAllByPhoneStore = "/api/vi-VN/Customer/GetAllByPhoneStore";
        public const string Url_GetByIdCustomer = "/api/vi-VN/Customer/GetById";
        public const string Url_GetByCodeCustomer = "/api/vi-VN/Customer/GetByCode";
        public const string Url_GenerateCodeCustomer = "/api/vi-VN/Customer/GenerateCode";
        public const string Url_AddCustomer = "/api/vi-VN/Customer/Add";
        public const string Url_AddCustomerAgency = "/api/vi-VN/Customer/Add-Agency";
        public const string Url_UpdateCustomer = "/api/vi-VN/Customer/Update";
        public const string Url_DeleteCustomer = "/api/vi-VN/Customer/Delete";


        // PO -------------------------------------- PO
        // Channel 
        public const string Url_GetAllChannel = "/api/vi-VN/Channel/GetAll";
        public const string Url_GetByIdChannel = "/api/vi-VN/Channel/GetById";
        public const string Url_GetAllComboboxChannel = "/api/vi-VN/Channel/GetAllChannel";
        public const string Url_GetByCodeChannel = "/api/vi-VN/Channel/GetByCode";
        public const string Url_AddChannel = "/api/vi-VN/Channel/Add";
        public const string Url_UpdateChannel = "/api/vi-VN/Channel/Update";
        public const string Url_DeleteChannel = "/api/vi-VN/Channel/Delete";

        // Order 
        public const string Url_GetAllOrder = "/api/vi-VN/Order/GetAll";
        public const string Url_GetByIdOrder = "/api/vi-VN/Order/GetById";
        public const string Url_GetViewById = "/api/vi-VN/Order/GetViewById";
        public const string Url_GetByCodeOrder = "/api/vi-VN/Order/GetByCode";
        public const string Url_GenerateCodeOrder = "/api/vi-VN/Order/GenerateCode";
        public const string Url_AddOrder = "/api/vi-VN/Order/Add";
        public const string Url_UpdateOrder = "/api/vi-VN/Order/Update";
        public const string Url_UpdateOrderStatus = "/api/vi-VN/Order/UpdateStatus";
        public const string Url_ResponeOnePay = "/api/vi-VN/Order/ResponeOnepay";
        public const string Url_ResponeNganLuong = "/api/vi-VN/Order/ResponeNganLuong";
        public const string Url_CheckEndQuantityProductNumber = "/api/vi-VN/Order/CheckEndQuantityProductNumber";
        public const string Url_CreatePayMent = "/api/vi-VN/Order/CreatePayMent";
        public const string Url_DeleteOrder = "/api/vi-VN/Order/Delete";
        public const string Url_GetListOrderByPhoneParent = "/api/vi-VN/Order/GetListOrderByPhoneParent";

        // HRM -------------------------------------- HRM
        // Department 
        public const string Url_GetAllDepartment = "/api/vi-VN/Department/GetAll";
        public const string Url_GetByIdDepartment = "/api/vi-VN/Department/GetById";
        public const string Url_GetAllComboboxDepartment = "/api/vi-VN/Department/GetAllDepartment";
        public const string Url_GetByCodeDepartment = "/api/vi-VN/Department/GetByCode";
        public const string Url_AddDepartment = "/api/vi-VN/Department/Add";
        public const string Url_UpdateDepartment = "/api/vi-VN/Department/Update";
        public const string Url_DeleteDepartment = "/api/vi-VN/Department/Delete";

        // StaffType 
        public const string Url_GetAllStaffType = "/api/vi-VN/StaffType/GetAll";
        public const string Url_GetByIdStaffType = "/api/vi-VN/StaffType/GetById";
        public const string Url_GetAllComboboxStaffType = "/api/vi-VN/StaffType/GetAllStaffType";
        public const string Url_GetByCodeStaffType = "/api/vi-VN/StaffType/GetByCode";
        public const string Url_AddStaffType = "/api/vi-VN/StaffType/Add";
        public const string Url_UpdateStaffType = "/api/vi-VN/StaffType/Update";
        public const string Url_DeleteStaffType = "/api/vi-VN/StaffType/Delete";

        // Regency 
        public const string Url_GetAllRegency = "/api/vi-VN/Regency/GetAll";
        public const string Url_GetByIdRegency = "/api/vi-VN/Regency/GetById";
        public const string Url_GetAllComboboxRegency = "/api/vi-VN/Regency/GetAllRegency";
        public const string Url_GetByCodeRegency = "/api/vi-VN/Regency/GetByCode";
        public const string Url_AddRegency = "/api/vi-VN/Regency/Add";
        public const string Url_UpdateRegency = "/api/vi-VN/Regency/Update";
        public const string Url_DeleteRegency = "/api/vi-VN/Regency/Delete";

        // Staff 
        public const string Url_GetAllStaff = "/api/vi-VN/Staff/GetAll";
        public const string Url_GetByIdStaff = "/api/vi-VN/Staff/GetById";
        public const string Url_GetAllComboboxStaff = "/api/vi-VN/Staff/GetAllStaff";
        public const string Url_GetByCodeStaff = "/api/vi-VN/Staff/GetByCode";
        public const string Url_CheckUserNameStaff = "/api/vi-VN/Staff/CheckUserName";
        public const string Url_AddStaff = "/api/vi-VN/Staff/Add";
        public const string Url_UpdateStaff = "/api/vi-VN/Staff/Update";
        public const string Url_DeleteStaff = "/api/vi-VN/Staff/Delete";
        public const string Url_GetStafByAccountId = "/api/vi-VN/Staff/GetStafByAccountId";
    }
}
