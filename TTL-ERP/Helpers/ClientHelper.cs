﻿using TTL_ERP.Common;
using TTL_ERP.Models.SystemMode;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace TTL_ERP.Helpers
{
    public class ClientHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private IRequestCookieCollection _cookies => _httpContextAccessor.HttpContext.Request.Cookies;
        private HttpClientHandler clientHandler = new HttpClientHandler();
        private readonly AppSettings appSetting;

        public ClientHelper(AppSettings _app, IHttpContextAccessor httpContextAccessor)
        {
            appSetting = _app;
            _httpContextAccessor = httpContextAccessor;
        }

        public string SessionValue(string key)
        {
            return _session.GetString(key);
        }

        public UserLoginVMs CookieValue(string key)
        {
            if (_cookies.ContainsKey(key))
            {
                var userValue = WebUtility.UrlDecode(_cookies[key]);
                UserLoginVMs? userInfo = JsonSerializer.Deserialize<UserLoginVMs>(WebUtility.UrlDecode(userValue));
                return userInfo;
            }
            return null;
        }
        public void AddSession(string key, string value)
        {
            _session.SetString(key, value);
        }

        public void AddSessions(Dictionary<string, string> session)
        {
            foreach (KeyValuePair<string, string> item in session)
            {
                _session.SetString(item.Key, item.Value);
            }
        }
    }
}
