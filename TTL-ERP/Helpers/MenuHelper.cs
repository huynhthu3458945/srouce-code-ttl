﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTL_ERP.Models;

namespace TTL_ERP.Helpers
{
    public static class MenuHelper
    {
        private static StringBuilder buildTree = null;
        private static List<MenuModel> categories = null;
        private static string url = null;
        public static StringBuilder BuildMenu(List<MenuModel> categoryByUser, int parentID, string currentMenu)
        {
            categories = categoryByUser;
            buildTree = new StringBuilder();
            buildTree.Append("<ul class=\"menu-sub\">");
            // lay con cap 1
            var lst = categoryByUser.Where(d => d.ParentId > 0 && d.ParentId.Equals(parentID)).OrderBy(d => d.Position);
            foreach (var item in lst.ToList())
            {
                var child = categories.Where(d => d.ParentId > 0  && d.ParentId.Equals(item.Id)).OrderBy(d => d.Position);

                var treeMenuActive = "";
                if (item.Controller == currentMenu.ToString())
                {
                    treeMenuActive = "active open";
                }

                if (child.Count() > 0)
                {
                    
                    buildTree.Append($"<li class=\"menu-item {treeMenuActive}\"><a href=\"javascript:void(0);\" class=\"menu-link menu-toggle\">" + item.Title + "</a>");
                    GetChildRecursive(child.ToList(), currentMenu);
                }
                else
                {
                    if (string.IsNullOrEmpty(item.Area))
                        url = item.Controller + "/" + item.Action;
                    else
                        url = item.Area + "/" + item.Controller + "/" + item.Action;
                    buildTree.Append($"<li class=\"menu-item {treeMenuActive}\"><a href=\"/" + url + "\"  class=\"menu-link\" >" + item.Title + "</a>");
                }
                buildTree.Append("</li>");
            }
            buildTree.Append("</ul>");

            return buildTree;
        }

        // lay con cap 2...n
        private static void GetChildRecursive(List<MenuModel> listChildren, string currentMenu)
        {
            buildTree.Append("<ul class=\"menu-sub\">");
            foreach (var item in listChildren)
            {
                var child = categories.Where(d => d.ParentId > 0 && d.ParentId == item.Id);

                var treeMenuActive = "";
                if (item.Controller == currentMenu.ToString())
                {
                    treeMenuActive = "active open";
                }

                if (child.Count() > 0)
                {
                    buildTree.Append($"<li class=\"menu-item {treeMenuActive}\"><a href=\"javascript:void(0);\" class=\"menu-link menu-toggle\">" + item.Title + "</a>");
                    GetChildRecursive(child.ToList(), currentMenu);
                }
                else
                {
                    if (string.IsNullOrEmpty(item.Area))
                        url = item.Controller + "/" + item.Action;
                    else
                        url = item.Area + "/" + item.Controller + "/" + item.Action;
                    buildTree.Append($"<li class=\"menu-item {treeMenuActive}\"><a href=\"/" + url + "\" class=\"menu-link\">" + item.Title + "</a>");
                }

                buildTree.Append("</li>");

            }
            buildTree.Append("</ul>");
        }

        // GET: Path cua he thong
        //public static string GetAppPath()
        //{
        //    string appPath = HttpContext.Request.PathBase;
        //    if (appPath == string.Empty || appPath.Equals("/")) appPath = "";
        //    return appPath;
        //}
    }
}
