﻿using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTL_ERP.Models;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace TTL_ERP.Helpers
{
    public static class MvcHtmlHelper
    {
        public static HtmlString RenderPaging(this IHtmlHelper helper, PagingModel model, PagingStyleModel styleModel)
        {
            var strBuilder = new StringBuilder();
            if (model != null && model.HasPaging)
            {
                var actionForNextPage = (model.CurrentPage < model.TotalPages) ? BuildActionCode(model.ActionCode, (model.CurrentPage + 1)) : "";
                var actionForPreviousPage = (model.CurrentPage > 1) ? BuildActionCode(model.ActionCode, (model.CurrentPage - 1)) : "";
                strBuilder.Append("<nav aria-label='Page navigation'>");
                strBuilder.Append("<ul " + styleModel.DivContainerStyle + " class='pagination pagination-round justify-content-end'>");

                if (model.TotalPages > 1)
                {
                    if (model.CurrentPage > 1)
                    {
                        strBuilder.Append("<li class='page-item'><a class='page-link' " + actionForPreviousPage + " " + styleModel.ItemStyle + "><</a></li>");
                    }
                    if (model.TotalPages > 7)
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            if (i <= 3)
                            {
                                strBuilder.Append("<li class='page-item'><a class='page-link' " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                strBuilder.Append("<li class='page-item'><a class='page-link' href='javascript:void(0)'><span>...</span></a></li>");
                                break;
                            }
                        }
                        if ((model.TotalPages - model.CurrentPage) < 2)
                        {
                            for (var i = model.TotalPages - 2; i < model.CurrentPage; i++)
                            {
                                strBuilder.Append("<li class='page-item'><a class='page-link' " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }
                        strBuilder.Append("<li class='page-item " + styleModel.CurrentItemStyle + "'><a class='page-link' href='javascript:void(0)' >" + model.CurrentPage + "</a></li>");
                        if (model.CurrentPage < 3)
                        {
                            for (var i = model.CurrentPage + 1; i <= 3; i++)
                            {
                                strBuilder.Append("<li class='page-item'><a class='page-link' " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }

                        var hasDot = false;
                        for (var i = (model.CurrentPage + 1); i <= model.TotalPages; i++)
                        {
                            if ((model.TotalPages - i) < 3)
                            {
                                strBuilder.Append("<li class='page-item'><a class='page-link' " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                if (!hasDot)
                                {
                                    strBuilder.Append("<li class='page-item'><a class='page-link' href='javascript:void(0)'><span>...</span></a></li>");
                                    hasDot = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            strBuilder.Append("<li class='page-item'><a class='page-link' " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }
                        strBuilder.Append("<li class='page-item " + styleModel.CurrentItemStyle + "'><a class='page-link' href='javascript:void(0)'>" + model.CurrentPage + "</a></li>");
                        for (var i = model.CurrentPage + 1; i <= model.TotalPages; i++)
                        {
                            strBuilder.Append("<li class='page-item'><a class='page-link' " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }

                    }
                    if (model.CurrentPage < model.TotalPages)
                    {
                        strBuilder.Append("<li class='page-item'><a class='page-link' " + actionForNextPage + " " + styleModel.ItemStyle + ">></a></li>");
                    }
                }
                strBuilder.Append("</ul>");
                strBuilder.Append("</nav");
            }

            return new HtmlString(strBuilder.ToString());
        }
        private static string BuildActionCode(string strFormat, int pageIndex)
        {
            //return string.Format(strFormat, pageIndex);
            return $"onclick='LoadData({pageIndex})'";
        }

    }
}
