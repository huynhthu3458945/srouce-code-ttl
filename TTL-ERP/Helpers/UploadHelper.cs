﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Helpers
{
    public class UploadHelper
    {
        public string PathSave { get; set; }
        public string FileName { get; set; }
        public string FileSave { get; set; }
        public string UploadDirecotroy { get; set; }
        private readonly IHostingEnvironment hostingEnvironment;
        public UploadHelper(IHostingEnvironment environment)
        {
            hostingEnvironment = environment;
        }
        public void UploadFile(IFormFile file)
        {
            var uploadPath = Path.Combine(hostingEnvironment.WebRootPath, UploadDirecotroy);
            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            var fileName = GetUniqueFileName(file.FileName);
            var filePath = Path.Combine(uploadPath, fileName);

            FileName = fileName;
            FileSave = uploadPath;
            PathSave = filePath;
            using (var strem = File.Create(filePath))
            {
                file.CopyTo(strem);
            }
        }
        public void UploadFile(string fileName)
        {
            if (fileName != null)
            {
                
                FileName = GetUniqueFileName(fileName);
                FileSave = Path.Combine(hostingEnvironment.WebRootPath, "uploads");
                PathSave = Path.Combine(FileSave, FileName);
                FileInfo fileInfo = new FileInfo(fileName);
                using (var fileStream = new FileStream(PathSave, FileMode.Create))
                {
                    fileInfo.CopyTo(fileName);
                }
            }
        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }

        public void DeleteFileOnServer(string filePath, string fileName)
        {
            try
            {
                FileInfo file = new FileInfo(filePath + fileName);
                if (file.Exists)
                {
                    file.Delete();
                }
            }
            catch
            {
                //GỠ BỎ THUỘC TÍNH READ-ONLY CỦA FILE
                var psi = new ProcessStartInfo("cmd.exe")
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    RedirectStandardError = true,
                    WorkingDirectory = @filePath
                };

                //CHẠY TIẾN TRÌNH CMD
                var proccess = Process.Start(psi);

                //VIẾT DÒNG LỆNH VÀO CMD VÀ THỰC THI
                var streamWriter = proccess.StandardInput;
                streamWriter.WriteLine("attrib -a -h -r -s \"" + @filePath + "/" + fileName + "\"");
                streamWriter.WriteLine("exit");
                proccess.Close();
                streamWriter.Close();

                System.Threading.Thread.Sleep(1000);
                System.IO.File.Delete(@filePath + fileName);
            }
        }
    }
}
