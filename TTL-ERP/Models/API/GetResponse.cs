﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Models.API
{
    public enum RetCodeEnum
    {
        [Description("OK")]
        Ok = 0,
        [Description("Api Error")]
        ApiError = 1,
        [Description("Not Exists")]
        ResultNotExists = 2,
        [Description("Parammeters Invalid")]
        ParammetersInvalid = 3,
        [Description("Parammeters Not Found")]
        ParammetersNotFound = 4,
        [Description("Not delete")]
        ApiNoDelete = 5,
        [Description("Not Role")]
        ApiNotRole = 6
    }
    public class GetResponse<T>
    {
        public RetCodeEnum RetCode { get; set; }
        public string RetText { get; set; }
        public T Data { get; set; }
    }
    public class ResponseList<T>
    {
        public Paging Paging { get; set; }
        public T ListData { get; set; }
    }
}
