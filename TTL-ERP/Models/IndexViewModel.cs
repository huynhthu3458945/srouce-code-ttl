﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_ERP.Areas.Administrator.Models;

namespace TTL_ERP.Models
{
    public class IndexViewModel<T> where T : class
    {
        public IList<T> ListItems { get; set; }
        // Chức năng Action
        public IEnumerable<FunctionSystemModel> FunctionList { get; set; }
        // Chức năng cha
        public IEnumerable<SelectListItem> ParenList { get; set; }

       
        // Giới tính
        public int GenderId { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }

        // Quận/huyện
        public int DistrictId { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
        // Tỉnh/TP
        public int ProvinceId { get; set; }
        public IEnumerable<SelectListItem> ProvinceList { get; set; }

        // Khách hàng
        public IEnumerable<SelectListItem> CustomerList { get; set; }
        // Kiểu khách hàng
        public IEnumerable<SelectListItem> CustomerTypeList { get; set; }
        // Loại khách hàng
        public IEnumerable<SelectListItem> CustomerStatusList { get; set; }
        // Danh mục sản phẩm
        public IEnumerable<SelectListItem> ProductCategoryList { get; set; }
        // Loại sản phẩm
        public IEnumerable<SelectListItem>  ProductTypeList { get; set; }
        // Sản phẩm
        public IEnumerable<SelectListItem> ProductList { get; set; }
        // Sản phẩm số
        public IEnumerable<SelectListItem> KeyProductList { get; set; }
        // Tổ chức
        public IEnumerable<SelectListItem> OrganizationList { get; set; }

        // Kệ
        public IEnumerable<SelectListItem> PalletList { get; set; }
        public int StoreIdScreenPallet { get; set; }
        public int PalletIdScreenPallet { get; set; }

        // Kiểu phiếu
        public IEnumerable<SelectListItem> BillCategoryList { get; set; }
        // Kho nhập
        public string StoreId { get; set; }
        public IEnumerable<SelectListItem> StoreList { get; set; }
        // Kho xuất
        public IEnumerable<SelectListItem> StoreExportList { get; set; }
        // Trạng thái phiếu
        public IEnumerable<SelectListItem> BillStatusList { get; set; }
        // Nhà cung cấp
        public IEnumerable<SelectListItem> SupplierList { get; set; }

        // Loại đơn
        public IEnumerable<SelectListItem> BillTypeList { get; set; }
        // Trạng thái đơn hàng
        public IEnumerable<SelectListItem> OrderStatusList { get; set; }


        // Phòng ban
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
        // Loại nhân viên
        public IEnumerable<SelectListItem> StaffTypeList { get; set; }
        // Chức vụ
        public IEnumerable<SelectListItem> RegencyList { get; set; }
        public int OrganizationId { get; set; }
        //public FilterViewModel FilterViewModel { get; set; }
        public PagingModel PagingViewModel { get; set; }
    }
}
