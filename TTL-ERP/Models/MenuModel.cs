﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Models
{
    public class MenuModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public int ParentId { get; set; }
        public int Position { get; set; }
    }
}
