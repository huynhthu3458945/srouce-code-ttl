﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Models
{
    public class PagingStyleModel
    {
        public string ItemStyle { get; set; }

        public string DivContainerStyle { get; set; }

        public string CurrentItemStyle { get; set; }
    }
}
