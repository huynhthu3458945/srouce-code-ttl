﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Models
{
    public class ResponseCombo
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
