﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_ERP.Models.SystemMode
{
    public class UserLoginVMs
    {
        public string DivisionID { get; set; }
        public int Id { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public int OrganizationId { get; set; }
        public string Role { get; set; }
    }
}
