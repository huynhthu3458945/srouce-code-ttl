﻿using TTL_ERP.Common;
using TTL_ERP.Helpers;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using License = ASOFT.ERP.ASoftLicense;

namespace TTL_ERP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCaching();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(typeof(AutoMapping));
            services.AddControllersWithViews();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(24);

            });

            // Execl
            services.AddScoped<License.ASoftReportLicense>();
            License.ASoftReportLicense.SetExcelLicense();

            services.AddSingleton<AppContext>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton(Configuration.GetSection("AppSettings").Get<AppSettings>());

            services.AddScoped<ClientHelper>();
            services.AddScoped<HttpClientHelper>();
            services.AddScoped<UploadHelper>();
            services.AddScoped<AuthorizeClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404)
                {
                    context.Request.Path = "/Home/PageNotFound";
                    await next();
                }
            });

            app.UseResponseCaching();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                // Đơn hàng
                endpoints.MapAreaControllerRoute(
                    name: "Đơn hàng",
                    areaName: "PO",
                    pattern: "PO/{controller=Home}/{action=Index}"
                );

                // Quan hệ khách hàng
                endpoints.MapAreaControllerRoute(
                     name: "Quan Hệ Khách Hàng",
                     areaName: "CRM",
                     pattern: "CRM/{controller=Home}/{action=Index}"
                 );

                // Kho
                endpoints.MapAreaControllerRoute(
                    name: "Kho",
                    areaName: "WM",
                    pattern: "WM/{controller=Home}/{action=Index}"
                );
                // Nhân sự
                endpoints.MapAreaControllerRoute(
                    name: "Nhân sự",
                    areaName: "HRM",
                    pattern: "HRM/{controller=Home}/{action=Index}"
                );

                endpoints.MapAreaControllerRoute(
                 name: "Administrator",
                 areaName: "Administrator",
                 pattern: "Administrator/{controller=Home}/{action=Index}/{id?}"
             );
                endpoints.MapAreaControllerRoute(
                name: "Administrator",
                areaName: "Administrator",
                pattern: "{controller=Home}/{action=Index}/{id?}"
            );

                endpoints.MapControllerRoute(
                  name: "areaRoute",
                  pattern: "{area:exists}/{controller}/{action}"
              );
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
