﻿document.addEventListener('DOMContentLoaded', function (e) {
    (function () {
        const formValidationExamples = document.getElementById('frm');
        const fv = FormValidation.formValidation(formValidationExamples, {
            fields: {
                UserName: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập tài khoản'
                        }
                    }
                },
                Password: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập mật khẩu'
                        }
                    }
                }
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap5: new FormValidation.plugins.Bootstrap5({

                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                autoFocus: new FormValidation.plugins.AutoFocus()
            },
            init: instance => {
                instance.on('plugins.message.placed', function (e) {
                    //* Move the error message out of the `input-group` element
                    if (e.element.parentElement.classList.contains('input-group')) {

                        e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
                    }
                });
            }
        });
        const submitButton = document.getElementById('btnSubmit');
        submitButton.addEventListener('click', function (e) {
            // Prevent default button action
            e.preventDefault();

            // Validate form before submit
            if (fv) {
                fv.validate().then(function (status) {
                    console.log(status);
                    if (status == 'Valid') {
                        var data = GetFormDataJs('frm');
                        console.log(data);
                        var TreeRoleAccs = [];
                        $('.jstree-node').each(function (index, value) {
                            console.log($(this).attr('id'));
                            console.log($(this).find('.jstree-undetermined').length > 0 ? true : $(this).attr('aria-selected'));
                            var id = $(this).attr('id');
                            var value = $(this).find('.jstree-undetermined').length > 0 ? true : $(this).attr('aria-selected');
                            TreeRoleAccs.push({ Id: $(this).attr('id'), IsCheck: (value == undefined ? ($(this).find('.jstree-clicked').length ? true : false) : value) })
                        });
                        data.TreeRoleAccs = TreeRoleAccs;
                        if ($('#Id').val() == 0) {
                            url = "/Administrator/Account/Create";
                        }
                        else
                            url = "/Administrator/Account/Edit";

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            dataType: "json",
                            encode: true,
                            success: function (res) {
                                if (res.retCode == 0) {
                                    ShowToast('success', res.retText)
                                    if ($('#Id').val() == 0)
                                        window.location = res.url;

                                }
                                else if (res.retCode = 1) {
                                    $('#UserName').focus();
                                    ShowToast('error', res.retText);
                                }
                            },
                            error: function () {
                                console.log('Error - Failed');
                            }
                        });
                    }
                });
            }
        });
    })();
});

LoadJstree();

function LoadJstree() {
    var theme = $('html').hasClass('light-style') ? 'default' : 'default-dark',
        ajaxTree = $('#jstree-ajax')
    // Ajax Example
    // --------------------------------------------------------------------
    if (ajaxTree.length) {
        ajaxTree.jstree({
            core: {
                themes: {
                    name: theme
                },
                "data": {
                    "url": "/Administrator/Account/GetTreeRole?accountId=" + $('#Id').val(),
                    "type": "get",
                    "data": function (node) {
                        /*return { "id": node.id };*/
                    }
                }
            },
            plugins: ['types', 'checkbox'],
            types: {
                default: {
                    icon: 'ti ti-folder'
                },
                html: {
                    icon: 'ti ti-brand-html5 text-danger'
                },
                css: {
                    icon: 'ti ti-brand-css3 text-info'
                },
                img: {
                    icon: 'ti ti-photo text-success'
                },
                js: {
                    icon: 'ti ti-brand-javascript text-warning'
                }
            }
        });
    }
}

function GetFormDataJs(formid) {
    var form = document.getElementById(formid);
    let formdata = {};
    let valueRadio = ""
    Array.from(form.querySelectorAll('input, select, textarea'))
        .filter(element => element.name)
        .forEach(element => {
            if (element.type === 'checkbox') {
                formdata[element.name] = element.checked;
            }
            else if (element.type === 'radio') {
                //formdata[element.name] = getRadioCheckedValue(formid, element.name);
                if (element.checked) {
                    valueRadio = element.value;
                }
                formdata[element.name] = valueRadio;
            }
            else if (element.options) {
                var selected = [...element.selectedOptions].map(option => option.value);
                //var selected = [...element.options].filter(option => option.selected).map(option => option.value);
                if (selected.length > 1) {
                    formdata[element.name] = selected;
                } else {
                    formdata[element.name] = element.value;
                }
            } else {
                if (element.id != '')
                    formdata[element.name] = element.value;
            }
        });
    return formdata;
}