﻿
document.addEventListener('DOMContentLoaded', function (e) {
    (function () {
        const formValidationExamples = document.getElementById('frm');
        const fv = FormValidation.formValidation(formValidationExamples, {
            fields: {
                Code: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập mã ứng dụng'
                        },
                        stringLength: {
                            min: 2,
                            max: 30,
                            message: 'Nhập lớn hơn 2 kí tự'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9]+$/,
                            message: 'Mã chỉ bao gồm chữ cái và số'
                        }
                    }
                },
                Title: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập tên ứng dụng'
                        },
                        stringLength: {
                            min: 2,
                            max: 30,
                            message: 'Nhập lớn hơn 6 kí tự'
                        }
                    }
                },

            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap5: new FormValidation.plugins.Bootstrap5({
                    // Use this for enabling/changing valid/invalid class
                    // eleInvalidClass: '',
                    //eleValidClass: '',
                    //rowSelector: function (field, ele) {
                    //    // field is the field name & ele is the field element
                    //    switch (field) {
                    //        case 'Code':
                    //        case 'Title':
                    //        default:
                    //            return '.row';
                    //    }
                    //}
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                autoFocus: new FormValidation.plugins.AutoFocus()
            },
            init: instance => {
                instance.on('plugins.message.placed', function (e) {
                    //* Move the error message out of the `input-group` element
                    if (e.element.parentElement.classList.contains('input-group')) {
                        // `e.field`: The field name
                        // `e.messageElement`: The message element
                        // `e.element`: The field element
                        e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
                    }
                    ////* Move the error message out of the `row` element for custom-options
                    //if (e.element.parentElement.parentElement.classList.contains('custom-option')) {
                    //    e.element.closest('.row').insertAdjacentElement('afterend', e.messageElement);
                    //}
                });
            }
        });
        //const submitButton = document.getElementById('btnSubmit');
        //submitButton.addEventListener('click', function (e) {
        //    // Prevent default button action
        //    e.preventDefault();
        //    // Validate form before submit
        //    if (formValidationExamples) {
        //        var formDataMaster = getJsonFormData(GetFormData('formValidationExamples'))
        //        fv.validate().then(function (status) {
        //            console.log(status);
        //            if (status == 'Valid') {
        //               $.ajax({
        //                   type: "POST",
        //                   url: '/Home/Submit',
        //                   data: formDataMaster,
        //                   success: function (res) {

        //                       if (res.statusCode == 0) {
        //                           ShowToast('success', 'Thành Công', 'Đã oke')
        //                       }
        //                       else if (res.statusCode = 1) {
        //                           ShowToast('error', 'Lỗi', res.mess)
        //                       }
        //                   },
        //                   error: function () {
        //                       console.log('Error - Failed');
        //                   }
        //               });
        //            }
        //        });
        //    }
        //});
        //? Revalidation third-party libs inputs on change trigger

    })();
});

