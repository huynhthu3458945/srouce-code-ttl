﻿
document.addEventListener('DOMContentLoaded', function (e) {
    (function () {
        const formValidationExamples = document.getElementById('frm');
        const fv = FormValidation.formValidation(formValidationExamples, {
            fields: {
                Title: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập tên chức năng'
                        },
                        stringLength: {
                            min: 2,
                            max: 30,
                            message: 'Nhập lớn hơn 2 kí tự'
                        }
                    }
                },
                ApplicationSelectId: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng chọn ứng dụng'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap5: new FormValidation.plugins.Bootstrap5({
               
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                autoFocus: new FormValidation.plugins.AutoFocus()
            },
            init: instance => {
                instance.on('plugins.message.placed', function (e) {
                    //* Move the error message out of the `input-group` element
                    if (e.element.parentElement.classList.contains('input-group')) {
                     
                        e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
                    }
                });
            }
        });
    })();
});

