﻿
$(document).ready(function () {
    RefreshDynamicCommon();
    LoadJstree(0);

    //// Sau khi uploadFile
    fnc_AfterUpdateLoadFile = (inputId, data) => {
        if (inputId == 'LogoImg') {
            $('#Logo').val(data)
            $('#imgLogo').attr('src', data);
        }
    }

    if ($('#Id').val() == 0) {
        $('#imgLogo').attr('src', '/assets/img/no-image.png');
    }
    else {
        $('#imgLogo').attr('src', $('#Logo').val());
    }
});

document.addEventListener('DOMContentLoaded', function (e) {
    (function () {
        const formValidationExamples = document.getElementById('frm');
        const fv = FormValidation.formValidation(formValidationExamples, {
            fields: {
                Code: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập mã công ty'
                        }
                    }
                },
                Title: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập tên công ty'
                        }
                    }
                },
                OwnerName: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng nhập tên tài khoản'
                        }
                    }
                },
                ClientIdSelectId: {
                    validators: {
                        notEmpty: {
                            message: 'Vui lòng chọn tổ chức'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap5: new FormValidation.plugins.Bootstrap5({

                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                autoFocus: new FormValidation.plugins.AutoFocus()
            },
            init: instance => {
                instance.on('plugins.message.placed', function (e) {
                    //* Move the error message out of the `input-group` element
                    if (e.element.parentElement.classList.contains('input-group')) {

                        e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
                    }
                });
            }
        });
        const submitButton = document.getElementById('btnSubmit');
        submitButton.addEventListener('click', function (e) {
            // Prevent default button action
            e.preventDefault();

            // Validate form before submit
            if (fv) {
                fv.validate().then(function (status) {
                    console.log(status);
                    if (status == 'Valid') {
                        var data = GetFormData('frm');
                        console.log(data);
                        var TreeFuncSystems = [];
                        $('.jstree-node').each(function (index, value) {
                            console.log($(this).attr('id'));
                            console.log($(this).find('.jstree-undetermined').length > 0 ? true : $(this).attr('aria-selected'));
                            var id = $(this).attr('id');
                            var value = $(this).find('.jstree-undetermined').length > 0 ? true : $(this).attr('aria-selected');
                            TreeFuncSystems.push({ Id: $(this).attr('id'), IsCheck: (value == undefined ? ($(this).find('.jstree-clicked').length ? true : false) : value) })
                        });
                        data.TreeFuncSystems = TreeFuncSystems;
                        if ($('#Id').val() == 0) {
                            url = "/Administrator/Organization/Create";
                        }
                        else
                            url = "/Administrator/Organization/Edit";

                        // check tồn tai
                        var urlCheckUserName = "/Administrator/Organization/CheckExistUserName?id=" + $('#Id').val()+"&userName=" + $('#OwnerName').val();
                        $.ajax({
                            type: "GET",
                            url: urlCheckUserName,
                            dataType: "json",
                            encode: true,
                            success: function (res) {
                                if (res.retCode == 1) {
                                    $('#OwnerName').focus();
                                    ShowToast('error', res.retText);
                                }
                                else {
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: data,
                                        dataType: "json",
                                        encode: true,
                                        success: function (res) {
                                            if (res.retCode == 0) {
                                                ShowToast('success', res.retText)
                                                if ($('#Id').val() == 0)
                                                    window.location = res.url;

                                            }
                                            else if (res.retCode = 1) {
                                                $('#Code').focus();
                                                ShowToast('error', res.retText);
                                            }
                                        },
                                        error: function () {
                                            console.log('Error - Failed');
                                        }
                                    });
                                }
                            },
                            error: function () {
                                console.log('Error - Failed');
                            }
                        });
                    }
                });
            }
        });
    })();
});



function LoadJstree(idApplication) {
    var theme = $('html').hasClass('light-style') ? 'default' : 'default-dark',
        ajaxTree = $('#jstree-ajax')
    // Ajax Example
    // --------------------------------------------------------------------
    if (ajaxTree.length) {
        ajaxTree.jstree({
            core: {
                themes: {
                    name: theme
                },
                "data": {
                    "url": "/Administrator/Organization/GetGetTreeFunctionSysTem?organizationId=" + $('#Id').val() + "&applicationId=" + idApplication,
                    "type": "get",
                    "data": function (node) {
                        /*return { "id": node.id };*/
                    }
                }
            },
            plugins: ['types', 'checkbox'],
            types: {
                default: {
                    icon: 'ti ti-folder'
                },
                html: {
                    icon: 'ti ti-brand-html5 text-danger'
                },
                css: {
                    icon: 'ti ti-brand-css3 text-info'
                },
                img: {
                    icon: 'ti ti-photo text-success'
                },
                js: {
                    icon: 'ti ti-brand-javascript text-warning'
                }
            }
        });
    }
}

function changeApplication(selectedValue) {
    $('.jstree-ajax').html('<div id="jstree-ajax"></div>');
    LoadJstree(selectedValue);
}
