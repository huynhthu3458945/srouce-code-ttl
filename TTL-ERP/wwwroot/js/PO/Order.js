﻿var frm = 'frm';
var tabletbodyStr = '#tbodyProduct tbody';
var tbodyStr = 'tbodyProduct';
var urlGetCodeMax = '/PO/Order/GetCodeMax?type=DH';


$(document).ready(function () {
    RefreshDynamicCommon();
    LoadProduct();
    LoadUnit();
    LoadCombo();
    LoadDiscountKind();
    InitValue();

    $('#tbodyProduct tbody').Uk_RebuilIdAndName();
    $('#CreateOnStr').FormatDateTime();

    $('select').select2({
        selectOnClose: true
    });

    if ($('#IsDelivery').prop("checked"))
        $('#DeliveryFeeStr').prop("disabled", false);
    else
        $('#DeliveryFeeStr').prop("disabled", true);

    $('#IsDelivery').change(function () {
        if (this.checked)
            $('#DeliveryFeeStr').prop("disabled", false);
        else
            $('#DeliveryFeeStr').prop("disabled", true);
    });

    $('#DeliveryFeeStr, #DiscountValue, #MT_DiscountKindId').change(function () {
        fuc_TotalAmountGrid();
    });

    $('#btnSearchPhone').click(function () {
        bindDataCustomer();
    });

    $('#searchPhone').change(function () {
        bindDataCustomer();
    });
    //$('#Phone').change(function () {
    //    $.ajax({
    //        url: '/Base/GetCustomerByPhoneJson?phone=' + $(this).val(),
    //        type: 'GET',
    //        dataType: 'json',
    //        success: function (res) {
    //            if (res.retCode == 0) {
    //                if (res.data != null) {
    //                    $('#CustomerCode').val(res.data.code);
    //                    $('#FullName').val(res.data.fullName);
    //                    $('#Email').val(res.data.email);
    //                    $('#Phone').val(res.data.phone);
    //                    $('#Address').val(res.data.address);
    //                    $('#CompanyName').val(res.data.companyName);
    //                } else {
    //                    $('#CustomerCode').val('');
    //                    $('#FullName').val('');
    //                    $('#Email').val('');
    //                    //$('#Phone').val('');
    //                    $('#Address').val('');
    //                    $('#CompanyName').val('');
    //                }
    //            }
    //        },
    //        error: function () {
    //            console.log('error');
    //        }
    //    });
    //});

    //$('#CustomerCode').change(function () {
    //    $.ajax({
    //        url: '/Base/GetCustomerByCodeJson?code=' + $(this).val(),
    //        type: 'GET',
    //        dataType: 'json',
    //        success: function (res) {
    //            if (res.retCode == 0) {
    //                if (res.data != null) {
    //                    $('#CustomerCode').val(res.data.code);
    //                    $('#FullName').val(res.data.fullName);
    //                    $('#Email').val(res.data.email);
    //                    $('#Phone').val(res.data.phone);
    //                    $('#Address').val(res.data.address);
    //                    $('#CompanyName').val(res.data.companyName);
    //                } else {
    //                    //$('#CustomerCode').val('');
    //                    $('#FullName').val('');
    //                    $('#Email').val('');
    //                    $('#Phone').val('');
    //                    $('#Address').val('');
    //                    $('#CompanyName').val('');
    //                }
    //            }
    //        },
    //        error: function () {
    //            console.log('error');
    //        }
    //    });
    //});

    $('#CustomerId').change(function () {
        if ($(this).val() != 0) {
            $.ajax({
                url: '/Base/GetCustomerJson?id=' + $(this).val(),
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    if (res.retCode == 0) {
                        if (res.data != null) {
                            $('#FullName').val(res.data.fullName);
                            $('#Email').val(res.data.email);
                            $('#Phone').val(res.data.phone);
                            $('#Address').val(res.data.address);
                            $('#CompanyName').val(res.data.companyName);
                        } else {
                            $('#FullName').val('');
                            $('#Email').val('');
                            $('#Phone').val('');
                            $('#Address').val('');
                            $('#CompanyName').val('');
                        }
                    }
                },
                error: function () {
                    console.log('error');
                }
            });
        }
        else {
            $('#FullName').val('');
            $('#Email').val('');
            $('#Phone').val('');
            $('#Address').val('');
            $('#CompanyName').val('');
        }
    });

    $('#PriceId').change(function () {
        if ($(this).val() != 0) {
            $.ajax({
                url: '/Base/GetPriceListJson?id=' + $(this).val(),
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    if (res.retCode == 0) {
                        $tableBody = $(tabletbodyStr);
                        for (var i = 0; i < $tableBody.find("tr").length; i++) {
                            var $trNew = $tableBody.find("tr:eq(" + i + ")");

                            // productId
                            var productId = $trNew.find(':input[name="ProductId"]').val();
                            var comboId = $trNew.find(':input[name="ComboId"]').val();

                            var originalPrice = $trNew.find(':input[name="OriginalPrice"]');
                            var price = $trNew.find(':input[name="Price"]');
                            var priceStr = $trNew.find(':input[name="PriceStr"]');
                            var hasVaue = false;
                            res.data.priceListDetail.forEach(product => {
                                if (product.productId != undefined && productId == product.productId) {
                                    price.val(product.priceNew)
                                    priceStr.val(FormatNumberStr(product.priceNew));
                                    hasVaue = true;
                                }
                                else if (product.comboId != undefined && comboId == product.comboId) {
                                    price.val(product.priceNew)
                                    priceStr.val(FormatNumberStr(product.priceNew));
                                    hasVaue = true;
                                }

                            });
                            if (!hasVaue) {
                                price.val(originalPrice.val())
                                priceStr.val(FormatNumberStr(originalPrice.val()));
                            }

                        }
                        fuc_TotalAmountGrid();
                    } else {

                    }

                },
                error: function () {
                    console.log('error');
                }
            });
        }
        else {
            $tableBody = $(tabletbodyStr);
            for (var i = 0; i < $tableBody.find("tr").length; i++) {
                var $trNew = $tableBody.find("tr:eq(" + i + ")");


                var originalPrice = $trNew.find(':input[name="OriginalPrice"]');
                var price = $trNew.find(':input[name="Price"]');
                var priceStr = $trNew.find(':input[name="PriceStr"]');

                price.val(originalPrice.val())
                priceStr.val(FormatNumberStr(originalPrice.val()));

            }
            fuc_TotalAmountGrid();
        }
    });

    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(true, frm, tbodyStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(true, frm, tbodyStr);
            }
        });
    });

    // trước khi showPopup
    //fnc_BeforeShowPopup = () => {
    //    if ($('#StoreId').val() == '') {
    //        ShowToast('warning', 'Vui lòng chọn kho!');
    //        $('#StoreId').focus();
    //        return false;
    //    }
    //    return true;
    //}

    fnc_AfterFormatNumberGrid = () => {
        fuc_TotalAmountGrid();
    }

    fnc_AfterDeletRowGrid = () => {
        fuc_TotalAmountGrid();
    }

    fnc_AfterChangeDiscountKindGrid = () => {
        fuc_TotalAmountGrid();
    }


    var dateCreate = $('#CreateOnStr').val();
    // Sau khi submit và tiếp tục 
    fnc_AfterSubmitFormAndContinue = () => {
        // get Code 
        GetCodeMax(urlGetCodeMax, '#Code');
        $('#CreateOnStr').val(dateCreate);
        $(tabletbodyStr).html('');
    }
    // sau khi reset
    fnc_AfterResetForm = () => {
        $('#CreateOnStr').val(dateCreate);
    }

    // Master
    $('#TotalDiscountAmountStr').FormatNumber('#TotalDiscountAmount')
    $('#TotalAmountStr').FormatNumber('#TotalAmount')
    $('#DeliveryFeeStr').FormatNumber('#DeliveryFee')

    // Lưới
    $('[name="QuantityStr"]').FormatNumberGrid('Quantity', $(tabletbodyStr))
    $('[name="OriginalPriceStr"]').FormatNumberGrid('OriginalPrice', $(tabletbodyStr))
    $('[name="PriceStr"]').FormatNumberGrid('Price', $(tabletbodyStr))
    $('[name="PriceDiscountStr"]').FormatNumberGrid('PriceDiscount', $(tabletbodyStr))
    $('[name="TotalPriceStr"]').FormatNumberGrid('TotalPrice', $(tabletbodyStr))
});


function fuc_TotalAmountGrid() {
    TotalAmountGridPO('Quantity', 'PriceDiscount', 'Price', 'TotalPrice', 'DiscountValue', 'DeliveryFee', 'TotalDiscountAmount', 'TotalAmount', 'MT_DiscountKindId', 'DT_DiscountKindId', $(tabletbodyStr));
}

function fuc_TotalAmountMaster() {
    TotalAmountMasterPO('DiscountValue', 'DeliveryFee', 'TotalDiscountAmount', 'TotalAmount', 'MT_DiscountKindId')
}

function bindDataCustomer() {
    BlockLoading('.card-master');
    $.ajax({
        url: '/Base/GetCustomerByPhoneJson?phone=' + $('#searchPhone').val(),
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            if (res.retCode == 0) {
                if (res.data != null) {
                    //$('#CustomerCodeParent').val(res.data.ref_Code_ERP);
                    $('#CustomerPhoneParent').val(res.data.ref_Phone);
                    $('#CustomerCode').val(res.data.code);
                    $('#FullName').val(res.data.fullName);
                    $('#Email').val(res.data.email);
                    $('#Phone').val(res.data.phone);
                    $('#Address').val(res.data.address);
                    $('#CompanyName').val(res.data.companyName);
                } else {
                    //$('#CustomerCodeParent').val('');
                    $('#CustomerPhoneParent').val('');
                    $('#CustomerCode').val('');
                    $('#FullName').val('');
                    $('#Email').val('');
                    //$('#Phone').val('');
                    $('#Address').val('');
                    $('#CompanyName').val('');
                }
            }
            UnblockLoading('.card-master');
        },
        error: function () {
            UnblockLoading('.card-master');
            console.log('error');
        }
    });
}

let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            TransportId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn hình thức vận chuyển'
                    }
                }
            },
            PayTypeId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn hình thức thanh toán'
                    }
                }
            },
            OrderStatusId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn trạng thái'
                    }
                }
            },
            CreateOnStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập ngày nhập phiếu'
                    }
                }
            },
            FullName: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập họ tên'
                    }
                }
            },
            Email: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập Email'
                    }
                }
            },
            Phone: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số điện thoại'
                    }
                }
            },
            SalePhone: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập Số điện người chốt sale'
                    }
                }
            },
            //UnitId: {
            //    // The children's date of birth are inputs with class .childDob
            //    selector: '.UnitId',
            //    // The field is placed inside .col-xs-4 div instead of .form-group
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng chọn đơn vị tính'
            //        }
            //    }
            //},
            QuantityStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.Quantity',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

function InitValue() {
    $('[name="DT_DiscountKindId"]').UK_ChangeDiscountKind($(tabletbodyStr));
    $('[name="PriceDiscountStr"]').UK_ChangePriceDiscount($(tabletbodyStr));
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyProduct').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}

function LoadProduct() {
    var str = '';
    $.ajax({
        url: '/Base/GetProductComboxJson?productTypeCode=',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ProductListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}

function LoadUnit() {
    var str = '';
    $.ajax({
        url: '/Base/GetUnitComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#UnitListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
function LoadCombo() {
    var str = '';
    $.ajax({
        url: '/Base/GetComboComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ComboListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
function LoadDiscountKind() {
    var str = '';
    $.ajax({
        url: '/Base/GetDiscountKindJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#DiscountKindListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}

/**
 *  Form Wizard
 */

'use strict';

//$(function () {
//    const select2 = $('.select2');
//    // select2
//    if (select2.length) {
//        select2.each(function () {
//            var $this = $(this);
//            $this.wrap('<div class="position-relative"></div>');
//            $this.select2({
//                placeholder: 'Select value',
//                dropdownParent: $this.parent()
//            });
//        });
//    }
//});
(function () {
    // Numbered Wizard
    // --------------------------------------------------------------------
    const wizardNumbered = document.querySelector('.wizard-numbered'),
        wizardNumberedBtnNextList = [].slice.call(wizardNumbered.querySelectorAll('.btn-next')),
        wizardNumberedBtnPrevList = [].slice.call(wizardNumbered.querySelectorAll('.btn-prev')),
        wizardNumberedBtnSubmit = wizardNumbered.querySelector('.btn-submit');

    if (typeof wizardNumbered !== undefined && wizardNumbered !== null) {
        const numberedStepper = new Stepper(wizardNumbered, {
            linear: false
        });
        if (wizardNumberedBtnNextList) {
            wizardNumberedBtnNextList.forEach(wizardNumberedBtnNext => {
                wizardNumberedBtnNext.addEventListener('click', event => {
                    numberedStepper.next();
                });
            });
        }
        if (wizardNumberedBtnPrevList) {
            wizardNumberedBtnPrevList.forEach(wizardNumberedBtnPrev => {
                wizardNumberedBtnPrev.addEventListener('click', event => {
                    numberedStepper.previous();
                });
            });
        }
        if (wizardNumberedBtnSubmit) {
            wizardNumberedBtnSubmit.addEventListener('click', event => {
                alert('Submitted..!!');
            });
        }
    }
})();
