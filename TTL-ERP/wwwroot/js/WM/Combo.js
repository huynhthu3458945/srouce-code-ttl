﻿var frm = 'frm';
var tabletbodyStr = '#tbodyContact tbody';
var tbodyStr = 'tbodyContact';

$(document).ready(function () {
    RefreshDynamicCommon();
    LoadProductBundled();
    LoadUnitBundled();
    InitValue();

    $('#tbodyContact tbody').Uk_RebuilIdAndName();
    $('#SaleDeadLineStr').FormatDateTime();
  
    $('select').select2({
        selectOnClose: true
    });

    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(true, frm, tbodyStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(true, frm, tbodyStr);
            }
        });
    });

    // Sau khi submit
    //fnc_AfterSubmitForm = (res) => {
    //    ShowToast('success', res.retText)
    //    location.href = res.urlIndex;
    //}

    //// Sau khi uploadFile
    fnc_AfterUpdateLoadFile = (inputId, data) => {
        if (inputId == 'AvatarImg') {
            $('#Avatar').val(data)
            $('#imgAvatar').attr('src', data);
        }
        else if (inputId == 'ThumbImg') {
            $('#Thumb').val(data)
            $('#imgThumb').attr('src', data);
        }
    }
    if ($('#Id').val() == 0) {
        $('#imgAvatar').attr('src', '/assets/img/no-image.png');
        $('#imgThumb').attr('src', '/assets/img/no-image.png');

    }
    else {
        $('#imgAvatar').attr('src', $('#Avatar').val());
        $('#imgThumb').attr('src', $('#Thumb').val());
    }

    // Master
    $('#PriceStr').FormatNumber('#Price');
    $('#CostPriceStr').FormatNumber('#CostPrice');

    // Lưới
    $('[name="QuantityStr"]').FormatNumberGrid('Quantity', $(tabletbodyStr))

});


let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã'
                    }
                }
            },
            Title: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập tên'
                    }
                }
            },
            CostPriceStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá vốn'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }
                }
            },
            PriceStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }
                }
            },
            QuantityStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.Quantity',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }
                    
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when a'#tbodyContact tbody' valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

function InitValue() {
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyContact').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}

function LoadProductBundled() {
    var str = '';
    $.ajax({
        url: '/Base/GetProductComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ProductListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}

function LoadUnitBundled() {
    var str = '';
    $.ajax({
        url: '/Base/GetUnitComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#UnitListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
