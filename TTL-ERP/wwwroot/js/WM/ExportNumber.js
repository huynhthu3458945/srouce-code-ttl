﻿var frm = 'frm';
var tabletbodyStr = '#tbodyProduct tbody';
var tbodyStr = 'tbodyProduct';
var urlGetCodeMax = '/WM/Warehouse/GetCodeMax?type=PX';


$(document).ready(function () {
    RefreshDynamicCommon();
    LoadProduct();
    LoadUnit();
    InitValue();

    $('#tbodyProduct tbody').Uk_RebuilIdAndName();
    $('#CreateOnStr').FormatDateTime();

    $('select').select2({
        selectOnClose: true
    });

    if ($('#IsDelivery').prop("checked"))
        $('#DeliveryFeeStr').prop("disabled", false);
    else
        $('#DeliveryFeeStr').prop("disabled", true);

    $('#IsDelivery').change(function () {
        if (this.checked)
            $('#DeliveryFeeStr').prop("disabled", false);
        else
            $('#DeliveryFeeStr').prop("disabled", true);
    });

    $('#DeliveryFeeStr, #DiscountValue').change(function () {
        fuc_TotalAmountGrid();
    });

    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(true, frm, '', tbodyStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(true, frm, '', tbodyStr);
            }
        });
    });

    // trước khi showPopup
    fnc_BeforeShowPopup = () => {
        if ($('#ProductId').val() == '') {
            ShowToast('warning', 'Vui lòng chọn sản phẩm!');
            $('#ProductId').focus();
            return false;
        }
        return true;
    }

    fnc_AfterFormatNumberGrid = () => {
        fuc_TotalAmountGrid();
    }

    fnc_AfterDeletRowGrid = () => {
        fuc_TotalAmountGrid();
    }



    var dateCreate = $('#CreateOnStr').val();
    // Sau khi submit và tiếp tục 
    fnc_AfterSubmitFormAndContinue = () => {
        // get Code 
        GetCodeMax(urlGetCodeMax, '#Code');
        $('#CreateOnStr').val(dateCreate);
        $(tabletbodyStr).html('');
    }
    // sau khi reset
    fnc_AfterResetForm = () => {
        $('#CreateOnStr').val(dateCreate);
    }

    // Master
    $('#TotalDiscountAmountStr').FormatNumber('#TotalDiscountAmount')
    $('#TotalAmountStr').FormatNumber('#TotalAmount')
    $('#DeliveryFeeStr').FormatNumber('#DeliveryFee')
    $('#QuantityStr').FormatNumber('#Quantity')

    // Lưới
    //$('[name="QuantityStr"]').FormatNumberGrid('Quantity', $(tabletbodyStr))
    //$('[name="QuantityEndStr"]').FormatNumberGrid('QuantityEnd', $(tabletbodyStr))
    //$('[name="QuantityRealStr"]').FormatNumberGrid('QuantityReal', $(tabletbodyStr))
    //$('[name="QuantityFailStr"]').FormatNumberGrid('QuantityFail', $(tabletbodyStr))
    //$('[name="PriceStr"]').FormatNumberGrid('Price', $(tabletbodyStr))
    //$('[name="PriceDiscountStr"]').FormatNumberGrid('PriceDiscount', $(tabletbodyStr))
    //$('[name="TotalPriceStr"]').FormatNumberGrid('TotalPrice', $(tabletbodyStr))
});


function fuc_TotalAmountGrid() {
    TotalAmountGrid('QuantityReal', 'PriceDiscount', 'Price', 'TotalPrice', 'DiscountValue', 'DeliveryFee', 'TotalDiscountAmount', 'TotalAmount', $(tabletbodyStr));
}

function fuc_TotalAmountMaster() {
    TotalAmountMaster('DiscountValue', 'DeliveryFee', 'TotalDiscountAmount', 'TotalAmount')
}

let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            StoreId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn kho nhập'
                    }
                }
            },
            ProductId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn sản phẩm'
                    }
                }
            },
            BillStatusId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn trạng thái'
                    }
                }
            },
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã phiếu'
                    }
                }
            },
            CreateOnStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập ngày nhập phiếu'
                    }
                }
            },
             Quantity: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá'
                    },
                    regexp: {
                        regexp: /[1-9]/,
                        message: 'Vui lòng nhập số lượng lớn hơn 0'
                    }
                }
            },
            
            //PriceStr: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng nhập giá'
            //        },
            //        regexp: {
            //            regexp: /[0-9]/,
            //            message: 'Định dạng số không hợp lệ'
            //        }
            //    }
            //},

            CodeValue: {
                // The children's date of birth are inputs with class .childDob
                selector: '.CodeValue',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã code'
                    }
                }
            },
            SeriNumber: {
                // The children's date of birth are inputs with class .childDob
                selector: '.SeriNumber',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số seri'
                    }
                }
            },
            QuantityRealStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.QuantityReal',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

function InitValue() {
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyProduct').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}

function LoadProduct() {
    var str = '';
    $.ajax({
        url: '/Base/GetProductComboxJson?productTypeCode=',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ProductListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}

function LoadUnit() {
    var str = '';
    $.ajax({
        url: '/Base/GetUnitComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#UnitListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
function sendFile(file) {
    var formData = new FormData();
    formData.append("file", file);

    $.ajax({
        type: 'POST',
        url: '/WM/ImportNumber/ImportExcelFile',
        data: formData,
        contentType: false,
        processData: false,
        async: true,
        success: function (status) {
            if (status.success) {
                // Add the compiled html to the page
                //if ($(tabletbodyStr).find("tr").length == 0 || ($(tabletbodyStr).find("tr").length == 1 && $(tabletbodyStr).find("tr").find(":contains(không có dữ liệu)").length) == 1) {
                //    $(tabletbodyStr).html(status.htm);
                //}
                //else {
                //    $(tabletbodyStr).append(status.htm);
                //}
                $('#tbodyProduct').html(status.html);

                InitValue();
            }
            $('#imgupload').val('');
        },
        processData: false,
        contentType: false,
        error: function () {
            alert('error');
            $('#imgupload').val('');
        }
    });
}