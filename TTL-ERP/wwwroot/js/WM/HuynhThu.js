﻿let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã'
                    }
                }
            },
            Title: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập tên'
                    }
                }
            },
            InventoryName: {
                // The children's date of birth are inputs with class .childDob
                selector: '.InventoryName',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mặt hàng'
                    }
                }
            }
            ,
            InventoryId: {
                // The children's date of birth are inputs with class .childDob
                selector: '.InventoryId',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mặt hàng'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

$(document).ready(function () {
    LoadInventory();
    InitValueContact();

    $('.btnAddRow').click(function () {
        //alert('aaa');
        var table = $('#tbodyContact tbody');
        var stt = table.find("tr").length + 1;
        var data = $('#InventoryHidden').val();
        var getTemplate = $("#template_subform").html();

        // Compile the template
        var compileTemplate = Handlebars.compile(getTemplate);
        var context = {
            "stt": stt,
            inventoryList: data
        };

        // Pass our data to the template
        var passDataToTemplate = compileTemplate(context);
        // Add the compiled html to the page
        $('#tbodyContact tbody').append(passDataToTemplate);

        InitValueContact();
        table.Uk_RebuildIndex();
        table.Uk_RebuildSortOrder();

        $('select').select2({
            selectOnClose: true
        });
    });

    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            console.log(status);
            if (status == 'Valid') {
                Save($("#frm"))
            }
        });
    });
});

function InitValueContact() {
    $('[name="btnRemoveLine"]').Uk_RemoveRow($('#tbodyContact tbody'));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyContact').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}

function LoadInventory() {
    var str = '';
    $.ajax({
        url: '/WM/HuynhThu/GetComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $.each(data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#InventoryHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
