﻿var frm = 'frm';
var tabletbodyStr = '#tbodyProduct tbody';
var tbodyStr = 'tbodyProduct';
var urlGetCodeMax = '/WM/Warehouse/GetCodeMax?type=PN';


$(document).ready(function () {
    RefreshDynamicCommon();
    LoadProduct();
    LoadUnit();
    LoadDiscountKind();
    InitValue();

    $('#tbodyProduct tbody').Uk_RebuilIdAndName();
    $('#CreateOnStr').FormatDateTime();

    $('select').select2({
        selectOnClose: true
    });

    if ($('#IsDelivery').prop("checked"))
        $('#DeliveryFeeStr').prop("disabled", false);
    else
        $('#DeliveryFeeStr').prop("disabled", true);

    $('#IsDelivery').change(function () {
        if (this.checked)
            $('#DeliveryFeeStr').prop("disabled", false);
        else
            $('#DeliveryFeeStr').prop("disabled", true);
    });

    $('#DeliveryFeeStr, #DiscountValue, #MT_DiscountKindId').change(function () {
        fuc_TotalAmountGrid();
    });

    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(true, frm, tbodyStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(true, frm, tbodyStr);
            }
        });
    });

    // trước khi showPopup
    fnc_BeforeShowPopup = () => {
        if ($('#StoreId').val() == '') {
            ShowToast('warning', 'Vui lòng chọn kho!');
            $('#StoreId').focus();
            return false;
        }
        return true;
    }

    fnc_AfterFormatNumberGrid = () => {
        fuc_TotalAmountGrid();
    }

    fnc_AfterDeletRowGrid = () => {
        fuc_TotalAmountGrid();
    }

    fnc_AfterChangeDiscountKindGrid = () => {
        fuc_TotalAmountGrid();
    }

    var dateCreate = $('#CreateOnStr').val();
    // Sau khi submit và tiếp tục 
    fnc_AfterSubmitFormAndContinue = () => {
        // get Code 
        GetCodeMax(urlGetCodeMax, '#Code');
        $('#CreateOnStr').val(dateCreate);
        $(tabletbodyStr).html('');
    }
    // sau khi reset
    fnc_AfterResetForm = () => {
        $('#CreateOnStr').val(dateCreate);
    }

    // Master
    $('#TotalDiscountAmountStr').FormatNumber('#TotalDiscountAmount')
    $('#TotalAmountStr').FormatNumber('#TotalAmount')
    $('#DeliveryFeeStr').FormatNumber('#DeliveryFee')

    // Lưới
    $('[name="QuantityStr"]').FormatNumberGrid('Quantity', $(tabletbodyStr))
    $('[name="QuantityEndStr"]').FormatNumberGrid('QuantityEnd', $(tabletbodyStr))
    $('[name="QuantityRealStr"]').FormatNumberGrid('QuantityReal', $(tabletbodyStr))
    $('[name="QuantityFailStr"]').FormatNumberGrid('QuantityFail', $(tabletbodyStr))
    $('[name="PriceStr"]').FormatNumberGrid('Price', $(tabletbodyStr))
    $('[name="PriceDiscountStr"]').FormatNumberGrid('PriceDiscount', $(tabletbodyStr))
    $('[name="TotalPriceStr"]').FormatNumberGrid('TotalPrice', $(tabletbodyStr))
});


function fuc_TotalAmountGrid() {
    TotalAmountGridPO('Quantity', 'PriceDiscount', 'Price', 'TotalPrice', 'DiscountValue', 'DeliveryFee', 'TotalDiscountAmount', 'TotalAmount', 'MT_DiscountKindId', 'DT_DiscountKindId', $(tabletbodyStr));
}

function fuc_TotalAmountMaster() {
    TotalAmountMasterPO('DiscountValue', 'DeliveryFee', 'TotalDiscountAmount', 'TotalAmount', 'MT_DiscountKindId')
}

let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            StoreId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn kho nhập'
                    }
                }
            },
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã phiếu'
                    }
                }
            },
            BillStatusId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn trạng thái'
                    }
                }
            },
            CreateOnStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập ngày nhập phiếu'
                    }
                }
            },
            //PriceStr: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng nhập giá'
            //        },
            //        regexp: {
            //            regexp: /[0-9]/,
            //            message: 'Định dạng số không hợp lệ'
            //        }
            //    }
            //},

            UnitId: {
                // The children's date of birth are inputs with class .childDob
                selector: '.UnitId',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn đơn vị tính'
                    }
                }
            },
            QuantityStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.Quantity',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            },
            QuantityRealStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.QuantityReal',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

function InitValue() {
    $('[name="DT_DiscountKindId"]').UK_ChangeDiscountKind($(tabletbodyStr));
    $('[name="PriceDiscountStr"]').UK_ChangePriceDiscount($(tabletbodyStr));
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyProduct').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}

function LoadProduct() {
    var str = '';
    $.ajax({
        url: '/Base/GetProductComboxJson?productTypeCode=',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ProductListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}

function LoadUnit() {
    var str = '';
    $.ajax({
        url: '/Base/GetUnitComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#UnitListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
function LoadDiscountKind() {
    var str = '';
    $.ajax({
        url: '/Base/GetDiscountKindJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#DiscountKindListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}