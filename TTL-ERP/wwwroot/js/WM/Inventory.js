﻿var frm = 'frm';
var tabletbodyStr = '#tbodyProduct tbody';
var tbodyStr = 'tbodyProduct';
var urlGetCodeMax = '/WM/Inventory/GetCodeMax?type=PX';

$(document).ready(function () {
    RefreshDynamicCommon();
    InitValue();

    $('#tbodyProduct tbody').Uk_RebuilIdAndName();

    $('#InventoryDateStr').FormatDateTime();

    $('select').select2({
        selectOnClose: true
    });


    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(true, frm, tbodyStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(true, frm, tbodyStr);
            }
        });
    });


    fnc_AfterFormatNumberGrid = () => {
        fuc_TotalAmountGrid();
    }

    fnc_AfterDeletRowGrid = () => {
        fuc_TotalAmountGrid();
    }



    var dateCreate = $('#CreateOnStr').val();
    // Sau khi submit và tiếp tục 
    fnc_AfterSubmitFormAndContinue = () => {
        // get Code 
        GetCodeMax(urlGetCodeMax, '#Code');
        $('#CreateOnStr').val(dateCreate);
        $(tabletbodyStr).html('');
    }
    // sau khi reset
    fnc_AfterResetForm = () => {
        $('#CreateOnStr').val(dateCreate);
    }

    // Master
    $('#TotalQuantityStr').FormatNumber('#TotalQuantity')
    $('#TotalQuantityActualStr').FormatNumber('#TotalQuantityActual')
    $('#TotalQuantityDiffStr').FormatNumber('#TotalQuantityDiff')

    // Lưới
    $('[name="QuantityStr"]').FormatNumberGrid('Quantity', $(tabletbodyStr))
    $('[name="QuantityActualStr"]').FormatNumberGrid('QuantityActual', $(tabletbodyStr))
    $('[name="QuantityDiffStr"]').FormatNumberGrid('QuantityDiff', $(tabletbodyStr))
    $('[name="DifferenceValueStr"]').FormatNumberGrid('DifferenceValue', $(tabletbodyStr))
});


function fuc_TotalAmountGrid() {
    var $tableBody = $(tabletbodyStr);
    var totalQuantity = 0;
    var totalQuantityActual = 0;
    var totalQuantityDiff = 0;
    var totalAll = 0
    for (var i = 0; i < $tableBody.find("tr").length; i++) {
        var $trNew = $tableBody.find("tr:eq(" + i + ")");
        var quantity = 0;
        var quantityActual = 0;
        var quantityDiff = 0;
        var costPrice = 0;
        var price = 0;
        var total = 0;
        $.each($trNew.find(':input'), function (j, val) {

            if ($(this).attr('name') == 'Quantity') {
                quantity = NumberValue($(this).val());
                totalQuantity += quantity;
            } else if ($(this).attr('name') == 'QuantityActual') {
                quantityActual = NumberValue($(this).val());
                totalQuantityActual += quantityActual;
            } else if ($(this).attr('name') == 'QuantityDiff') {
                totalQuantityDiff += quantityActual - quantity;
                $(this).val(quantityActual - quantity);
                quantityDiff = (quantityActual - quantity);
            } else if ($(this).attr('name') == 'QuantityDiffStr') {
                $(this).val(FormatNumberStr(quantityActual - quantity));
            } else if ($(this).attr('name') == 'CostPrice') {
                costPrice = NumberValue($(this).val());
            } else if ($(this).attr('name') == 'Price') {
                price = NumberValue($(this).val());
            } else if ($(this).attr('name') == 'DifferenceValue') {
                total = ((quantityActual - quantity) > 0 ? (quantityDiff * costPrice) : (Math.abs(quantityDiff) * price));
                totalAll += total;
                $(this).val(total); 
            }
            else if ($(this).attr('name') == 'DifferenceValueStr') {
                $(this).val(FormatNumberStr(total));
            }
        });
    }

    $(`#TotalQuantity`).val(totalQuantity);
    $(`#TotalQuantityStr`).val(FormatNumberStr(totalQuantity));

    $(`#TotalQuantityActual`).val(totalQuantityActual);
    $(`#TotalQuantityActualStr`).val(FormatNumberStr(totalQuantityActual));

    $(`#TotalQuantityDiff`).val(totalQuantityDiff);
    $(`#TotalQuantityDiffStr`).val(FormatNumberStr(totalQuantityDiff));

}

function fuc_TotalAmountMaster() {
    //TotalAmountMaster('DiscountValue', 'DeliveryFee', 'TotalDiscountAmount', 'TotalAmount')
}

let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            StoreId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn kho kiểm'
                    }
                }
            },
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã phiếu'
                    }
                }
            },
            InventoryDateStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập ngày kiểm kê'
                    }
                }
            },
            //PriceStr: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng nhập giá'
            //        },
            //        regexp: {
            //            regexp: /[0-9]/,
            //            message: 'Định dạng số không hợp lệ'
            //        }
            //    }
            //},

            QuantityStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.Quantity',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            },
            QuantityActual: {
                // The children's date of birth are inputs with class .childDob
                selector: '.QuantityActual',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

function InitValue() {
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyProduct').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}
function changeStoreBindProducts(selectedValue) {
    var request = $.ajax({
        url: "/WM/Inventory/GetProductByStoreId?storeId=" + selectedValue,
        type: "GET",
    });
    request.done(function (result) {
        if (result.success) {
            $('#tbodyProduct tbody').html(result.html);
            InitValue();
            // Lưới
            $('[name="QuantityStr"]').FormatNumberGrid('Quantity', $(tabletbodyStr))
            $('[name="QuantityActualStr"]').FormatNumberGrid('QuantityActual', $(tabletbodyStr))
            $('[name="QuantityDiffStr"]').FormatNumberGrid('QuantityDiff', $(tabletbodyStr))
            $('[name="DifferenceValueStr"]').FormatNumberGrid('DifferenceValue', $(tabletbodyStr))
        }
    });
    request.fail(function (jqXHR, textStatus) {
        ShowToast('error', textStatus)
    });
}

