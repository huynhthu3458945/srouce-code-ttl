﻿var frm = 'frm';
var tabletbodyStr = '#tbodyProduct tbody';
var tbodyStr = 'tbodyProduct';

$(document).ready(function () {

    RefreshDynamicCommon();
    LoadProduct();
    LoadCombo();
    InitValue();

    $(tabletbodyStr).Uk_RebuilIdAndName();
    $('#StartdateStr').FormatDateTime();
    $('#EnddateStr').FormatDateTime();

    $('select').select2({
        selectOnClose: true
    });

    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(true, frm, tbodyStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(true, frm, tbodyStr);
            }
        });
    });

    // Sau khi submit
    //fnc_AfterSubmitForm = (res) => {
    //    ShowToast('success', res.retText)
    //    location.href = res.urlIndex;
    //}

    // Lưới
    $('[name="CostPriceStr"]').FormatNumberGrid('CostPrice', $(tabletbodyStr))
    $('[name="PriceStr"]').FormatNumberGrid('Price', $(tabletbodyStr))
    $('[name="PriceNewStr"]').FormatNumberGrid('PriceNew', $(tabletbodyStr))
});


let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã'
                    }
                }
            },
            Title: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập tên'
                    }
                }
            },
            PriceNewStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.Price',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}

function InitValue() {
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyProduct').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}

function LoadProduct() {
    var str = '';
    $.ajax({
        url: '/Base/GetProductComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ProductListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}

function LoadCombo() {
    var str = '';
    $.ajax({
        url: '/Base/GetComboComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ComboListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
