﻿var frm = 'frm';
var tabletbodyStr = '#tbodyContact tbody';
var tbodyStr = 'tbodyContact';

var tabletbodyAttributeStr = '#tbodyAttributes tbody';
var tbodyAttributeStr = 'tbodyAttributes';

$(document).ready(function () {
    RefreshDynamicCommon();
    LoadProductBundled();
    LoadUnitBundled();
    LoadAttribute();
    InitValue();

    $('#tbodyContact tbody').Uk_RebuilIdAndName();
    $('#tbodyAttributes tbody').Uk_RebuilIdAndName();
    $('#SaleDeadLineStr').FormatDateTime();

    $('.btnAddRowAttributes').click(function () {
        var table = $(tabletbodyAttributeStr);
        var stt = table.find("tr").length + 1;
        var dataAttribute = $('#AttributeHidden').val();
        var getTemplate = $("#template_subform_attributes").html();

        // Compile the template
        var compileTemplate = Handlebars.compile(getTemplate);
        var context = {
            "stt": stt,
            attributesList: dataAttribute
        };

        // Pass our data to the template
        var passDataToTemplate = compileTemplate(context);
        // Add the compiled html to the page
        if (table.find("tr").length == 0 || (table.find("tr").length == 1 && table.find("tr").find(":contains(không có dữ liệu)").length) == 1) {
            table.html(passDataToTemplate);
        }
        else {
            table.append(passDataToTemplate);
        }

        InitValue();
        table.Uk_RebuildIndex();
        table.Uk_RebuildSortOrder();

        $('select').select2({
            selectOnClose: true
        });
    });

    $('select').select2({
        selectOnClose: true
    });

    // Reset Form
    $('#btnReSet').ResetForm('frm')

    // Lưu
    $('#btnSubmit').click(function () {
        $('.fv-plugins-message-container').remove();
        //if ($('#ProductTypeId').val() == 2)
        //    ValidateStore();
        //else
            Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetail(false, frm, tbodyStr, tbodyAttributeStr);
            }
        });
    });

    // Lưu và nhập tiếp
    $('#btnSubmitAndContinue').click(function () {
        $('.fv-plugins-message-container').remove();
        //if ($('#ProductTypeId').val() == 2)
        //    ValidateStore();
        //else
            Validate();
        fv.validate().then(function (status) {
            // Check Valid
            if (status == 'Valid') {
                $(this).SubmitMasterDetailAndContinue(false, frm, tbodyStr, tbodyAttributeStr);
            }
        });
    });

    // Sau khi submit
    //fnc_AfterSubmitForm = (res) => {
    //    ShowToast('success', res.retText)
    //    location.href = res.urlIndex;
    //}

    //// Sau khi uploadFile
    fnc_AfterUpdateLoadFile = (inputId, data) => {
        if (inputId == 'AvatarImg') {
            $('#Avatar').val(data)
            $('#imgAvatar').attr('src', data);
        }
        else if (inputId == 'ThumbImg') {
            $('#Thumb').val(data)
            $('#imgThumb').attr('src', data);
        }
    }
    if ($('#IsBundledGift').prop("checked"))
        $("#InfoBundledGift").css("display", "block");
    else
        $("#InfoBundledGift").css("display", "none");

    $('#IsBundledGift').change(function () {
        if (this.checked)
            $("#InfoBundledGift").css("display", "block");
        else
            $("#InfoBundledGift").css("display", "none");
    });

    if ($('#ProductTypeId').val() == 2)
        $("#InfoStore").css("display", "block");
    else
        $("#InfoStore").css("display", "none");

    $('#ProductTypeId').change(function () {
        if ($(this).val() == 2)
            $("#InfoStore").css("display", "block");
        else
            $("#InfoStore").css("display", "none");
    });

    if ($('#Id').val() == 0)
    {
        $('#imgAvatar').attr('src', '/assets/img/no-image.png');
        $('#imgThumb').attr('src', '/assets/img/no-image.png');
        
    }
    else {
        if ($('#Avatar').val() == '')
            $('#imgAvatar').attr('src', '/assets/img/no-image.png');
        else
            $('#imgAvatar').attr('src', $('#Avatar').val());

        if ($('#Thumb').val() == '')
            $('#imgThumb').attr('src', '/assets/img/no-image.png');
        else
            $('#imgThumb').attr('src', $('#Thumb').val());
    }
   

    // Master
    $('#PriceStr').FormatNumber('#Price');
    $('#CostPriceStr').FormatNumber('#CostPrice')

    // Lưới
    $('[name="QuantityStr"]').FormatNumberGrid('Quantity', $(tabletbodyStr))
});


let fv = null;

function Validate() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ProductCategoryId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn danh mục'
                    }
                }
            },
            ProductTypeId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn kiểu'
                    }
                }
            },
            UnitId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn đơn vị tính'
                    }
                }
            },
            //SupplierId: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng chọn nhà cung cấp'
            //        }
            //    }
            //},
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã'
                    }
                }
            },
            Title: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập tên'
                    }
                }
            },
            CostPriceStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá vốn'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }
                }
            },
            PriceStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }
                }
            },
            QuantityStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.Quantity',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            }
            ,
            ProductBundled: {
                // The children's date of birth are inputs with class .childDob
                selector: '.ProductBundled',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mặt hàng'
                    }
                }
            },
            UnitIdBundled: {
                // The children's date of birth are inputs with class .childDob
                selector: '.UnitIdBundled',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn đơn vị tính'
                    }
                }
            },
            AttributeId: {
                // The children's date of birth are inputs with class .childDob
                selector: '.AttributeId',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn thuộc tính'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}


function ValidateStore() {
    const formValidationExamples = document.getElementById('frm');
    fv = FormValidation.formValidation(formValidationExamples, {
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ProductCategoryId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn danh mục'
                    }
                }
            },
            ProductTypeId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn kiểu'
                    }
                }
            },
            StoreId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn kho xuất'
                    }
                }
            },
            UnitId: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn đơn vị tính'
                    }
                }
            },
            //SupplierId: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Vui lòng chọn nhà cung cấp'
            //        }
            //    }
            //},
            Code: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mã'
                    }
                }
            },
            Title: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập tên'
                    }
                }
            },
            CostPriceStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá vốn'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }
                }
            },
            PriceStr: {
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập giá'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }
                }
            },
            QuantityStr: {
                // The children's date of birth are inputs with class .childDob
                selector: '.Quantity',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập số lượng'
                    },
                    regexp: {
                        regexp: /[0-9]/,
                        message: 'Định dạng số không hợp lệ'
                    }

                }
            }
            ,
            ProductBundled: {
                // The children's date of birth are inputs with class .childDob
                selector: '.ProductBundled',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng nhập mặt hàng'
                    }
                }
            },
            UnitIdBundled: {
                // The children's date of birth are inputs with class .childDob
                selector: '.UnitIdBundled',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn đơn vị tính'
                    }
                }
            },
            AttributeId: {
                // The children's date of birth are inputs with class .childDob
                selector: '.AttributeId',
                // The field is placed inside .col-xs-4 div instead of .form-group
                validators: {
                    notEmpty: {
                        message: 'Vui lòng chọn thuộc tính'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({

            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
}
function InitValue() {
    $('[name="btnRemoveLine"]').Uk_RemoveRow($(tabletbodyStr));
    $('[accesskey="KeyNewLine"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyContact').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRow').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });

    $('[name="btnRemoveLineAttributes"]').Uk_RemoveRow($(tabletbodyAttributeStr));
    $('[accesskey="btnRemoveLineAttributes"]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = $('#tbodyAttributes').find('.tr').length;
            var idx = $('[accesskey="KeyNewLine"]').index($(this)) + 1;
            if (idxTbl == idx) {
                $('.btnAddRowAttributes').click();
                $('[accesskey="KeyFocus"]').last().focus();
            }
            return false;
        }
    });
}

function LoadProductBundled() {
    var str = '';
    $.ajax({
        url: '/Base/GetProductComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#ProductBundledHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}

function LoadUnitBundled() {
    var str = '';
    $.ajax({
        url: '/Base/GetUnitComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#UnitListHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}
function LoadAttribute() {
    var str = '';
    $.ajax({
        url: '/Base/GetAttributesComboxJson',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data.data, function (index, value) {
                str += '<option value="' + value.value + '">' + value.text + '</option>';
            });
            $('#AttributeHidden').val(str);
        },
        error: function () {
            console.log('error');
        }
    });
}



/**
 * Block UI (jquery)
 */

'use strict';

$(function () {
    var   pageBlock = $('.btn-page-block');
      
    // Default
    if (pageBlock.length) {
        pageBlock.on('click', function () {
            $.blockUI({
                message: '<div class="spinner-border text-white" role="status"></div>',
                timeout: 1000,
                css: {
                    backgroundColor: 'transparent',
                    border: '0'
                },
                overlayCSS: {
                    opacity: 0.5
                }
            });
        });
    }
});
