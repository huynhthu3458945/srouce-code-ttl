﻿var frmId = undefined;
var tableDetailID = undefined;

var fnc_AfterSubmitForm = undefined;
var fnc_AfterSubmitFormAndContinue = undefined;
var fnc_AfterResetForm = undefined;

var fnc_BeforeSubmitForm = undefined;

var fnc_AfterUpdateLoadFile = undefined;
var fnc_AfterLoadChangeCombobox = undefined;
var fnc_AfterFormatNumberGrid = undefined;
var fnc_AfterDeletRowGrid = undefined;
var fnc_AfterChangeDiscountKindGrid = undefined;


var fnc_AfterLoadMenu = undefined;

var fnc_BeforeShowPopup = undefined;

function RefreshDynamicCommon() {
    frmId = undefined;
    tableDetailID = undefined;

    fnc_AfterSubmitForm = undefined;
    fnc_AfterSubmitFormAndContinue = undefined;
    fnc_AfterResetForm = undefined;

    fnc_AfterUpdateLoadFile = undefined;
    fnc_AfterLoadChangeCombobox = undefined;
    fnc_AfterDeletRowGrid = undefined;
    fnc_AfterChangeDiscountKindGrid = undefined;

    fnc_BeforeShowPopup = undefined;
    fnc_AfterFormatNumberGrid = undefined;

    fnc_AfterLoadMenu = undefined;
}

function GetFormData(formid) {
    var form = document.getElementById(formid);
    let formdata = {};
    let valueRadio = ""
    Array.from(form.querySelectorAll('input, select, textarea'))
        .filter(element => element.name)
        .forEach(element => {
            if (element.id != '') {
                if (element.type === 'checkbox') {
                    formdata[element.name] = element.checked;
                }
                else if (element.type === 'radio') {
                    //formdata[element.name] = getRadioCheckedValue(formid, element.name);
                    if (element.checked) {
                        valueRadio = element.value;
                    }
                    formdata[element.name] = valueRadio;
                }
                else if (element.options) {
                    var selected = [...element.selectedOptions].map(option => option.value);
                    //var selected = [...element.options].filter(option => option.selected).map(option => option.value);
                    if (selected.length > 1) {
                        formdata[element.name] = selected;
                    } else {
                        formdata[element.name] = element.value;
                    }
                } else {
                    // if (element.id != '')
                    formdata[element.name] = element.value;
                }
            }
        });
    return formdata;
}

function getJsonFormData(formdata) {
    var dataStringJson = JSON.stringify(Object.assign({}, formdata));
    var datalJson = JSON.parse(dataStringJson)

    return datalJson;
}
function getRadioCheckedValue(formid, radio_name) {
    var oRadio = document.forms[formid].elements[radio_name];
    var data = {};
    oRadio.forEach(element => {
        data[element.id] = element.checked;
    });
    return data;
}

function GetFormDataDetail(tableId) {
    var rows = [];
    if (tableId != '') {
        $(`#${tableId} tbody`).find("tr").each(function () {
            cur = {};
            $(this).find('td').each(function (i, v) {
                Array.from($(this).find('input, select, textarea'))
                    .filter(element => element.name)
                    .forEach(element => {
                        if (element.type === 'checkbox') {
                            cur[element.name] = element.checked;
                        }
                        else if (element.type === 'radio') {
                            //cur[element.name] = getRadioCheckedValue(formid, element.name);
                            if (element.checked) {
                                valueRadio = element.value;
                            }
                            cur[element.name] = valueRadio;
                        }
                        else if (element.options) {
                            var selected = [...element.selectedOptions].map(option => option.value);
                            //var selected = [...element.options].filter(option => option.selected).map(option => option.value);
                            if (selected.length > 1) {
                                cur[element.name] = selected;
                            } else {
                                cur[element.name] = element.value;
                            }
                        } else {
                            cur[element.name] = element.value;
                        }
                    });
            });
            rows.push(cur);
            cur = {};
        })
    }

    var detailStringJson = JSON.stringify(Object.assign({}, rows));
    var detailData = JSON.parse(detailStringJson)
    return detailData;
}

function formatCurrency(n) {
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

function ShowToast(statusErr, msg) {
    var shortCutFunction = statusErr,
        isRtl = true,
        msg = msg,
        title = '';
    switch (statusErr) {
        case 'success':
            title = 'Thành công';
            break;
        case 'info':
            title = 'Thông tin';
            break;
        case 'warning':
            title = 'Cảnh báo';
            break;
        case 'error':
            title = 'Lỗi';
            break;
    }
    // success, info, warning, error
    toastr.options = {
        maxOpened: 1,
        autoDismiss: true,
        closeButton: true,
        progressBar: true,
        preventDuplicates: true,
        onclick: null,
        rtl: isRtl
    };
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;

}

// Xác nhận delete
function confirmDetele(id, urlDelete, isReloadPage) {
    Swal.fire({
        title: 'Bạn có chắc chắn xóa?',
        text: "Bạn sẽ không thể hoàn nguyên điều này!!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Chắc chắn',
        cancelButtonText: 'Hủy',
        customClass: {
            confirmButton: 'btn btn-primary me-3',
            cancelButton: 'btn btn-label-secondary'
        },
        buttonsStyling: false
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: urlDelete,
                data: { id: id },
                success: function (res) {
                    if (res.retCode == 0) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Deleted!',
                            text: 'Đã xóa thành công.',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                        if (isReloadPage) {
                            location.reload();
                        }
                    }
                    else {
                        ShowToast('error', res.retText)
                        location.href = res.url;
                    }
                },
                error: function () {
                    console.log('Error - Failed');
                }
            });

        }
    });
}

$.urlParam = function (name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return '';
    } else {
        return results[1] || 0;
    }
}

function LoadInfoUser() {
    $.ajax({
        url: '/Base/GetJsonUserInfo',
        type: 'GET',
        data: {},

        success: function (res) {
            $('#spanFullName').text(res.fullName);
            $('#spanRole').text(res.role);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function LoadApplication() {
    $.ajax({
        url: '/Base/GetApplicationStr',
        type: 'GET',
        data: {},

        success: function (res) {
            $('.dropdown-shortcuts-list').html(res);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function LoadOrganization() {
    $.ajax({
        url: '/Base/GetOrganization',
        type: 'GET',
        data: {},

        success: function (res) {
            if (res.retCode == 0) {
                if (res.data.title == null)
                    $("#titleOrganization").remove();
                else
                    $("#titleOrganization").text(res.data.title);
            }
            else if (res.retCode == 1) {
                ShowToast('error', res.retText)
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function LoadMenu(applicationCode, currentMenu) {
    $.ajax({
        url: '/Base/GetMenuStr?applicationCode=' + applicationCode + '&currentMenu=' + currentMenu,
        type: 'GET',
        data: {},
        async: true,
        success: function (res) {
            $('.menu-inner').html(res);
            if (fnc_AfterLoadMenu !== undefined) {
                fnc_AfterLoadMenu();
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function LoadCommon() {
    LoadInfoUser();
    LoadApplication();
    LoadOrganization();
}
// Nhập lại
$.fn.ResetForm = function (formIDMst) {
    this.on("click", function (e) {
        e.preventDefault();
        // reset 
        $('.fv-plugins-message-container').remove();
        var code = $("#Code").val();
        $(`#${formIDMst}  input[type=text]`).val("");
        $(`#${formIDMst}  input[type=file]`).val("");
        $(`#${formIDMst}  img`).prop("src", "");
        //$(`#${formIDMst}  select`).prop("selectedIndex", 0);
        $(`#${formIDMst}  input[type=radio]`).prop("checked", false);
        $(`#${formIDMst}  input[type=checkbox]`).prop("checked", false);
        $("#Code").val(code);
        if (fnc_AfterResetForm !== undefined) {
            fnc_AfterResetForm();
        }
    });
}
// Submit Form và tiếp tục
$.fn.SubmitMasterDetailAndContinue = function (ischeckDetail, formIDMst, tableId, tableId1) {
    if (ischeckDetail) {
        if (tableId != '' && $('#' + tableId + ' tbody').find("tr").find(":contains(không có dữ liệu)").length == 1) {
            ShowToast('warning', 'Chưa nhập dữ liệu chi tiết!')
            return;
        }
        if (tableId1 != undefined && $('#' + tableId1 + ' tbody').find("tr").find(":contains(không có dữ liệu)").length == 1) {
            ShowToast('warning', 'Chưa nhập dữ liệu chi tiết!')
            return;
        }
    }
    BlockUI();
    var masterData = GetFormData(formIDMst);
    masterData.Details = GetFormDataDetail(tableId);
    masterData.Details1 = GetFormDataDetail(tableId1);

    $.ajax({
        type: "POST",
        url: $(`#${formIDMst}`).attr('action'),
        data: masterData,
        dataType: "json",
        encode: true,
        success: function (res) {
            UnblockUI();
            if (res.retCode == 0) {
                ShowToast('success', res.retText)
                $(`#${formIDMst}  input[type=text]`).val("");
                $(`#${formIDMst}  input[type=file]`).val("");
                $(`#${formIDMst}  img`).prop("src", "");
                //$(`#${formIDMst}  select`).prop("selectedIndex", 0);
                $(`#${formIDMst}  input[type=radio]`).prop("checked", false);
                $(`#${formIDMst}  input[type=checkbox]`).prop("checked", false);
                // Có hàm customer thì call
                if (fnc_AfterSubmitFormAndContinue !== undefined) {
                    fnc_AfterSubmitFormAndContinue();
                }
            }
            else if (res.retCode == 1) {
                ShowToast('error', res.retText)
            }
        },
        error: function () {
            console.log('Error - Failed');
        }
    });
}
// Submit Form
$.fn.SubmitMasterDetail = function (ischeckDetail, formIDMst, tableId, tableId1) {

    if (ischeckDetail) {
        if (tableId != '' && $('#' + tableId + ' tbody').find("tr").find(":contains(không có dữ liệu)").length == 1) {
            ShowToast('warning', 'Chưa nhập dữ liệu chi tiết!1')
            return;
        }
        if ((tableId1 != undefined) && $('#' + tableId1 + ' tbody').find("tr").find(":contains(không có dữ liệu)").length == 1) {
            ShowToast('warning', 'Chưa nhập dữ liệu chi tiết!')
            return;
        }
    }
    BlockUI();
    var masterData = GetFormData(formIDMst);
    masterData.Details = GetFormDataDetail(tableId);
    masterData.Details1 = GetFormDataDetail(tableId1);

    $.ajax({
        type: "POST",
        url: $(`#${formIDMst}`).attr('action'),
        data: masterData,
        dataType: "json",
        encode: true,
        success: function (res) {
            UnblockUI();
            if (res.retCode == 0) {

                // Có hàm customer thì call
                if (fnc_AfterSubmitForm !== undefined) {
                    fnc_AfterSubmitForm(res);
                }
                else {
                    ShowToast('success', res.retText)
                    location.href = res.urlIndex;
                    //window.history.pushState('', '', res.url)
                    //document.title = title;
                    //$(`#${formIDMst}`).attr('action', res.url.split('?')[0]);
                    //$(`#btnSubmit`).html('Cập nhật');
                    //$(`#btnReSet`).remove();
                    //$(`#btnSubmitAndContinue`).remove();
                }
            }
            else if (res.retCode == 1) {
                ShowToast('error', res.retText)
            }
        },
        error: function () {
            console.log('Error - Failed');
        }
    });
}

showInPopup = (url, title) => {
    if (fnc_BeforeShowPopup !== undefined) {
        if (!fnc_BeforeShowPopup()) {
            return;
        }
    }
    $.ajax({
        type: "GET",
        url: url,
        success: function (res) {
            $('#form-modal .modal-body-detail').html(res);
            $('#form-modal .modal-title').html(title);
            $('#form-modal').modal("show");
        }
    });
}
AddNew = (url) => {
    location.href = url;
}

$.fn.Uk_RemoveRow = function (objTbl) {
    this.on("click", function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();
        objTbl.Uk_RebuildSortOrder();
        objTbl.Uk_RebuildIndex();
        if (fnc_AfterDeletRowGrid !== undefined) {
            fnc_AfterDeletRowGrid();
        }
    });
}

$.fn.UK_ChangeDiscountKind = function (objTbl) {
    this.on("change", function (e) {
        e.preventDefault();
        var priceDiscount = $(this).parent().parent().find('[name="PriceDiscountStr"]').val();
        if ($(this).val() == 1)  // 1 % , 2 đ
            if (NumberValue(priceDiscount) > 100) {
                $(this).parent().parent().find('[name="PriceDiscountStr"]').val('100');
                $(this).parent().parent().find('[name="PriceDiscount"]').val('100');
            }
        if (fnc_AfterChangeDiscountKindGrid !== undefined) {
            fnc_AfterChangeDiscountKindGrid();
        }
    });
}

$.fn.UK_ChangePriceDiscount = function (objTbl) {
    this.on("change", function (e) {
        e.preventDefault();
        var discountKind = $(this).parent().parent().find('[name="DT_DiscountKindId"]').val();
        //var discountKind = $(this).parent();
        if (discountKind == 1)  // 1 % , 2 đ
            if (NumberValue($(this).val()) > 100) {
                $(this).val('100');
                $(this).parent().parent().find('[name="PriceDiscount"]').val('100');
            }
    });
}

$.fn.Uk_RebuildIndex = function () {
    var $tableBody = this;
    if ($tableBody.find("tr").find(":contains(không có dữ liệu)").length == 0) {
        for (var i = 0; i < $tableBody.find("tr").length; i++) {
            var $trNew = $tableBody.find("tr:eq(" + i + ")");
            var suffix = $trNew.find(':input:first').attr('name').match(/\d+/);
            $.each($trNew.find(':input'), function (j, val) {
                var oldN = $(this).attr('name');
                var oldId = $(this).attr('id');
                if (typeof (oldN) != "undefined") {
                    var newN = oldN.replace('[' + suffix + ']', '[' + i + ']');
                    $(this).attr('name', newN);
                }
                if (typeof (oldId) != "undefined") {
                    var newId = oldId.replace('_' + suffix + '__', '_' + i + '__');
                    $(this).attr('id', newId);
                }
            });
        }
    }
};

$.fn.Uk_RebuildSortOrder = function () {
    var $tableBody = this;
    if ($tableBody.find("tr").find(":contains(không có dữ liệu)").length == 0) {
        for (var i = 0; i < $tableBody.find('tr').length; i++) {
            $tableBody.find('tr:eq(' + i + ')').find("td:first").html((i + 1));
        }
    }
};

// Dùng cho màn hình Master Detail
$.fn.Uk_RebuilIdAndName = function () {
    var $tableBody = this;
    if ($tableBody.find("tr").find(":contains(không có dữ liệu)").length == 0) {
        for (var i = 0; i < $tableBody.find("tr").length; i++) {
            var $trNew = $tableBody.find("tr:eq(" + i + ")");
            var suffix = $trNew.find(':input:first').attr('name').match(/\d+/);
            $.each($trNew.find(':input'), function (j, val) {
                var oldN = $(this).attr('name');
                var oldId = $(this).attr('id');
                if (typeof (oldN) != "undefined") {
                    var newN = oldN.split(".");
                    $(this).attr('name', newN[1]);
                }
                if (typeof (oldId) != "undefined") {
                    $(this).attr('id', '');
                }
            });
        }
    }
};

function changeProvince(selectedValue) {
    var request = $.ajax({
        url: "/Base/GetDistrictByProvinceId?provinceId=" + selectedValue,
        type: "GET",
    });
    request.done(function (result) {
        $('#DistrictId').children().remove();
        //$("#DistrictId").append($('<option></option>').val("0").html("All"));
        $.each(result.data, function (i, item) {
            $("#DistrictId").append($('<option></option>').val(item.value).html(item.text).attr('selected', item.selected));
        });
    });
    request.fail(function (jqXHR, textStatus) {
        ShowToast('error', textStatus)
    });
}

function GetCodeMax(url, idCode) {
    var request = $.ajax({
        url: url,
        type: "GET",
    });
    request.done(function (result) {
        $(idCode).val(result.code);
    });
    request.fail(function (jqXHR, textStatus) {
        ShowToast('error', textStatus)
    });
}
function changeOrganization(value) {

    // set Organization
    var request = $.ajax({
        url: "/Base/SetOrganization?organizationId=" + value,
        type: "GET",
    });
    request.done(function (result) {
        // location.reload();
        location.href = '/';
    });
    request.fail(function (jqXHR, textStatus) {
        ShowToast('error', textStatus)
    });
}

function changeStoreBindPallet(selectedValue) {
    var request = $.ajax({
        url: "/Base/GetPalletByStoreId?storeId=" + selectedValue,
        type: "GET",
    });
    request.done(function (result) {
        $('#PalletId').children().remove();
        //$("#DistrictId").append($('<option></option>').val("0").html("All"));
        $.each(result.data, function (i, item) {
            $("#PalletId").append($('<option></option>').val(item.value).html(item.text).attr('selected', item.selected));
        });
        if (fnc_AfterLoadChangeCombobox !== undefined) {
            fnc_AfterLoadChangeCombobox();
        }
    });
    request.fail(function (jqXHR, textStatus) {
        ShowToast('error', textStatus)
    });
}

//function loadOrganization() {
//    var request = $.ajax({
//        url: "/Base/GetOrganization",
//        type: "GET",
//    });
//    request.done(function (result) {
//        //if (result.retCode == 1) {
//        //    console.log(result.data);
//        //    ShowToast('error', result.retText);
//        //    $('#layoutOrganizationId').children().remove();
//        //    //$("#DistrictId").append($('<option></option>').val("0").html("All"));
//        //    $.each(result.data, function (i, item) {
//        //        $("#layoutOrganizationId").append($('<option></option>').val(item.value).html(item.text).attr('selected', item.selected));
//        //    });
//        //    $('#layoutOrganizationId').focus();
//        //}
//        //else {
//            $('#layoutOrganizationId').children().remove();
//            //$("#DistrictId").append($('<option></option>').val("0").html("All"));
//            $.each(result.data, function (i, item) {
//                $("#layoutOrganizationId").append($('<option></option>').val(item.value).html(item.text).attr('selected', item.selected));
//            });
//        //}
//    });
//    request.fail(function (jqXHR, textStatus) {
//        ShowToast('error', textStatus)
//    });
//}


uploadFiles = (inputId, uploadDirecotroy) => {
    var input = document.getElementById(inputId);
    var files = input.files;
    var formData = new FormData();

    for (var i = 0; i != files.length; i++) {
        formData.append("files", files[i]);
    }
    formData.append('uploadDirecotroy', uploadDirecotroy);
    $.ajax(
        {
            url: "/Base/UploadFile",
            data: formData,
            processData: false,
            contentType: false,
            async: false,
            type: "POST",
            success: function (data) {
                if (fnc_AfterUpdateLoadFile !== undefined) {
                    fnc_AfterUpdateLoadFile(inputId, data.data);
                }
            }
        }
    );
}

const USDollar = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});
const VND = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
});

// parseFloat("1234567.891").toLocaleString(window.document.documentElement.lang);
// "1,234,567.891"

// fomrmat number và gán vào input ẩn
$.fn.FormatNumber = function (idInputValue) {
    this.on("change", function (e) {
        e.preventDefault();
        $(idInputValue).val(NumberValue($(this).val()));
        $(this).val(FormatNumberStr(NumberValue($(this).val())));
    });
    $(this).val(FormatNumberStr(NumberValue($(idInputValue).val())));
}

// fomrmat number và gán vào input ẩn trên lưới
$.fn.FormatNumberGrid = function (nameValue, objTbl) {

    //var inputParent = $(this).parent().children('[name="' + nameValue + '"]');
    // var inputParentThis = $(this).parent().find('[name="' + $(this).attr('name') + '"]');

    this.on("change", function (e) {
        e.preventDefault();
        $(this).parent().children('[name="' + nameValue + '"]').val(NumberValue($(this).val()))
        $(this).val(FormatNumberStr($(this).parent().children('[name="' + nameValue + '"]').val()));

        if (fnc_AfterFormatNumberGrid !== undefined) {
            fnc_AfterFormatNumberGrid();
        }
    });

    var thisInput = $(this);
    var $tableBody = objTbl;
    for (var i = 0; i < $tableBody.find("tr").length; i++) {
        var $trNew = $tableBody.find("tr:eq(" + i + ")");
        var value = 0
        $.each($trNew.find(':input'), function (j, val) {
            if ($(this).attr('name') == nameValue) {
                value = $(this).val();
            }
            if ($(this).attr('name') == thisInput.attr('name')) {
                $(this).val(FormatNumberStr(NumberValue(value)));
            }
        });
    }
    // $(this).val(FormatNumberStr($(this).parent().children('[name="' + nameValue + '"]').val()));
}

// fomrmat number vnđ
function FormatNumberStr(value) {
    return parseFloat(value).toLocaleString('en')
}

// trả số
function NumberValue(value) {
    if (isNaN(value.replace(/,/g, "")))
        return 0;
    else
        return Number(value.replace(/,/g, ""));
}

// kiểu tra số
function isNaN(x) {
    // Ép kiểu Number cho biến x
    x = Number(x);
    // Nếu x là NaN, NaN != NaN trả về true, các trường hợp khác sẽ trả về false
    return x != x;
}

// tính thành tiền trên master
function TotalAmountMasterPO(discount, deliveryFee, totalDiscountAmount, totalAmount, MT_DiscountKind) {

    var total = $(`#${totalAmount}`).val();
    var deliveryFee = $(`#${deliveryFee}`).val();

    if ($(`#${discount}`).val() > 0) {
        var discountId = `#${MT_DiscountKind}`.val(); // 1 % , 2 đ
        var discountTotal = total * $(`#${discount}`).val() / (discountId == 1 ? 100 : 1);

        $(`#${totalDiscountAmount}`).val(discountTotal);
        $(`#${totalDiscountAmount}Str`).val(FormatNumberStr(discountTotal));

        $(`#${totalAmount}`).val(total - discountTotal - deliveryFee);
        $(`#${totalAmount}Str`).val(FormatNumberStr(total - discountTotal - deliveryFee));
    }
    else {
        $(`#${totalDiscountAmount}`).val(0);
        $(`#${totalDiscountAmount}Str`).val(FormatNumberStr(0));
        $(`#${totalAmount}`).val(total - deliveryFee);
        $(`#${totalAmount}Str`).val(FormatNumberStr(total - deliveryFee));
    }
}


// tính thành tiền trên lưới list name cột muốn tính
function TotalAmountGridPO(nameQuantity, nameDiscount, namePrice, nameTotal, idDiscountMaster, idDeliveryFeeMaster, idTotalDiscountAmountMaster, idTotalAmountMaster, idDiscountKindMaster, nameDiscountKind, objTbl) {
    var $tableBody = objTbl;
    var totalAll = 0
    var deliveryFee = NumberValue($(`#${idDeliveryFeeMaster}Str`).val());
    for (var i = 0; i < $tableBody.find("tr").length; i++) {
        var $trNew = $tableBody.find("tr:eq(" + i + ")");
        var quantity = 0
        var discount = 0
        var discountKindId = 0
        var price = 0
        var total = 0
        var elementPriceDiscount = null;
        var elementPriceDiscountStr = null;
        $.each($trNew.find(':input'), function (j, val) {
            if ($(this).attr('name') == nameQuantity) {
                quantity = $(this).val();
            } else if ($(this).attr('name') == nameDiscount) {
                discount = $(this).val();
                elementPriceDiscount = $(this);
            }
            else if ($(this).attr('name') == nameDiscount + 'Str') {
                elementPriceDiscountStr = $(this);
            }
            else if ($(this).attr('name') == nameDiscountKind) {
                discountKindId = $(this).val();
                if (discountKindId == 1)  // 1 % , 2 đ
                    if (discount > 100) {
                        discount = 100;
                        elementPriceDiscount.val(100)
                        elementPriceDiscountStr.val(FormatNumberStr(100))
                    }
            } else if ($(this).attr('name') == namePrice) {
                price = $(this).val();
            } else if ($(this).attr('name') == nameTotal) {

                total = (quantity * price);
                total = total - (discountKindId == 1 ? total * discount / 100 : discount);
                totalAll += total;
                $(this).val(total);
            }
            else if ($(this).attr('name') == nameTotal + 'Str') {
                $(this).val(FormatNumberStr(total));
            }
        });
    }
    if ($(`#${idDiscountMaster}`).val() > 0) {
        var discountId = $(`#${idDiscountKindMaster}`).val();
        var discountTotal = 0;
        if (discountId == 1)  // 1 % , 2 đ
        {
            if ($(`#${idDiscountMaster}`).val() > 100) {
                $(`#${idDiscountMaster}`).val(100)
                $(`#${idDiscountMaster}Str`).val(FormatNumberStr(100))
            }
            discountTotal = totalAll * $(`#${idDiscountMaster}`).val() / 100;
        }
        else
            discountTotal = $(`#${idDiscountMaster}`).val();
        $(`#${idTotalDiscountAmountMaster}`).val(discountTotal);
        $(`#${idTotalDiscountAmountMaster}Str`).val(FormatNumberStr(discountTotal));

        $(`#${idTotalAmountMaster}`).val(totalAll - discountTotal - deliveryFee);
        $(`#${idTotalAmountMaster}Str`).val(FormatNumberStr(totalAll - discountTotal - deliveryFee));
    }
    else {
        $(`#${idTotalDiscountAmountMaster}`).val(0);
        $(`#${idTotalDiscountAmountMaster}Str`).val(FormatNumberStr(0));
        $(`#${idTotalAmountMaster}`).val(totalAll - deliveryFee);
        $(`#${idTotalAmountMaster}Str`).val(FormatNumberStr(totalAll - deliveryFee));
    }
}


// tính thành tiền trên master
function TotalAmountMaster(discount, deliveryFee, totalDiscountAmount, totalAmount) {

    var total = $(`#${totalAmount}`).val();
    var deliveryFee = $(`#${deliveryFee}`).val();

    if ($(`#${discount}`).val() > 0) {
        var discountTotal = total * $(`#${discount}`).val() / 100;

        $(`#${totalDiscountAmount}`).val(discountTotal);
        $(`#${totalDiscountAmount}Str`).val(FormatNumberStr(discountTotal));

        $(`#${totalAmount}`).val(total - discountTotal - deliveryFee);
        $(`#${totalAmount}Str`).val(FormatNumberStr(total - discountTotal - deliveryFee));
    }
    else {
        $(`#${totalDiscountAmount}`).val(0);
        $(`#${totalDiscountAmount}Str`).val(FormatNumberStr(0));
        $(`#${totalAmount}`).val(total - deliveryFee);
        $(`#${totalAmount}Str`).val(FormatNumberStr(total - deliveryFee));
    }
}


// tính thành tiền trên lưới list name cột muốn tính
function TotalAmountGrid(nameQuantity, nameDiscount, namePrice, nameTotal, idDiscountMaster, idDeliveryFeeMaster, idTotalDiscountAmountMaster, idTotalAmountMaster, objTbl) {
    var $tableBody = objTbl;
    var totalAll = 0
    var deliveryFee = NumberValue($(`#${idDeliveryFeeMaster}Str`).val());
    for (var i = 0; i < $tableBody.find("tr").length; i++) {
        var $trNew = $tableBody.find("tr:eq(" + i + ")");
        var quantity = 0
        var discount = 0
        var price = 0
        var total = 0
        $.each($trNew.find(':input'), function (j, val) {
            if ($(this).attr('name') == nameQuantity) {
                quantity = $(this).val();
            } else if ($(this).attr('name') == nameDiscount) {
                discount = $(this).val();
            } else if ($(this).attr('name') == namePrice) {
                price = $(this).val();
            } else if ($(this).attr('name') == nameTotal) {
                total = (quantity * price) - (discount);
                totalAll += total;
                $(this).val(total);
            }
            else if ($(this).attr('name') == nameTotal + 'Str') {
                $(this).val(FormatNumberStr(total));
            }
        });
    }
    if ($(`#${idDiscountMaster}`).val() > 0) {
        var discountTotal = totalAll * $(`#${idDiscountMaster}`).val() / 100;
        $(`#${idTotalDiscountAmountMaster}`).val(discountTotal);
        $(`#${idTotalDiscountAmountMaster}Str`).val(FormatNumberStr(discountTotal));

        $(`#${idTotalAmountMaster}`).val(totalAll - discountTotal - deliveryFee);
        $(`#${idTotalAmountMaster}Str`).val(FormatNumberStr(totalAll - discountTotal - deliveryFee));
    }
    else {
        $(`#${idTotalDiscountAmountMaster}`).val(0);
        $(`#${idTotalDiscountAmountMaster}Str`).val(FormatNumberStr(0));
        $(`#${idTotalAmountMaster}`).val(totalAll - deliveryFee);
        $(`#${idTotalAmountMaster}Str`).val(FormatNumberStr(totalAll - deliveryFee));
    }
}

// formatDateTime dd/MM/yyyy
$.fn.FormatDateTime = function () {
    var bsDateStrDatepickerFormat = $(this);
    // Format
    if (bsDateStrDatepickerFormat.length) {
        bsDateStrDatepickerFormat.datepicker({
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            orientation: isRtl ? 'auto right' : 'auto left'
        });
    }
}

// blockUI
function BlockUI() {
    $.blockUI({
        message: '<div class="spinner-border text-primary" role="status"></div>',
        //timeout: 1000,
        css: {
            backgroundColor: 'transparent',
            border: '0'
        },
        overlayCSS: {
            opacity: 0.5
        }
    });
}
function UnblockUI() {
    $.unblockUI()
}
//// Loading Click
//$.fn.BlockLoadingOnClick = function (cardBlock) {
//    const cardReload = $(this);
//    if (cardReload.length) {
//        cardReload.on('click', function (e) {
//            e.preventDefault();
//            var $this = $(this);
//            $this.closest(cardBlock).block({
//                message:
//                    '<div class="sk-fold sk-primary"><div class="sk-fold-cube"></div><div class="sk-fold-cube"></div><div class="sk-fold-cube"></div><div class="sk-fold-cube"></div></div><h5>LOADING...</h5>',

//                css: {
//                    backgroundColor: 'transparent',
//                    border: '0'
//                },
//                overlayCSS: {
//                    backgroundColor: $('html').hasClass('dark-style') ? '#000' : '#fff',
//                    opacity: 0.55
//                }
//            });
//            // alert
//            //   $this.closest('.card').unblock();
//            //                if ($this.closest('.card').find('.card-alert').length) {
//            //                    $this
//            //                        .closest('.card')
//            //                        .find('.card-alert')
//            //                        .html(
//            //                            '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button><strong>Holy grail!</strong> Your success/error message here.</div>'
//            //                        );
//            //                }
//        });
//    }
//}

//$.fn.UnblockLoadingOnClick = function (cardBlock) {
//    $(this).closest(cardBlock).unblock();
//}

// Loading 
function BlockLoading(cardBlock) {
    $(cardBlock).block({
        message:
            '<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div><h6>Đang tải dữ liệu...</h6>',
        //'<div class="sk-fold sk-primary"><div class="sk-fold-cube"></div><div class="sk-fold-cube"></div><div class="sk-fold-cube"></div></div><h5>LOADING...</h5>',

        css: {
            backgroundColor: 'transparent',
            border: '0'
        },
        overlayCSS: {
            backgroundColor: $('html').hasClass('dark-style') ? '#000' : '#fff',
            opacity: 0.55
        }
    });
    // alert
    //   $this.closest('.card').unblock();
    //                if ($this.closest('.card').find('.card-alert').length) {
    //                    $this
    //                        .closest('.card')
    //                        .find('.card-alert')
    //                        .html(
    //                            '<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button><strong>Holy grail!</strong> Your success/error message here.</div>'
    //                        );
    //                }

}

function UnblockLoading(cardBlock) {
    $(cardBlock).unblock();
}

// Check trùng dữ liệu trên lưới trên lưới
function CheckComboxContains(tableId, value, index) {

    var res = false;
    $.each($(`#${tableId} tbody tr`), function () {
        var valueTr = $(this).find("td:eq(" + index + ")").find('select option:selected').val();
        var textTr = $(this).find("td:eq(" + index + ")").find('select option:selected').text();

        //console.log($(this).find("td:eq(" + index + ")").find('select option:selected').val());
        if (valueTr == value) {
            ShowToast('warning', textTr + ' đã có trong danh sách.')
            res = true;
            return false;
        }
    });
    return res;
}